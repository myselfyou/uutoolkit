import axios from 'axios'
import qs from 'qs'
import store from '@/store'
const http = {}
var instance = axios.create({
    timeout: 15000,
    baseURL: process.env.VUE_APP_BASE_API,
})
//添加請求攔截器
instance.interceptors.request.use(
    function (config) {
        if (store.state.UserToken) {
            config.headers.Authorization = `Bearer ${store.state.UserToken}`
        }
        return config
    },
    function (error) {
        return Promise.reject(error)
    }
)
instance.interceptors.response.use(response => {
    let contentType = response.headers["content-type"]
    if (contentType == null || !contentType.toString().startsWith("application/json")) {
        return response
    }
    if (response.data.code != null && response.data.code === 401) {
        // window.vue.$message.info('请重新登录')
        store.commit('LOGIN_OUT')
        setTimeout(() => {
            window.router.push({ name: 'login' })
        }, 1000)
        return response.data
    } else {
        return response.data
    }
}, err => {
    if (!err || !err.response) {
        err.message = '连接服务器失败'
    }
    window.vue.$message.info(err.message)
    return Promise.reject(err.response)
})
http.get = function (url, options) {
    return new Promise((resolve, reject) => {
        instance.get(url, options).then(response => {
            if (response.code === 200) {
                resolve(response)
            } else {
                reject(response.msg)
            }
        }).catch(e => {
            console.log(e)
        })
    })
}

http.post = function (url, data, options) {
    return new Promise((resolve, reject) => {
        instance.post(url, data, options).then(response => {
            if (response.code === 200) {
                resolve(response)
            } else {
                reject(response.msg)
            }
        }).catch(e => {
            console.log(e)
        })
    })
}
http.postForm = function (url, data, options) {
    return new Promise((resolve, reject) => {
        instance.post(url, qs.stringify(data), options).then(response => {
            if (response.code === 200) {
                resolve(response)
            } else {
                reject(response.msg)
            }
        }).catch(e => {
            console.log(e)
        })
    })
}
http.getUrl = function (url, options) {
    return new Promise((resolve, reject) => {
        instance.get(url, options).then(response => {
            return resolve(response)
        }).catch(e => {
            reject(e)
        })
    })
}
export default http