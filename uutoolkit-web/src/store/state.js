export default{
    get UserToken(){
        return localStorage.getItem('authentication')
    },
    set UserToken(token){
        localStorage.setItem('authentication',token)
    }
}