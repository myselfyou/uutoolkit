import {userInfo} from '@/api'
export default{
    namespaced:true,
    state:{
        avatar:'',
        account:'',
        userId:'',
    },
    mutations:{
        SET_AVATAR(state,avatar){
            state.avatar=avatar;
        },
        SET_ACCOUNT (state,account){
            state.account=account;
        },
        SET_USERID (state,userId){
            state.userId=userId;
        }
    },
    actions:{
        async FETCH_USER(context){
            let result=await userInfo()
            context.commit("SET_AVATAR",result.data.avatar)
            context.commit("SET_ACCOUNT",result.data.username)
            context.commit("SET_USERID",result.data.id)
        }
    }
}