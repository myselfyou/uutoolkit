import menu from './modules/menu'
import childMenu from './modules/child-menu'
import permission from './modules/permission'
export default {
    menu,
    childMenu,
    permission
}