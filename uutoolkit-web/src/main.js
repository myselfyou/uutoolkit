import Vue from 'vue'
import App from './App.vue'
import store from '@/store'
import router from '@/router'
// import Antd from 'ant-design-vue';
// import 'ant-design-vue/dist/antd.css';
import '@/icons'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import '@/assets/text.css'
import { Menu, Input, Dropdown, Icon, FormModel, Button, Table, Card, Row, Col, Layout, Avatar, Upload, Pagination, Select, Modal, message, Tag, Tooltip, Popconfirm, InputNumber, Radio } from 'ant-design-vue';
import { Document } from 'postcss'
[Menu, Input, Dropdown, Icon, FormModel, Button, Table, Card, Row, Col, Layout, Avatar, Upload, Pagination, Select, Modal, Tag, Tooltip, Popconfirm, InputNumber, Radio].forEach(Component => Vue.use(Component))
// use
Vue.use(mavonEditor)
Vue.directive('title', {
    inserted: function (el, binding) {
        // document.title = el.dataset.title
    }
})

Vue.config.productionTip = false;

Vue.prototype.$confirm = Modal.confirm
Vue.prototype.$message = message

window.store = store
window.router = router
router.$login = function (isRedirect) {
    if (isRedirect) {
        this.push({ name: "login", query: { path: this.history.current.path } })
    } else {
        this.push({ name: "login" })
    }
}
router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title
        let head = document.getElementsByTagName('head')[0];
        for (let key of Object.keys(to.meta)) {
            if (key == "title") continue
            var ele = getElement(key)
            if (ele == null) {
                ele = document.createElement('meta');
                ele.name = key
                head.appendChild(ele)
            }
            ele.content = to.meta[key]
        }
    }
    if (!store.state.UserToken) {
        if (to.matched.length > 0 && !to.matched.some(record => record.meta.requiresAuth)) {
            next()
        } else {
            next({ path: '/login' })
        }
    } else {
        if (!store.state.permission.account || store.state.permission.account == '') {
            store.dispatch('permission/FETCH_USER').then(() => {
                next()
            })
        } else {
            if (to.path !== '/login') {
                next()
            } else {
                next(from.fullpath)
            }
        }
    }
})

router.afterEach((to) => {

})
function getElement(name) {
    var metas = document.getElementsByTagName(name)
    for (item of metas) {
        if (item.name == name) {
            return item
        }
    }
    return null
}
const vue = new Vue({
    router,
    store,
    render: h => h(App)
})
window.vue = vue
vue.$mount('#app')
