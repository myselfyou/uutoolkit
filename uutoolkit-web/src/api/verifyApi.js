import axios from '@/config/httpConfig'

export function verifyAccount(verifyCode) {
    return axios.postForm("/verify", { verifyCode })
}
export function getVerifyImg() {
    return axios.getUrl("/verify/code",{responseType:"arraybuffer"})
}
