import axios from '@/config/httpConfig'

export function getFeedbackList(data){
    return axios.post("/feedback/f/list",data)
}
export function addFeedback(data){
    return axios.post("/feedback/f/submit",data)
}