import axios from '@/config/httpConfig'

export function userLogin(data) {
    return axios.post("/f/login", data)
}
export function userInfo() {
    return axios.post("/user/f/info", null)
}
export function userRegister(data) {
    return axios.post("/f/register", data)
}
export function userForget(data) {
    return axios.post("/user/f/forget", data)
}
export function userForgetSet(data) {
    return axios.post("/user/f/forgetSet", data)
}
export function updateInfo(data) {
    return axios.post("/user/f/updateInfo", data)
}
export function updatePasswordAPI(password) {
    return axios.postForm("/user/f/updatePassword", {password})
}

export function toolCollectAPI(categoryCode) {
    return axios.postForm("/tool/collect/list", {categoryCode})
}