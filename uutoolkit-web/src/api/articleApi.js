import axios from '@/config/httpConfig'

export function getArticleList(data){
    return axios.post("/article/f/list",data)
}
export function getUserArticles(data){
    return axios.post("/article/getUserArticles",data)
}
export function getArticleAPI(id){
    return axios.get(`/article/f/get/${id}`)
}
export function submitArticle(data){
    return axios.post("/article/add",data)
}
export function updateArticle(data){
    return axios.post("/article/update",data)
}
export function deleteArticle(id){
    return axios.post(`/article/delete/${id}`,null)
}
//--------------------------ArticleCategory-------------------------
export function getArticleCategoryList() {
    return axios.post('/article/category/f/list',null)
}
//--------------------------ArticleComment-------------------------
export function getArticleCommentAPI(articleId,commentId) {
    return axios.postForm(`/article/comment/f/${articleId}`,{commentId})
}
export function getArticleReplyAPI(commentId,replyId) {
    return axios.postForm(`/article/reply/f/${commentId}`,{replyId})
}
export function submitArticleCommentAPI(data) {
    return axios.post(`/article/comment/submit`,data)
}
export function submitArticleReplyAPI(data) {
    return axios.post(`/article/reply/submit`,data)
}
export function deleteArticleCommentAPI(commentId) {
    return axios.post(`/article/comment/delete/${commentId}`)
}
export function deleteArticleReplyAPI(replyId) {
    return axios.post(`/article/reply/delete/${replyId}`)
}