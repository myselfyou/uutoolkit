import axios from '@/config/httpConfig'

export function searchToolsAPI(page) {
    return axios.post(`/search/f/tools`, page)
}
export function searchArticlesAPI(page) {
    return axios.post(`/search/f/articles`, page)
}
export function searchDiscussAPI(page) {
    return axios.post(`/search/f/discuss`, page)
}