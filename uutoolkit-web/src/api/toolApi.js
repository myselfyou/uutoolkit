import axios from '@/config/httpConfig'

export function getToolList(category) {
    return axios.postForm('/tool/f/list',{category})
}
export function toolCollectAPI(categoryCode) {
    return axios.postForm("/tool/collect/list", {categoryCode})
}
export function toolCollectFavoriteAPI(toolId) {
    return axios.postForm("/tool/collect/favorite", {toolId})
}