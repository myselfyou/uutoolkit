import axios from '@/config/httpConfig'

export function getDiscussList(data){
    return axios.post("/discuss/f/list",data)
}
export function getUserDiscussAPI(data){
    return axios.post("/discuss/getUserDiscuss",data)
}
export function getDiscussAPI(id){
    return axios.get(`/discuss/f/get/${id}`)
}
export function getDetailDiscussAPI(id){
    return axios.get(`/discuss/f/detail/${id}`)
}
export function deleteDiscussAPI(id){
    return axios.post(`/discuss/delete/${id}`)
}
export function addDiscussAPI(data){
    return axios.post(`/discuss/add`,data)
}
export function updateDiscussAPI(data){
    return axios.post(`/discuss/update`,data)
}


//--------------------------DiscussCategory-------------------------
export function getDiscussCategoryList() {
    return axios.post('/discuss/category/f/list',null)
}

//--------------------------DiscussComment-------------------------
export function getDiscussCommentAPI(discussId,commentId) {
    return axios.postForm(`/discuss/comment/f/${discussId}`,{commentId})
}
export function getDiscussReplyAPI(commentId,replyId) {
    return axios.postForm(`/discuss/reply/f/${commentId}`,{replyId})
}
export function submitDiscussCommentAPI(data) {
    return axios.post(`/discuss/comment/submit`,data)
}
export function submitDiscussReplyAPI(data) {
    return axios.post(`/discuss/reply/submit`,data)
}
export function deleteDiscussCommentAPI(commentId) {
    return axios.post(`/discuss/comment/delete/${commentId}`)
}
export function deleteDiscussReplyAPI(replyId) {
    return axios.post(`/discuss/reply/delete/${replyId}`)
}