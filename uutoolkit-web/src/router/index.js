import Vue from 'vue'
import Router from "vue-router"

Vue.use(Router)
export default new Router({
    mode: 'history',
    routes: [
        {
            name: "index",
            path: "/",
            component: () => import('@/pages/layout'),
            children: [
                {
                    path: "/",
                    name: 'home',
                    component: () => import("@/pages/home"),
                    children: [
                        {
                            path: "/",
                            name: 'tool-list',
                            meta: {
                                category: "all",
                                title: "UUToolkit在线工具",
                                keywords: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析",
                                description: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析"
                            },
                            component: () => import("@/pages/home/tool-list")
                        },
                        {
                            path: "/tools/:category",
                            name: 'tool-list',
                            meta: {
                                title: "UUToolkit在线工具",
                                keywords: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析",
                                description: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析"
                            },
                            component: () => import("@/pages/home/tool-list")
                        }
                    ]
                },
                {
                    path: "/article",
                    name: "article",
                    component: () => import("@/pages/article")
                },
                {
                    path: "/discuss",
                    name: "discuss",
                    component: () => import("@/pages/discuss")
                },
                {
                    path: "/search",
                    name: "search",
                    component: () => import("@/pages/search"),
                    children: [
                        {
                            path: "/search/tool",
                            name: "search-tool",
                            component: () => import("@/pages/search/tool"),
                        },
                        {
                            path: "/search/article",
                            name: "search-article",
                            component: () => import("@/pages/search/article"),
                        },
                        {
                            path: "/search/discuss",
                            name: "search-discuss",
                            component: () => import("@/pages/search/discuss"),
                        },
                    ]
                },
                {
                    path: "/article/add",
                    name: "article-add",
                    meta: {
                        title: "在线文章添加-UUToolkit在线工具",
                        keywords: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析",
                        description: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析"
                    },
                    component: () => import("@/pages/article/create")
                },
                {
                    path: "/article/edit/:id",
                    name: "article-edit",
                    component: () => import("@/pages/article/create")
                },
                {
                    path: "/article/detail/:id",
                    name: "article-detail",
                    component: () => import("@/pages/article/detail")
                },
                {
                    path: "/discuss/add",
                    name: "discuss-add",
                    meta: {
                        title: "在线话题添加-UUToolkit在线工具",
                        keywords: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析",
                        description: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析"
                    },
                    component: () => import("@/pages/discuss/create")
                },
                {
                    path: "/discuss/edit/:id",
                    name: "discuss-edit",
                    component: () => import("@/pages/discuss/create")
                },
                {
                    path: "/discuss/detail/:id",
                    name: "discuss-detail",
                    component: () => import("@/pages/discuss/detail")
                },
                {
                    path: "/feedback",
                    name: "feedback",
                    component: () => import("@/pages/feedback")
                },
                {
                    path: "/login",
                    name: "login",
                    meta: {
                        title: "登录-UUToolkit在线工具",
                        keywords: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析",
                        description: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析"
                    },
                    component: () => import("@/pages/login")
                },
                {
                    path: "/register",
                    name: "register",
                    meta: {
                        title: "注册-UUToolkit在线工具",
                        keywords: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析",
                        description: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析"
                    },
                    component: () => import("@/pages/register")
                },
                {
                    path: "/forget",
                    name: "forget",
                    meta: {
                        title: "忘记密码-UUToolkit在线工具",
                        keywords: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析",
                        description: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析"
                    },
                    component: () => import("@/pages/forget")
                },
                {
                    path: "/setpassword",
                    name: "setpassword",
                    meta: {
                        title: "设置密码-UUToolkit在线工具",
                        keywords: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析",
                        description: "在线,JSON,JSON 校验,格式化,xml转json 工具,在线工具,json视图,可视化,程序,服务器,域名注册,正则表达式,测试,在线json格式化工具,json 格式化,json格式化工具,json字符串格式化,json 在线查看器,json在线,json 在线验证,json tools online,在线文字对比工具,json解析"
                    },
                    component: () => import("@/pages/setpassword")
                },
                {
                    path: "/persion",
                    name: "persion",
                    component: () => import("@/pages/persion"),
                    children: [
                        {
                            path: "/persion/me",
                            name: "persion-me",
                            meta: {
                                requiresAuth: true
                            },
                            component: () => import("@/pages/persion/me")
                        },
                        {
                            path: "/persion/security",
                            name: "persion-security",
                            meta: {
                                requiresAuth: true
                            },
                            component: () => import("@/pages/persion/security")
                        },
                        {
                            path: "/persion/tools",
                            name: "persion-tools",
                            meta: {
                                requiresAuth: true
                            },
                            component: () => import("@/pages/persion/tools")
                        },
                        {
                            path: "/persion/article",
                            name: "persion-article",
                            meta: {
                                requiresAuth: true
                            },
                            component: () => import("@/pages/persion/article")
                        },
                        {
                            path: "/persion/discuss",
                            name: "persion-discuss",
                            meta: {
                                requiresAuth: true
                            },
                            component: () => import("@/pages/persion/discuss")
                        }
                    ]
                },
                {
                    path: "/tool",
                    name: "tools",
                    component: () => import("@/pages/tools"),
                    children: [
                        {
                            path: "/tool/textcontrast",
                            name: "textcontrast",
                            meta: {
                                title: "在线文本对比 -UUToolkit在线工具",
                                keywords: "在线文本对比,代码对比",
                                description: "本工具可以方便大家快速对比两个文本文件中的不同之处,可以自动对两段文本比较,标注不同之处,结果清晰明了"
                            },
                            component: () => import("@/pages/tools/textcontrast"),
                        },
                        {
                            path: "/tool/ascii",
                            name: "ascii",
                            meta: {
                                title: "文本转ASCII-UUToolkit在线工具",
                                keywords: "Html转文本转ASCII",
                                description: "ASCII编码在线转工具可以帮助你把中文转换成ASCII编码"
                            },
                            component: () => import("@/pages/tools/ascii"),
                        },
                        {
                            path: "/tool/pinyin",
                            name: "pinyin",
                            meta: {
                                title: "中文转拼音-UUToolkit在线工具",
                                keywords: "在线中文转拼音转语音",
                                description: "汉字,汉语,拼音,在线,转换... 在线汉字转拼音 请输入您要转换的汉字内容。"
                            },
                            component: () => import("@/pages/tools/pinyin"),
                        },
                        {
                            path: "/tool/rmb",
                            name: "rmb",
                            meta: {
                                title: "人民币大写在线转换工具-UUToolkit在线工具",
                                keywords: "人民币大写在线转换工具 在线将阿拉伯数字转换成中文大写工具 人民币大写转换工具",
                                description: "人民币大写在线转换工具"
                            },
                            component: () => import("@/pages/tools/rmb"),
                        },
                        {
                            path: "/tool/complex",
                            name: "complex",
                            meta: {
                                title: "在线繁体字转换器-UUToolkit在线工具",
                                keywords: "简体,繁体,简体转繁体,在线简体转繁体",
                                description: "在线繁体字转换器"
                            },
                            component: () => import("@/pages/tools/complex"),
                        },
                        {
                            path: "/tool/weight",
                            name: "weight",
                            meta: {
                                title: "重量计量单位转换-UUToolkit在线工具",
                                keywords: "在线重量计量单位转换工具,美担,英石,磅(Lb),盎司(Oz),打兰(Dr),格令,等各种重量单位间的换算",
                                description: "美担,英石,磅(Lb),盎司(Oz),打兰(Dr),格令,等各种重量单位间的换算"
                            },
                            component: () => import("@/pages/tools/weight"),
                        },
                        {
                            path: "/tool/relationship",
                            name: "relationship",
                            meta: {
                                desc: "家庭称谓"
                            },
                            component: () => import("@/pages/tools/relationship"),
                        },
                        {
                            path: "/tool/qqstatus",
                            name: "qqstatus",
                            meta: {
                                title: "QQ在线状态-UUToolkit在线工具",
                                keywords: "QQ电脑在线查询",
                                description: "QQ查询是否电脑在线,可以通过QQ交谈进行QQ会话"
                            },
                            component: () => import("@/pages/tools/qqstatus"),
                        },
                        {
                            path: "/tool/phone",
                            name: "phone",
                            meta: {
                                title: "手机号归属查询-UUToolkit在线工具",
                                keywords: "手机号归属查询",
                                description: "手机号归属查询-为你提供全面的手机号码归属地查询服务,电话号码归属地查询,包括移动手机号码,联通手机号码,固定号码归属地查询,电信手机号码等等"
                            },
                            component: () => import("@/pages/tools/phone"),
                        },
                        {
                            path: "/tool/measure",
                            name: "measure",
                            meta: {
                                title: "长度计量单位换算-UUToolkit在线工具",
                                keywords: "长度计量单位换算,长度换算器,度量衡计量单位换算转换器",
                                description: "长度计量单位换算-度量衡计量单位换算转换器 [长度换算器] 可实现在线公里(Km)、米(M)、分米(Dm)、厘米(Cm)、里、丈、尺、寸、分、厘、海里(Nmi)等"
                            },
                            component: () => import("@/pages/tools/measure"),
                        },
                        {
                            path: "/tool/ipaddress",
                            name: "ipaddress",
                            meta: {
                                title: "IP地址查询-UUToolkit在线工具",
                                keywords: "IP地址查询",
                                description: "IP地址查询"
                            },
                            component: () => import("@/pages/tools/ipaddress"),
                        },
                        {
                            path: "/tool/dns",
                            name: "dns",
                            meta: {
                                title: "DNS检测-UUToolkit在线工具",
                                keywords: "DNS检测",
                                description: "利用本工具能检测您本地的上网IP以及DNS相关信息，可用于判断DNS设置是否正确及是否遭到DNS劫持。"
                            },
                            component: () => import("@/pages/tools/dns"),
                        },
                        {
                            path: "/tool/ping",
                            name: "ping",
                            meta: {
                                title: "超级Ping-UUToolkit在线工具",
                                keywords: "超级Ping,ping,ip,网站",
                                description: "利用本工具能检测超级Ping"
                            },
                            component: () => import("@/pages/tools/ping"),
                        },
                        {
                            path: "/tool/port",
                            name: "port",
                            meta: {
                                title: "在线端口扫描-UUToolkit在线工具",
                                keywords: "在线端口扫描",
                                description: "在线端口扫描"
                            },
                            component: () => import("@/pages/tools/port"),
                        },
                        {
                            path: "/tool/rank",
                            name: "rank",
                            meta: {
                                title: "域名权重查询-UUToolkit在线工具",
                                keywords: "域名权重查询",
                                description: "域名权重查询"
                            },
                            component: () => import("@/pages/tools/rank"),
                        },
                        {
                            path: "/tool/statuscode",
                            name: "statuscode",
                            meta: {
                                title: "网站状态码查询-UUToolkit在线工具",
                                keywords: "网站状态码查询",
                                description: "网站状态码查询"
                            },
                            component: () => import("@/pages/tools/statuscode"),
                        },
                        {
                            path: "/tool/domain",
                            name: "domain",
                            meta: {
                                desc: "在线子域名扫描"
                            },
                            component: () => import("@/pages/tools/domain"),
                        },
                        {
                            path: "/tool/color",
                            name: "color",
                            meta: {
                                title: "颜色值转换-UUToolkit在线工具",
                                keywords: "颜色值转换",
                                description: "颜色值转换"
                            },
                            component: () => import("@/pages/tools/color"),
                        },
                        {
                            path: "/tool/code",
                            name: "code",
                            meta: {
                                title: "字符串加解密-UUToolkit在线工具",
                                keywords: "字符串加解密",
                                description: "字符串加解密"
                            },
                            component: () => import("@/pages/tools/code"),
                        },
                        {
                            path: "/tool/base64",
                            name: "base64",
                            meta: {
                                title: "图片base64-UUToolkit在线工具",
                                keywords: "图片base64,图片,base64,图片转base64",
                                description: "图片base64"
                            },
                            component: () => import("@/pages/tools/base64"),
                        },
                        {
                            path: "/tool/hex",
                            name: "hex",
                            component: () => import("@/pages/tools/hex"),
                        },
                        {
                            path: "/tool/unix",
                            name: "unix",
                            meta: {
                                title: "Unix时间戳-UUToolkit在线工具",
                                keywords: "Unix时间戳转换,时间戳转换工具",
                                description: "Unix时间戳转换可以把Unix时间转成北京时间。"
                            },
                            component: () => import("@/pages/tools/unix"),
                        },
                        {
                            path: "/tool/prop2yaml",
                            name: "prop2yaml",
                            meta: {
                                title: "properties转yaml-UUToolkit在线工具",
                                keywords: "properties转yaml",
                                description: "properties转yaml"
                            },
                            component: () => import("@/pages/tools/prop2yaml"),
                        },
                        {
                            path: "/tool/textcount",
                            name: "textcount",
                            component: () => import("@/pages/tools/textcount"),
                        },
                        {
                            path: "/tool/html",
                            name: "html",
                            meta: {
                                title: "JS/HTML压缩、格式化-UUToolkit在线工具",
                                keywords: "JS/HTML在线格式化,JS/HTML在线压缩,JS/HTML格式化,JS/HTML压缩",
                                description: "可以对JavaScript、HTML进行格式化排版，整齐的进行显示。可以对JavaScript、HTML进行加密，加密压缩。"
                            },
                            component: () => import("@/pages/tools/html"),
                        },
                        {
                            path: "/tool/css",
                            name: "css",
                            meta: {
                                title: "在线CSS压缩、格式化-UUToolkit在线工具",
                                keywords: "css在线格式化,css在线压缩,css格式化,css压缩",
                                description: "可以对css进行格式化排版，整齐的进行显示。可以对css进行加密，加密压缩。"
                            },
                            component: () => import("@/pages/tools/css"),
                        },
                        {
                            path: "/tool/xml",
                            name: "xml",
                            meta: {
                                title: "在线XML格式化、压缩-UUToolkit在线工具",
                                keywords: "XML在线格式化,XML在线压缩,XML格式化,XML压缩,在线工具",
                                description: "可以对XML进行格式化排版，整齐的进行显示。可以对XML压缩。"
                            },
                            component: () => import("@/pages/tools/xml"),
                        },
                        {
                            path: "/tool/cacl",
                            name: "cacl",
                            meta: {
                                title: "计算器在线计算-UUToolkit在线工具",
                                keywords: "计算器在线计算,计算机,在线计算器,计算器,计算器下载,科学计算器,计算器在线使用,语音计算器,在线计算器使用",
                                description: "计算器在线计算,计算机,在线计算器,计算器,计算器下载,科学计算器,计算器在线使用"
                            },
                            component: () => import("@/pages/tools/cacl"),
                        },
                    ]
                },
            ]
        },
        {
            path: "/verify",
            name: "verify",
            component: () => import("@/pages/verify")
        }
    ]
})