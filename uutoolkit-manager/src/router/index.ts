import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: '/',
        name: 'container',
        component: () => import('@/pages/layout/index.vue'),
        redirect: '/permission/admin',
        children: [
            {
                path: '/permission',
                name: 'permission',
                component: () => import("@/pages/permissions/index.vue"),
                redirect: '/permission/admin',
                meta:{
                    title:'权限管理',
                    requireAuth:true
                },
                children: [
                    {
                        path: '/permission/admin',
                        name: 'permission-admin',
                        meta:{
                            title:'后台用户列表',
                            requireAuth:true
                        },
                        component: () => import("@/pages/permissions/admin/index.vue")
                    }
                ]
            },
            {
                path: '/user',
                name: 'user',
                component: () => import("@/pages/users/index.vue"),
                redirect: '/permission/admin',
                meta:{
                    title:'用户管理',
                    requireAuth:true
                },
                children: [
                    {
                        path: '/user/list',
                        name: 'user-list',
                        meta:{
                            title:'用户列表',
                            requireAuth:true
                        },
                        component: () => import("@/pages/users/user-list/index.vue")
                    }
                ]
            },
            {
                path: '/tools',
                name: 'tools',
                component: () => import("@/pages/tools/index.vue"),
                redirect: '/tools/list',
                meta:{
                    title:'工具管理',
                    requireAuth:true
                },
                children: [
                    {
                        path: '/tools/list',
                        name: 'tools-list',
                        meta:{
                            title:'工具列表',
                            requireAuth:true
                        },
                        component: () => import("@/pages/tools/tool-list/index.vue")
                    },{
                        path: '/tools/category',
                        name: 'tools-category',
                        meta:{
                            title:'工具分类',
                            requireAuth:true
                        },
                        component: () => import("@/pages/tools/tool-category/index.vue")
                    },{
                        path: '/tools/comment',
                        name: 'tools-comment',
                        meta:{
                            title:'用户评论',
                            requireAuth:true
                        },
                        component: () => import("@/pages/tools/tool-comment/index.vue")
                    },
                ]
            },
            {
                path: '/article',
                name: 'article',
                component: () => import("@/pages/article/index.vue"),
                redirect: '/article/list',
                meta:{
                    title:'文章管理',
                    requireAuth:true
                },
                children: [
                    {
                        path: '/article/list',
                        name: 'article-list',
                        meta:{
                            title:'文章列表',
                            requireAuth:true
                        },
                        component: () => import("@/pages/article/article-list/index.vue")
                    },{
                        path: '/article/category',
                        name: 'article-category',
                        meta:{
                            title:'文章分类',
                            requireAuth:true
                        },
                        component: () => import("@/pages/article/article-category/index.vue")
                    },{
                        path: '/article/comment',
                        name: 'article-comment',
                        meta:{
                            title:'用户评论',
                            requireAuth:true
                        },
                        component: () => import("@/pages/article/article-comment/index.vue")
                    },
                ]
            },
            {
                path: '/discuss',
                name: 'discuss',
                component: () => import("@/pages/discuss/index.vue"),
                redirect: '/discuss/list',
                meta:{
                    title:'话题管理',
                    requireAuth:true
                },
                children: [
                    {
                        path: '/discuss/list',
                        name: 'discuss-list',
                        meta:{
                            title:'话题列表',
                            requireAuth:true
                        },
                        component: () => import("@/pages/discuss/discuss-list/index.vue")
                    }, {
                        path: '/discuss/category',
                        name: 'discuss-category',
                        meta:{
                            title:'话题分类',
                            requireAuth:true
                        },
                        component: () => import("@/pages/discuss/discuss-category/index.vue")
                    }, {
                        path: '/discuss/comment',
                        name: 'discuss-comment',
                        meta:{
                            title:'用户评论',
                            requireAuth:true
                        },
                        component: () => import("@/pages/discuss/discuss-comment/index.vue")
                    },
                ]
            },{
                path: '/feedback',
                name: 'feedback',
                component: () => import("@/pages/feedback/index.vue"),
                redirect: '/feedback/list',
                meta:{
                    title:'反馈',
                    requireAuth:true
                },
                children: [
                    {
                        path: '/feedback/list',
                        name: 'feedback-list',
                        meta:{
                            title:'反馈列表',
                            requireAuth:true
                        },
                        component: () => import("@/pages/feedback/feedback-list/index.vue")
                    },
                ]
            },
        ]
    },{
        path:'/login',
        name:'login',
        meta:{
            title:'登录'
        },
        component:()=>import("@/pages/login/index.vue")
    }
]
export const menus=routes[0].children
const router = new VueRouter({
    mode: "history",
    routes
})
export default router