interface User{
    account:string,
    avatar:string
}
interface LoadingListener{
    onLoading(isLoading:boolean):void
}
export {User,LoadingListener}