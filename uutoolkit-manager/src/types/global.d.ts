import Vue from 'vue';
import { Store } from 'vuex';

import type { User, LoadingListener } from '@/types/model'
import type { IRootState } from '@/store/store'
import type { Secret } from '@/utils/secret';
import type { BaseResponse } from '@/model/BaseResponse';
import type BaseVue from '@/BaseVue';


import axios from 'axios'
// declare  module 'axios/index'{
//     interface AxiosRequestConfig{
//         isShowLoading:boolean
//     }
// }
declare global {
    interface Window {
        store: Store;
        router: VueRouter;
        vue: Vue
        loadListener: LoadingListener;
    }
}