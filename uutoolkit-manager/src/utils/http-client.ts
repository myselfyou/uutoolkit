import { BaseResponse } from '@/model/base-reponse'
import store from '@/store'
import { message, Spin } from 'ant-design-vue'
import axios, { AxiosRequestConfig } from 'axios'
import qs from 'qs'
interface Request {
    url: string,
    config?: AxiosRequestConfig,
    isLoading: boolean
}
export class HttpClient {
    instance = axios.create({
        timeout: 10000,
        baseURL: process.env.VUE_APP_BASE_API,
    })
    public constructor() {
        //添加請求攔截器
        this.instance.interceptors.request.use(
            function (config: any) {
                if (store.state.UserToken) {
                    config.headers.Authorization = `Bearer ${store.state.UserToken}`
                    config.headers.Flag = 2
                }
                return config
            },
            function (error) {
                return Promise.reject(error)
            }
        )
        this.instance.interceptors.response.use(response => {
            if (response.data.code != null && response.data.code === 401) {
                window.store.commit('LOGIN_OUT')
                window.vue.$confirm({
                    title: '请重新登录?',
                    content: '1s将会重新进入登录页面',
                    onOk() {
                        window.location.reload()
                    },
                    onCancel() {
                        window.location.reload()
                    },
                });
            } else {
                return response.data
            }
        }, err => {
            return Promise.reject(err.response)
        })
    }
    get(url: string, options?: AxiosRequestConfig) {
        if (window.loadListener.onLoading != null) {
            window.loadListener.onLoading(true)
        }
        return new Promise<BaseResponse>((resolve, reject) => {
            this.instance.get<any, BaseResponse>(url, options).then(response => {
                if (window.loadListener.onLoading != null) {
                    window.loadListener.onLoading(false)
                }
                if (response.code === 1 || response.code === 200) {
                    if (response.msg) {
                        message.success(response.msg)
                    }
                    resolve(response.data)
                } else {
                    message.error(response.msg!)
                    reject(response.msg)
                }
            }).catch(e => {
                if (window.loadListener.onLoading != null) {
                    window.loadListener.onLoading(false)
                }
            })
        })
    }
    post(url: string, data: any, options?: AxiosRequestConfig) {
        if (window.loadListener.onLoading != null) {
            window.loadListener.onLoading(true)
        }
        return new Promise<any>((resolve, reject) => {
            this.instance.post<any, BaseResponse>(url, data, options).then(response => {
                if (window.loadListener.onLoading != null) {
                    window.loadListener.onLoading(false)
                }
                if (response.code === 1 || response.code === 200) {
                    if (response.msg) {
                        message.success(response.msg)
                    }
                    resolve(response.data)
                } else {
                    message.error(response.msg!)
                    reject(response.msg)
                }
            }).catch(e => {
                if (window.loadListener.onLoading != null) {
                    window.loadListener.onLoading(false)
                }
            })
        })
    }

    postForm(url: string, data: any, options?: AxiosRequestConfig) {
        return new Promise((resolve, reject) => {
            if (window.loadListener.onLoading != null) {
                window.loadListener.onLoading(true)
            }
            this.instance.post<any, BaseResponse>(url, qs.stringify(data), options).then(response => {
                if (window.loadListener.onLoading != null) {
                    window.loadListener.onLoading(false)
                }
                if (response.code === 1 || response.code === 200) {
                    if (response.msg) {
                        message.success(response.msg)
                    }
                    resolve(response.data)
                } else {
                    message.error(response.msg!)
                    reject(response.msg)
                }
            }).catch(e => {
                if (window.loadListener.onLoading != null) {
                    window.loadListener.onLoading(false)
                }
            })
        })
    }
}
export default new HttpClient()