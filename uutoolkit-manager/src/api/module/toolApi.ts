import axios from '@/utils/http-client'

export function getToolList(data:any):any {
    return axios.post('/tool/b/list',data)
}
export function addTool(data:any) {
    return axios.post('/tool/b/add',data)
}
export function updateTool(data:any) {
    return axios.post('/tool/b/update',data)
}
export function deleteTool(idStr:string){
    return axios.postForm(`/tool/b/delete`,{idStr})
}