import axios from '@/utils/http-client'

export function getArticleList(data:any){
    return axios.post("/article/b/list",data)
}
export function deleteArticle(id:Number){
    return axios.post("/article/b/delete/{id}",null)
}