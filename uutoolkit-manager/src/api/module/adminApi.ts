import axios from '@/utils/http-client'
// 获取用户列表
export function getUserList(data:any):any {
    return axios.post('/admin/b/list',data)
}
// 添加用户
export function addUser(data:any) {
    return axios.post('/admin/b/add',data)
}
// 修改用户
export function updateUser(data:any) {
    return axios.post('/admin/b/update',data)
}
// 修改用户狀態
export function changeUserStatus(id:Number,status:Number) {
    return axios.postForm(`/admin/b/changeStatus/${id}`,{status})
}
export function deleteUser(idStr:string){
    return axios.postForm(`/admin/b/delete`,{idStr})
}
