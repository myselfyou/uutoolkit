import axios from '@/utils/http-client'

// 获取用户列表
export function getFUserList(data:any):any {
    return axios.post('/user/b/list',data)
}
// 添加用户
export function addFUser(data:any) {
    return axios.post('/user/b/add',data)
}
// 修改用户
export function updateFUser(data:any) {
    return axios.post('/user/b/update',data)
}
// 修改用户狀態
export function changeFUserStatus(id:Number,status:Number) {
    return axios.postForm(`/user/b/changeStatus/${id}`,{status})
}
export function deleteFUser(idStr:string){
    return axios.postForm(`/user/b/delete`,{idStr})
}