import axios from '@/utils/http-client'

export function getToolCategoryList():any {
    return axios.post('/tool/category/b/list',null)
}
export function addToolCategory(data:any) {
    return axios.post('/tool/category/b/add',data)
}
export function updateToolCategory(data:any) {
    return axios.post('/tool/category/b/update',data)
}
export function deleteToolCategory(idStr:string){
    return axios.postForm(`/tool/category/b/delete`,{idStr})
}