import axios from '@/utils/http-client'

export function getArticleCategoryList():any {
    return axios.post('/article/category/b/list',null)
}
export function addArticleCategory(data:any) {
    return axios.post('/article/category/b/add',data)
}
export function updateArticleCategory(data:any) {
    return axios.post('/article/category/b/update',data)
}
export function deleteArticleCategory(idStr:string){
    return axios.postForm(`/article/category/b/delete`,{idStr})
}