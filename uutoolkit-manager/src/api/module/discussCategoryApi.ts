import axios from '@/utils/http-client'

export function getDiscussCategoryList():any {
    return axios.post('/discuss/category/b/list',null)
}
export function addDiscussCategory(data:any) {
    return axios.post('/discuss/category/b/add',data)
}
export function updateDiscussCategory(data:any) {
    return axios.post('/discuss/category/b/update',data)
}
export function deleteDiscussCategory(idStr:string){
    return axios.postForm(`/discuss/category/b/delete`,{idStr})
}