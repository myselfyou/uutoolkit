export class BaseResponse {
    code?: Number
    msg?: string
    data: any
}