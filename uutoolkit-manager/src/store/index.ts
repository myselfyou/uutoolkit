import Vue from 'vue'
import Vuex from 'vuex'
import { IRootState } from './store'

Vue.use(Vuex)

export default new Vuex.Store<IRootState>({
    state: {
        set UserToken(value: string | null) {
            if (value != null) {
                console.log(value)
                localStorage.setItem('token', value)
            }
        },
        get UserToken(): string | null {
            return localStorage.getItem('token')
        },
        user: {
            account: '',
            avatar: ''
        },
    },
    getters: {
        getUserToken(): string | null {
            return localStorage.getItem('token')
        },
    },
    mutations: {
        LOGIN_IN(state, token) {
            state.UserToken = token;
        },
        LOGIN_OUT(state) {
            state.UserToken = '';
        }
    },
    actions: {

    },
    modules: {

    }
})