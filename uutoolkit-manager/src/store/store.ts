import type { User } from '@/types/model';
interface IRootState {
    user: User | null;
    UserToken:string|null
}
export { IRootState }