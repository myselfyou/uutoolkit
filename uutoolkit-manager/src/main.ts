console.log("Hello World!");
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/icons'
import { Layout, Avatar, Dropdown, Menu, Icon, FormModel, Button, Input, Table, Divider, Tag, Spin, Modal, Col, Row, Select, Drawer, DatePicker, Switch, Radio, Upload, message, InputNumber } from 'ant-design-vue';
[Layout, Avatar, Dropdown, Menu, Icon, FormModel, Button, Input, Table, Divider, Tag, Spin, Modal, Col, Row, Select, Drawer, DatePicker, Switch, Radio, Upload,InputNumber].forEach(Component => Vue.use(Component))

Vue.config.productionTip = false
Vue.prototype.$confirm = Modal.confirm
Vue.prototype.$message = message

window.store = store
window.router = router


router.beforeEach((to, from, next: any) => {
    if (!store.state.UserToken) {
        if (to.matched.length > 0 && !to.matched.some(record => record.meta.requireAuth)) {
            console.log(store.state.UserToken + "==========")
            next()
        } else {
            next({ path: '/login' })
        }
    } else {
        if (to.path !== '/login') {
            next()
        } else {
            next({ path: '/' })
        }
    }
})
router.afterEach((to) => {

})

const vue = new Vue({
    router,
    store,
    render: h => h(App)
});
window.vue = vue
vue.$mount('#app')

