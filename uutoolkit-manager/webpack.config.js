// Generated using webpack-cli https://github.com/webpack/webpack-cli

const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const { VueLoaderPlugin } = require('vue-loader');
const TerserPlugin = require("terser-webpack-plugin");
const ZipPlugin = require('zip-webpack-plugin')

const isProduction = process.env.NODE_ENV == "production";

const stylesHandler = "style-loader";
function resolve(dir) {
  return path.resolve(__dirname, dir);
}
function getBaseAPI() {
  if (isProduction) {
    return '/api'
  } else {
    return '/api'
  }
}
const config = {
  entry: "./src/main.ts",
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "/"
  },
  devServer: {
    open: false,
    host: "localhost",
    historyApiFallback: {
      index: '/index.html'//该路径为打包后的首页路径及dist目录下的index.html页面
    },
    proxy: {
      '/api': {
        target: 'http://192.168.10.100:8085',
        changeOrigin: true
      },
      '/uutools/uploads': {
        target: 'http://192.168.10.100:8085',
        changeOrigin: true
      }
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'public', "index.html"),
      favicon: path.resolve(__dirname, 'public', "icon.ico"),
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
    new VueLoaderPlugin(),
    new ZipPlugin({
      path: resolve('./'),
      filename: 'dist.zip'
    }),
    new webpack.DefinePlugin({
      'process.env.VUE_APP_BASE_API': JSON.stringify(getBaseAPI()),
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
    // Add your plugins here
    // Learn more about plugins from https://webpack.js.org/configuration/plugins/
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          "babel-loader",
          {
            loader: "ts-loader",
            options: {
              appendTsSuffixTo: [/\.vue$/]
            }
          }
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [stylesHandler, "css-loader", "postcss-loader", "sass-loader"],
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.(eot|ttf|woff|woff2|png|jpg|gif)$/i,
        type: "asset",
      },
      {
        exclude: /node_modules/,
        test: /\.vue$/,
        use: 'vue-loader',
      },
      {
        test: /\.svg$/,
        loader: 'svg-sprite-loader',
        include: [resolve('src/icons')],
        options: {
          symbolId: 'icon-[name]'
        }
      },
    ]
  },
  resolve: {
    extensions: ['.ts', '.vue', '.json', ".js", '.png', ".sass", ".css"],
    alias: {
      '@': path.join(__dirname, '.', 'src'),
      process: "process/browser"
    }
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
    minimizer: [
      // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
      // `...`,
      new CssMinimizerPlugin(),
      new TerserPlugin()
    ],
  },
};

module.exports = () => {
  if (isProduction) {
    config.mode = "production";
  } else {
    config.mode = "development";
    config.devtool = 'inline-source-map';
  }
  return config;
};
