package com.newland.uutools.config.security;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.newland.uutools.entity.Admin;
import com.newland.uutools.mapper.AdminMapper;
import com.newland.uutools.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("userDetailService")
public class UserDetailsServiceImpl implements IUserDetailsService {
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Admin user = adminMapper.selectOne(Wrappers.<Admin>lambdaQuery().select(Admin::getUsername).eq(Admin::getUsername, username));
            if (user == null) return null;
            return new User(username, "", new ArrayList<>());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username, int flag) throws UsernameNotFoundException {
        if (flag == 1) {
            try {
                com.newland.uutools.entity.User user = userMapper.selectOne(Wrappers.<com.newland.uutools.entity.User>lambdaQuery().select(com.newland.uutools.entity.User::getId,com.newland.uutools.entity.User::getUsername).eq(com.newland.uutools.entity.User::getUsername, username));
                if (user == null) return null;
                SecurityUser securityUser=new SecurityUser(username,"", new ArrayList<>());
                securityUser.setUserId(user.getId());
                return securityUser;
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        } else {
            return loadUserByUsername(username);
        }
    }
}
