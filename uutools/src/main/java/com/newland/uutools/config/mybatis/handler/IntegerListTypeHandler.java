package com.newland.uutools.config.mybatis.handler;

public class IntegerListTypeHandler extends ListTypeHandler<Integer> {

	@Override
	Integer parseString(String value) {
		return Integer.parseInt(value);
	}
}
