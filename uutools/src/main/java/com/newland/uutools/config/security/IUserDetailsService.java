package com.newland.uutools.config.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface IUserDetailsService extends UserDetailsService {
    /**
     * @param username 用戶名
     * @param flag 1 後台 2前台用戶
     * @return
     * @throws UsernameNotFoundException
     */
    UserDetails loadUserByUsername(String username,int flag) throws UsernameNotFoundException ;
}
