package com.newland.uutools.handler;

import com.newland.uutools.constant.ResultCode;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.utils.JsonUtils;
import lombok.extern.log4j.Log4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@RestControllerAdvice
@Order(value = Ordered.HIGHEST_PRECEDENCE)
@Log4j
public class GlobalExceptionHandler {
    @ExceptionHandler({RuntimeException.class, Exception.class})
    public ResponseEntity handleException(Exception e) {
        log.error("系统内部异常，异常信息：", e);
        String message = "系统内部异常，异常信息";
        return ResponseEntity.error(ResultCode.INTERNAL_SERVER_ERROR.getCode(), message);
    }
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public ResponseEntity handleNotSupportException(Exception e) {
        String message = "不支持该请求类型";
        return ResponseEntity.error(ResultCode.ERROR.getCode(), message);
    }
    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity handleAccessDeniedException(AccessDeniedException e){
        log.error("未认证，请在前端系统进行认证：", e);
        String message = "未认证，请在前端系统进行认证";
        return ResponseEntity.error(ResultCode.UN_LOGIN.getCode(), message);
    }

    @ExceptionHandler({BusinessException.class})
    public ResponseEntity handleBusinessException(BusinessException e) {
        log.error(e.getMessage());
        return ResponseEntity.error(e.getCode(), e.getMessage());
    }
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity handleValidateException(MethodArgumentNotValidException e) {
        return ResponseEntity.error(e.getBindingResult().getFieldErrors().get(0).getDefaultMessage());
    }
}
