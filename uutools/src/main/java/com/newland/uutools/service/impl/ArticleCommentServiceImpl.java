package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.newland.uutools.entity.*;
import com.newland.uutools.entity.vo.ArticleCommentDataVO;
import com.newland.uutools.entity.vo.ArticleCommentVO;
import com.newland.uutools.entity.vo.DiscussCommentDataVO;
import com.newland.uutools.entity.vo.DiscussCommentVO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.ArticleCommentMapper;
import com.newland.uutools.mapper.ArticleReplyMapper;
import com.newland.uutools.mapper.DiscussReplyMapper;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IArticleCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 文章评论 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-02-08 14:14:30
 */
@Service
public class ArticleCommentServiceImpl extends ServiceImpl<ArticleCommentMapper, ArticleComment> implements IArticleCommentService {
    @Autowired
    private ArticleReplyMapper articleReplyMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public ArticleCommentDataVO list(Long discussId, Long commentId) {
        Integer count = baseMapper.selectCount(Wrappers.emptyWrapper());
        List<ArticleCommentVO> list = baseMapper.selectComments(discussId, commentId, 10);
        list.forEach(item -> item.setReplyList(articleReplyMapper.selectReplys(item.getId(), null, 3)));
        ArticleCommentDataVO commentDataVO = new ArticleCommentDataVO();
        commentDataVO.setCount(count);
        commentDataVO.setList(list);
        return commentDataVO;
    }

    @Override
    public List<ArticleReply> replyList(Long commentId, Long replyId) {
        return articleReplyMapper.selectReplys(commentId, replyId, 10);
    }

    @Override
    public void comment(ArticleComment discussComment) {
        User user = userMapper.selectById(discussComment.getFromId());
        discussComment.setFromName(user.getNickname());
        discussComment.setFromAvatar(user.getAvatar());
        baseMapper.insert(discussComment);
    }

    @Transactional
    @Override
    public void reply(ArticleReply reply) {
        ArticleComment comment = baseMapper.selectCommentForUpdate(reply.getCommentId());
        if (comment == null) {
            throw new BusinessException("该评论已删除");
        }
        User from = userMapper.selectById(reply.getFromId());
        User to = userMapper.selectById(reply.getToId());
        if (to == null) {
            throw new BusinessException("回复用户不存在");
        }
        reply.setFromAvatar(from.getAvatar());
        reply.setFromName(from.getNickname());
        reply.setToAvatar(to.getAvatar());
        reply.setToName(to.getNickname());
        articleReplyMapper.insert(reply);
        comment.setToCount(comment.getToCount() + 1);
        baseMapper.updateById(comment);
    }


    @Override
    public void deleteReply(Long replyId) {
        ArticleReply reply = articleReplyMapper.selectById(replyId);
        if (reply == null) {
            throw new BusinessException("删除回复不存在");
        }
        articleReplyMapper.deleteById(replyId);
    }

    @Transactional
    @Override
    public void deleteComment(Long commentId) {
        ArticleComment comment = baseMapper.selectCommentForUpdate(commentId);
        if (comment == null) {
            throw new BusinessException("删除评论不存在");
        }
        baseMapper.deleteById(commentId);
        comment.setToCount(Math.max(comment.getToCount() - 1, 0));
        baseMapper.updateById(comment);
    }
}
