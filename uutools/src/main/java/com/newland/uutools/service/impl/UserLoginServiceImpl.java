package com.newland.uutools.service.impl;

import com.newland.uutools.entity.User;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IUserLoginService;
import com.newland.uutools.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserLoginServiceImpl implements IUserLoginService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public String login(String username, String password) {
        User user = userMapper.selectUserByUsername(username);
        if (user == null) {
            throw new BusinessException("账户不存在");
        }
        if (passwordEncoder.matches(password, user.getPassword())) {
            if (user.getStatus() == 0) {
                throw new BusinessException("账户还未激活");
            } else if (user.getStatus() == 2) {
                throw new BusinessException("账户已锁定");
            }
            user.setLoginTime(LocalDateTime.now().withNano(0));
            userMapper.updateById(user);
            return JwtTokenUtil.createToken(username);
        }
        return null;
    }
}
