package com.newland.uutools.service;

import com.newland.uutools.entity.DiscussCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 话题分类 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IDiscussCategoryService extends IService<DiscussCategory> {

    List<DiscussCategory> getCategoryList();

    void add(DiscussCategory category);

    void delete(List<Long> ids);

    void updateItem(DiscussCategory category);
}
