package com.newland.uutools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.entity.BlogArticle;
import com.newland.uutools.entity.Discuss;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.dto.DiscussDTO;
import com.newland.uutools.entity.dto.DiscussPageDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.DiscussDetailVO;
import com.newland.uutools.entity.vo.DiscussVO;

/**
 * <p>
 * 话题 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IDiscussService extends IService<Discuss> {

    IPage<Discuss> getList(DiscussPageDTO page);

    DiscussVO getDiscuss(Long id);

    void add(DiscussDTO discuss);

    void updateItem(Long userId,DiscussDTO discuss);

    void delete(Long userId,Long id);

    DiscussDetailVO getDetailDiscuss(Long id);
}
