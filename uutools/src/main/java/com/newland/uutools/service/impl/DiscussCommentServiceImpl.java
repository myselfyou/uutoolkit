package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newland.uutools.entity.DiscussComment;
import com.newland.uutools.entity.DiscussReply;
import com.newland.uutools.entity.User;
import com.newland.uutools.entity.vo.DiscussCommentDataVO;
import com.newland.uutools.entity.vo.DiscussCommentVO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.DiscussCommentMapper;
import com.newland.uutools.mapper.DiscussReplyMapper;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IDiscussCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 话题评论 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class DiscussCommentServiceImpl extends ServiceImpl<DiscussCommentMapper, DiscussComment> implements IDiscussCommentService {
    @Autowired
    private DiscussReplyMapper discussReplyMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public DiscussCommentDataVO list(Long discussId, Long commentId) {
        Integer count = baseMapper.selectCount(Wrappers.emptyWrapper());
        List<DiscussCommentVO> list = baseMapper.selectDiscussComments(discussId, commentId, 10);
        list.forEach(item -> item.setReplyList(discussReplyMapper.selectDiscussReplys(item.getId(), null, 3)));
        DiscussCommentDataVO commentDataVO = new DiscussCommentDataVO();
        commentDataVO.setCount(count);
        commentDataVO.setList(list);
        return commentDataVO;
    }

    @Override
    public List<DiscussReply> replyList(Long commentId, Long replyId) {
        return discussReplyMapper.selectDiscussReplys(commentId, replyId, 10);
    }

    @Override
    public void comment(DiscussComment discussComment) {
        User user = userMapper.selectById(discussComment.getFromId());
        discussComment.setFromName(user.getNickname());
        discussComment.setFromAvatar(user.getAvatar());
        baseMapper.insert(discussComment);
    }

    @Transactional
    @Override
    public void reply(DiscussReply discussReply) {
        DiscussComment discussComment = baseMapper.selectCommentForUpdate(discussReply.getCommentId());
        if (discussComment == null) {
            throw new BusinessException("该评论已删除");
        }
        User from = userMapper.selectById(discussReply.getFromId());
        User to = userMapper.selectById(discussReply.getToId());
        if (to == null) {
            throw new BusinessException("回复用户不存在");
        }
        discussReply.setFromAvatar(from.getAvatar());
        discussReply.setFromName(from.getNickname());
        discussReply.setToAvatar(to.getAvatar());
        discussReply.setToName(to.getNickname());
        discussReplyMapper.insert(discussReply);
        discussComment.setToCount(discussComment.getToCount() + 1);
        baseMapper.updateById(discussComment);
    }


    @Override
    public void deleteReply(Long replyId) {
        DiscussReply discussReply = discussReplyMapper.selectById(replyId);
        if (discussReply == null) {
            throw new BusinessException("删除回复不存在");
        }
        discussReplyMapper.deleteById(replyId);
    }

    @Transactional
    @Override
    public void deleteComment(Long commentId) {
        DiscussComment discussComment = baseMapper.selectCommentForUpdate(commentId);
        if (discussComment == null) {
            throw new BusinessException("删除评论不存在");
        }
        baseMapper.deleteById(commentId);
        discussComment.setToCount(Math.max(discussComment.getToCount() - 1, 0));
        baseMapper.updateById(discussComment);
    }

}
