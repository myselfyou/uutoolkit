package com.newland.uutools.service.impl;

import com.newland.uutools.entity.Picture;
import com.newland.uutools.entity.Upload;
import com.newland.uutools.manager.FileManager;
import com.newland.uutools.mapper.UploadMapper;
import com.newland.uutools.service.IUploadService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newland.uutools.utils.DateUtils;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * <p>
 * 图片表 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-30 21:18:55
 */
@Service
public class UploadServiceImpl extends ServiceImpl<UploadMapper, Upload> implements IUploadService {
    @Autowired
    private FileManager fileManager;

    @Override
    public String uploadPicture(MultipartFile file) {
        String ymd = DateUtils.formatYMD(new Date());

        File folder = new File(fileManager.getImgPath() + ymd);
        String imgpath = fileManager.getPrefix() + ymd;
        if (!folder.exists()) {
            folder.mkdirs();
        }
        String originalFilename = file.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String img = String.format("%d%s", System.currentTimeMillis(), suffix);
        try {
            File picFile = new File(folder.getCanonicalFile(), img);
            Thumbnails.of(file.getInputStream()).scale(1.00f)   //压缩尺寸 范围（0.00--1.00）
                    .outputQuality(0.50f).toFile(picFile);
            String path = imgpath + "/" + img;
            baseMapper.insert(new Upload(path));
            return path;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
