package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newland.uutools.entity.Tool;
import com.newland.uutools.entity.ToolCategory;
import com.newland.uutools.entity.User;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.ToolLVO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.ToolCategoryMapper;
import com.newland.uutools.mapper.ToolMapper;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IToolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 工具表 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class ToolServiceImpl extends ServiceImpl<ToolMapper, Tool> implements IToolService {
    @Autowired
    private ToolCategoryMapper toolCategoryMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public IPage<ToolLVO> getList(PageDTO pageDTO) {
        Page<ToolLVO> page = new Page<>(pageDTO.getPageNumber(), pageDTO.getPageSize());
        page.addOrder(OrderItem.desc("id"));
        return baseMapper.selectToolLPage(page);
    }

    @Override
    public void add(Tool tool) {
        Tool dbTool = baseMapper.selectOne(Wrappers.<Tool>lambdaQuery().eq(Tool::getName, tool.getName()));
        if (dbTool != null) {
            throw new BusinessException("已经存在指定工具");
        }
        baseMapper.insert(tool);
    }

    @Override
    public void delete(List<Long> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public void updateItem(Tool category) {
        Tool dbItem = baseMapper.selectById(category.getId());
        if (dbItem == null) {
            throw new BusinessException("指定类名不存在");
        }
        Tool dbTool = baseMapper.selectOne(Wrappers.<Tool>lambdaQuery().eq(Tool::getName, category.getName()));
        if (dbTool != null && dbTool.getId() != dbItem.getId()) {
            throw new BusinessException("已经存在指定工具");
        }
        baseMapper.updateById(category);
    }

    @Override
    public List<ToolLVO> getTools(Long userId, String category) {
        ToolCategory toolCategory = toolCategoryMapper.selectOne(Wrappers.<ToolCategory>lambdaQuery().select(ToolCategory::getId).eq(ToolCategory::getCode, category));
        if (userId != null) {
            return baseMapper.selectToolsWithLogin(toolCategory != null ? toolCategory.getId() : null, userId);
        }
        return baseMapper.selectTools(toolCategory != null ? toolCategory.getId() : null);
    }

}
