package com.newland.uutools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.entity.Discuss;
import com.newland.uutools.entity.dto.SearchDTO;
import com.newland.uutools.entity.vo.BlogArticleVO;
import com.newland.uutools.entity.vo.ToolLVO;

public interface ISearchService {
    IPage<ToolLVO> searchTools(SearchDTO searchDTO);

    IPage<BlogArticleVO> searchArticles(SearchDTO searchDTO);

    IPage<Discuss> searchDiscuss(SearchDTO searchDTO);
}
