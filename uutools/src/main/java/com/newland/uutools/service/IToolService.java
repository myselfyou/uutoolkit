package com.newland.uutools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.Tool;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.ToolLVO;

import java.util.List;

/**
 * 工具表 服务类
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IToolService extends IService<Tool> {

    IPage<ToolLVO> getList(PageDTO pageDTO);

    void add(Tool category);

    void delete(List<Long> ids);

    void updateItem(Tool category);

    List<ToolLVO> getTools(Long userId, String category);
}
