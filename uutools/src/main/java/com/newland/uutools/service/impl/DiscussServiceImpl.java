package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.util.BeanUtil;
import com.newland.uutools.constant.ImageConstant;
import com.newland.uutools.controller.PictureController;
import com.newland.uutools.entity.*;
import com.newland.uutools.entity.dto.DiscussDTO;
import com.newland.uutools.entity.dto.DiscussPageDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.DiscussDetailVO;
import com.newland.uutools.entity.vo.DiscussVO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.DiscussCategoryMapper;
import com.newland.uutools.mapper.DiscussMapper;
import com.newland.uutools.mapper.PictureMapper;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IDiscussService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 话题 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class DiscussServiceImpl extends ServiceImpl<DiscussMapper, Discuss> implements IDiscussService {
    @Autowired
    private DiscussCategoryMapper discussCategoryMapper;
    @Autowired
    private PictureMapper pictureMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public IPage<Discuss> getList(DiscussPageDTO pageDTO) {
        Integer pageNumber = pageDTO.getPageNumber();
        Integer pageSize = pageDTO.getPageSize();
        Page<Discuss> page = new Page<>(pageNumber, pageSize);
        page.addOrder(OrderItem.desc("id"));
        if (!StringUtils.isEmpty(pageDTO.getCategory())) {
            DiscussCategory category = discussCategoryMapper.selectOne(Wrappers.<DiscussCategory>lambdaQuery().eq(DiscussCategory::getCode, pageDTO.getCategory()));
            if (category != null) {
                pageDTO.setCategoryId(category.getId());
            }
        }
        IPage result = baseMapper.selectDiscussList(page, pageDTO);
        return result;
    }

    @Override
    public DiscussVO getDiscuss(Long id) {
        DiscussVO discuss = baseMapper.selectDiscussVO(id);
        if (discuss == null) {
            throw new BusinessException("话题不存在");
        }
        return discuss;
    }

    @Override
    public DiscussDetailVO getDetailDiscuss(Long id) {
        DiscussVO discuss = baseMapper.selectDiscussVO(id);
        if (discuss == null) {
            throw new BusinessException("话题不存在");
        }
        User user = userMapper.selectById(discuss.getUserId());
        DiscussDetailVO detailVO = new DiscussDetailVO();
        detailVO.setDiscussVO(discuss);
        detailVO.setUser(user);
        return detailVO;
    }

    @Transactional
    @Override
    public void add(DiscussDTO discussDTO) {
        Discuss discuss = new Discuss();
        BeanUtils.copyProperties(discussDTO, discuss);
        List<Picture> pictureList = new ArrayList<>();
        baseMapper.insert(discuss);
        for (int i = 0; i < discussDTO.getPictures().size(); i++) {
            pictureList.add(new Picture(discuss.getId(), discussDTO.getPictures().get(0), ImageConstant.FLAG_DISCUSS, i));
        }
        if (pictureList.size() > 0) {
            pictureMapper.insertBatch(pictureList);
        }
    }

    @Transactional
    @Override
    public void updateItem(Long userId, DiscussDTO discussDTO) {
        Discuss dbDiscuss = baseMapper.selectById(discussDTO.getId());
        if (dbDiscuss == null) {
            throw new BusinessException("话题不存在");
        }
        if (dbDiscuss.getUserId() != userId) {
            throw new BusinessException("操作异常");
        }
        Discuss discuss = new Discuss();
        BeanUtils.copyProperties(discussDTO, discuss);
        List<Picture> pictureList = new ArrayList<>();
        pictureMapper.delete(Wrappers.<Picture>lambdaQuery().eq(Picture::getTid, discuss.getId()).eq(Picture::getFlag, ImageConstant.FLAG_DISCUSS));
        for (int i = 0; i < discussDTO.getPictures().size(); i++) {
            pictureList.add(new Picture(discuss.getId(), discussDTO.getPictures().get(0), ImageConstant.FLAG_DISCUSS, i));
        }
        if (pictureList.size() > 0) {
            pictureMapper.insertBatch(pictureList);
        }
        baseMapper.updateById(discuss);
    }

    @Override
    public void delete(Long userId, Long id) {
        Discuss discuss = baseMapper.selectById(id);
        if (discuss.getUserId() != userId) {
            throw new BusinessException("您没有删除该话题的权限");
        }
        baseMapper.deleteById(id);
    }
}
