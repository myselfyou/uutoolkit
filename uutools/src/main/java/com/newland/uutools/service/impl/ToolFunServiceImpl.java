package com.newland.uutools.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.service.IToolFunService;
import com.newland.uutools.utils.TransformUtils;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.io.StringReader;
import java.util.Properties;

@Service
public class ToolFunServiceImpl implements IToolFunService {
    @SneakyThrows
    @Override
    public String prop2yaml(Integer from, Integer to, String content) {
        Properties properties = new Properties();
        properties.load(new StringReader(content));
        try {
            //读取 JSON 字符串
            JsonNode jsonNodeTree = new ObjectMapper().readTree(TransformUtils.propToJson(properties).toJSONString());
            //转换成 YAML 字符串
            String yamlStr = new YAMLMapper().writeValueAsString(jsonNodeTree);
            return yamlStr;
        } catch (Exception e) {
            throw new BusinessException("转换错误");
        }
    }

    public static void main(String[] args) {
        ToolFunServiceImpl service = new ToolFunServiceImpl();
        System.out.println(service.prop2yaml(1, 1, "a.b.c=1"));
    }
}
