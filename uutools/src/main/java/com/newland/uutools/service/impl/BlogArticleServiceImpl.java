package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.newland.uutools.entity.*;
import com.newland.uutools.entity.dto.ArticleDTO;
import com.newland.uutools.entity.dto.ArticleListDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.BlogArticleVO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.manager.FileManager;
import com.newland.uutools.mapper.ArticleTagMapper;
import com.newland.uutools.mapper.BlogArticleMapper;
import com.newland.uutools.mapper.BlogCategoryMapper;
import com.newland.uutools.mapper.TagMapper;
import com.newland.uutools.service.IBlogArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 博客文章 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class BlogArticleServiceImpl extends ServiceImpl<BlogArticleMapper, BlogArticle> implements IBlogArticleService {
//    @Autowired
//    private FileManager fileManager;
    @Autowired
    private TagMapper tagMapper;
    @Autowired
    private ArticleTagMapper articleTagMapper;
    @Autowired
    private BlogCategoryMapper blogCategoryMapper;

    @Override
    public IPage<BlogArticleVO> getArticleList(ArticleListDTO pageDTO) {
        Integer pageNumber = pageDTO.getPageNumber();
        Integer pageSize = pageDTO.getPageSize();
        Page<BlogArticleVO> page = new Page<>(pageNumber, pageSize);
        page.addOrder(OrderItem.desc("id"));
        if (!StringUtils.isEmpty(pageDTO.getCategory())) {
            BlogCategory blogCategory = blogCategoryMapper.selectOne(Wrappers.<BlogCategory>lambdaQuery().eq(BlogCategory::getCode, pageDTO.getCategory()));
            if (blogCategory != null) {
                pageDTO.setCategoryId(blogCategory.getId());
            }
        }
        IPage result = baseMapper.getArticles(page, pageDTO);
        return result;
    }

    @Override
    public IPage<BlogArticleVO> getUserArticleList(ArticleListDTO pageDTO) {
        Integer pageNumber = pageDTO.getPageNumber();
        Integer pageSize = pageDTO.getPageSize();
        Page<BlogArticleVO> page = new Page<>(pageNumber, pageSize);
        page.addOrder(OrderItem.desc("id"));
        if (!StringUtils.isEmpty(pageDTO.getCategory())) {
            BlogCategory blogCategory = blogCategoryMapper.selectOne(Wrappers.<BlogCategory>lambdaQuery().eq(BlogCategory::getCode, pageDTO.getCategory()));
            if (blogCategory != null) {
                pageDTO.setCategoryId(blogCategory.getId());
            }
        }
        IPage result = baseMapper.getUserArticles(page, pageDTO);
        return result;
    }

    @Override
    public BlogArticleVO getArticle(Long id) {
        BlogArticleVO article = baseMapper.getArticle(id);
        if (article == null) {
            throw new BusinessException("文章不存在");
        }
//        article.setContent(fileManager.getContent(article.getId(), article.getCreateTime()));
        return article;
    }

    @Transactional
    @Override
    public void addArticle(ArticleDTO article) {
        List<String> tagStrs = article.getTagList();
        BlogArticle blogArticle = new BlogArticle();
        BeanUtils.copyProperties(article, blogArticle);
        article.setCreateTime(LocalDateTime.now());
        baseMapper.insert(blogArticle);
        if (tagStrs != null && tagStrs.size() > 0) {
            List<Tag> tags = new ArrayList<>();
            for (String tagStr : tagStrs) {
                Tag dbTag = tagMapper.selectOne(Wrappers.<Tag>lambdaQuery().eq(Tag::getName, tagStr));
                if (dbTag == null) {
                    Tag tag = new Tag();
                    tag.setName(tagStr);
                    try {
                        tagMapper.insert(tag);
                        tags.add(tag);
                    } catch (Exception e) {
                        dbTag = tagMapper.selectOne(Wrappers.<Tag>lambdaQuery().eq(Tag::getName, tag.getName()));
                        tag.setId(dbTag.getId());
                    }
                } else {
                    tags.add(dbTag);
                }
            }
            List<ArticleTag> articleTags = new ArrayList<>();
            for (int i = 0; i < tags.size(); i++) {
                articleTags.add(new ArticleTag(blogArticle.getId(), tags.get(i).getId(), i, LocalDateTime.now()));
            }
            articleTagMapper.insertBatch(articleTags);
        }
//        String path = fileManager.getContentPath(article.getId(), article.getCreateTime());
//        fileManager.setContent(path, article.getContent());
    }

    @Transactional
    @Override
    public void updateArticle(Long userId, ArticleDTO article) {
        BlogArticle dbArticle = baseMapper.selectById(article.getId());
        if (dbArticle == null) {
            throw new BusinessException("文章不存在");
        }
        if (dbArticle.getUserId() != userId) {
            throw new BusinessException("操作异常");
        }
        List<String> tagStrs = article.getTagList();
        BlogArticle blogArticle = new BlogArticle();
        BeanUtils.copyProperties(article, blogArticle);
        baseMapper.updateById(blogArticle);
        if (tagStrs != null && tagStrs.size() > 0) {
            List<Tag> tags = new ArrayList<>();
            List<ArticleTag> dbArticleTags = articleTagMapper.selectList(Wrappers.<ArticleTag>lambdaQuery().eq(ArticleTag::getArticleId, article.getId()));
            Map<Long, ArticleTag> articleTagMap = new HashMap<>();
            dbArticleTags.forEach(item -> articleTagMap.put(item.getTagId(), item));
            for (String tagStr : tagStrs) {
                Tag dbTag = tagMapper.selectOne(Wrappers.<Tag>lambdaQuery().eq(Tag::getName, tagStr));
                if (dbTag == null) {
                    Tag tag = new Tag();
                    tag.setName(tagStr);
                    try {
                        tagMapper.insert(tag);
                        tags.add(tag);
                    } catch (Exception e) {
                        dbTag = tagMapper.selectOne(Wrappers.<Tag>lambdaQuery().eq(Tag::getName, tag.getName()));
                        tag.setId(dbTag.getId());
                    }
                } else {
                    tags.add(dbTag);
                }
            }
            List<ArticleTag> articleTags = new ArrayList<>();
            for (int i = 0; i < tags.size(); i++) {
                Tag tag = tags.get(i);
                if (articleTagMap.containsKey(tag.getId())) {
                    ArticleTag articleTag = articleTagMap.get(tag.getId());
                    if (articleTag.getPosition() != i) {
                        articleTag.setPosition(i);
                        articleTagMapper.updateById(articleTag);
                    }
                    articleTagMap.remove(tag.getId());
                } else {
                    articleTags.add(new ArticleTag(blogArticle.getId(), tags.get(i).getId(), i, LocalDateTime.now()));
                }
            }
            if (articleTagMap.size() > 0) {
                articleTagMapper.deleteBatchIds(articleTagMap.keySet());
            }
            if (articleTags.size() > 0) {
                articleTagMapper.insertBatch(articleTags);
            }
        }

//        String path = fileManager.getContentPath(article.getId(), dbArticle.getCreateTime());
//        fileManager.setContent(path, article.getContent());
    }

    @Transactional
    @Override
    public void deleteArticle(Long userId, Long id) {
        BlogArticle blogArticle = baseMapper.selectOne(Wrappers.<BlogArticle>lambdaQuery().select(BlogArticle::getUserId).eq(BlogArticle::getId, id));
        if (blogArticle == null) {
            throw new BusinessException("文章不存在");
        }
        if (blogArticle.getUserId() != userId) {
            throw new BusinessException("操作异常");
        }
        baseMapper.deleteById(id);
    }
    @Transactional
    @Override
    public void backDeleteArticle(Long id) {
        BlogArticle blogArticle = baseMapper.selectOne(Wrappers.<BlogArticle>lambdaQuery().select(BlogArticle::getUserId).eq(BlogArticle::getId, id));
        if (blogArticle == null) {
            throw new BusinessException("文章不存在");
        }
        baseMapper.deleteById(id);
    }
}
