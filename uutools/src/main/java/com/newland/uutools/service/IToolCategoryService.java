package com.newland.uutools.service;

import com.newland.uutools.entity.ToolCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 文章类别 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-25 21:52:50
 */
public interface IToolCategoryService extends IService<ToolCategory> {

    List<ToolCategory> getCategoryList();

    void add(ToolCategory category);

    void delete(List<Long> ids);

    void updateItem(ToolCategory category);
}
