package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.newland.uutools.entity.Admin;
import com.newland.uutools.entity.dto.AdminPageDTO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.AdminMapper;
import com.newland.uutools.service.IAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 后台管理用户表 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:14
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Admin getInfo(String username) {
        Admin user = adminMapper.getAdmin(username);
        return user;
    }

    @Override
    public IPage<Admin> getAdmins(AdminPageDTO adminPageDTO) {
        Integer pageNumber = adminPageDTO.getPageNumber();
        Integer pageSize = adminPageDTO.getPageSize();
        Page<Admin> page = new Page<>(pageNumber, pageSize);
        IPage result = baseMapper.selectPage(page, Wrappers.emptyWrapper());
        return result;
    }

    @Override
    @Transactional(noRollbackFor = {BusinessException.class})
    public void addAdmin(Admin admin) {
        Admin dbAdmin = baseMapper.selectOne(Wrappers.<Admin>lambdaQuery().eq(Admin::getUsername, admin.getUsername()));
        if (dbAdmin != null) {
            throw new BusinessException("该用户名已经存在");
        }
        if (!org.springframework.util.StringUtils.isEmpty(admin.getPassword())) {
            admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        }
        baseMapper.insert(admin);
    }

    @Override
    @Transactional(noRollbackFor = {BusinessException.class})
    public void updateAdmin(Admin admin) {
        Admin dbAdmin = baseMapper.selectById(admin.getId());
        if (dbAdmin == null) {
            throw new BusinessException("该用户不存在");
        }
        if (!org.springframework.util.StringUtils.isEmpty(admin.getPassword())) {
            admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        }
        baseMapper.updateById(admin);
    }

    @Override
    @Transactional(noRollbackFor = {BusinessException.class})
    public void deleteAdmin(List<Long> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    @Transactional(noRollbackFor = {BusinessException.class})
    public void changeStatus(Long id, int status) {
        if (status != 0 && status != 1) {
            throw new BusinessException("狀態異常");
        }
        Admin dbAdmin = baseMapper.selectById(id);
        if (dbAdmin == null) {
            throw new BusinessException("该用户不存在");
        }
        baseMapper.update(null, Wrappers.<Admin>lambdaUpdate().set(Admin::getStatus, status).eq(Admin::getId, id));
    }
}
