package com.newland.uutools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.entity.DiscussComment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.DiscussReply;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.DiscussCommentDataVO;

import java.util.List;

/**
 * <p>
 * 话题评论 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IDiscussCommentService extends IService<DiscussComment> {

    DiscussCommentDataVO list(Long discussId, Long commentId);

    List<DiscussReply> replyList(Long commentId, Long replyId);

    void deleteReply(Long replyId);

    void deleteComment(Long commentId);

    void reply(DiscussReply discussReply);

    void comment(DiscussComment discussComment);
}
