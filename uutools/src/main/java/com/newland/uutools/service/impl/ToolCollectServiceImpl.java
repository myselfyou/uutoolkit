package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.newland.uutools.entity.Tool;
import com.newland.uutools.entity.ToolCategory;
import com.newland.uutools.entity.ToolCollect;
import com.newland.uutools.entity.User;
import com.newland.uutools.entity.vo.ToolLVO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.ToolCategoryMapper;
import com.newland.uutools.mapper.ToolCollectMapper;
import com.newland.uutools.mapper.ToolMapper;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IToolCollectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 我的工具 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-30 15:21:28
 */
@Service
public class ToolCollectServiceImpl extends ServiceImpl<ToolCollectMapper, ToolCollect> implements IToolCollectService {

    @Autowired
    private ToolMapper toolMapper;
    @Autowired
    private ToolCategoryMapper toolCategoryMapper;

    @Override
    public List<ToolLVO> getCollectTools(Long userId, String categoryCode) {
        ToolCategory toolCategory = toolCategoryMapper.selectOne(Wrappers.<ToolCategory>lambdaQuery().select(ToolCategory::getId).eq(ToolCategory::getCode, categoryCode));
        return baseMapper.selectTools(userId, toolCategory != null ? toolCategory.getId() : null);
    }

    @Override
    public Long collect(Long userId, Long toolId) {
        Tool tool = toolMapper.selectOne(Wrappers.<Tool>lambdaQuery().select(Tool::getId, Tool::getStatus).eq(Tool::getId, toolId));
        if (tool == null || tool.getStatus() != 1) {
            throw new BusinessException("收藏工具不存在");
        }
        ToolCollect toolCollect = baseMapper.selectOne(Wrappers.<ToolCollect>lambdaQuery().eq(ToolCollect::getUserId, userId).eq(ToolCollect::getToolId, toolId));
        if (toolCollect != null) {
            baseMapper.deleteById(toolCollect);
            return null;
        } else {
            ToolCollect collect = new ToolCollect(userId, toolId);
            baseMapper.insert(collect);
            return collect.getId();
        }
    }
}
