package com.newland.uutools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.entity.BlogArticle;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.dto.ArticleDTO;
import com.newland.uutools.entity.dto.ArticleListDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.BlogArticleVO;

import java.util.List;

/**
 * <p>
 * 博客文章 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IBlogArticleService extends IService<BlogArticle> {

    IPage<BlogArticleVO> getArticleList(ArticleListDTO page);

    BlogArticleVO getArticle(Long id);

    void addArticle(ArticleDTO article);

    void updateArticle(Long userId,ArticleDTO article);

    void deleteArticle(Long userId,Long id);

    IPage<BlogArticleVO> getUserArticleList(ArticleListDTO page);

    void backDeleteArticle(Long id);
}
