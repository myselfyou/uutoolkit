package com.newland.uutools.service;

import com.newland.uutools.entity.BlogCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 文章类别 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IBlogCategoryService extends IService<BlogCategory> {
    List<BlogCategory> getCategoryList();

    void add(BlogCategory category);

    void delete(List<Long> ids);

    void updateItem(BlogCategory category);
}
