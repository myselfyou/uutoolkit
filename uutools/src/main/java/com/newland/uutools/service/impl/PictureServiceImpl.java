package com.newland.uutools.service.impl;

import com.newland.uutools.entity.Picture;
import com.newland.uutools.manager.FileManager;
import com.newland.uutools.mapper.PictureMapper;
import com.newland.uutools.service.IPictureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newland.uutools.utils.DateUtils;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * <p>
 * 图片表 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class PictureServiceImpl extends ServiceImpl<PictureMapper, Picture> implements IPictureService {
    @Autowired
    private FileManager fileManager;

    @Override
    public String uploadPicture(MultipartFile file) {
        String ymd = DateUtils.formatYMD(new Date());

        File folder = new File(fileManager.getImgPath() + ymd);
        String imgpath = fileManager.getPrefix() + ymd;
        if (!folder.exists()) {
            folder.mkdirs();
        }
        String originalFilename = file.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String img = String.format("%d%s", System.currentTimeMillis(), suffix);
        try {
            File picFile = new File(folder.getCanonicalFile(), img);
            Thumbnails.of(file.getInputStream()).size(800,800).outputQuality(0.5).toFile(picFile);
            String path = imgpath + "/" + img;
            baseMapper.insert(new Picture(path));
            return path;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
