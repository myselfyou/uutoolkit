package com.newland.uutools.service;

import com.newland.uutools.entity.Tag;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.dto.PageDTO;

import java.util.List;

/**
 * <p>
 * 标签 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface ITagService extends IService<Tag> {

    List<Tag> list(PageDTO pageDTO);
}
