package com.newland.uutools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.dto.ForgetDTO;
import com.newland.uutools.entity.dto.ForgetSetDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.dto.UserDTO;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IUserService extends IService<User> {

    IPage<User> getUsers(PageDTO pageDTO);

    void addUser(User user);

    void deleteUser(List<Long> ids);

    void updateUser(User user);

    void changeStatus(Long id, Integer status);

    User getUser(String username);

    void forget(ForgetDTO forgetDTO);

    void forgetSet(ForgetSetDTO forgetDTO);

    void updateUserInfo(UserDTO user);

    void updatePassword(String username, String password);
}
