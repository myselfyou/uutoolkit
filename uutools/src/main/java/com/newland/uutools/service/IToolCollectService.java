package com.newland.uutools.service;

import com.newland.uutools.entity.ToolCollect;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.vo.ToolLVO;

import java.util.List;

/**
 * <p>
 * 我的工具 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-30 15:21:28
 */
public interface IToolCollectService extends IService<ToolCollect> {

    List<ToolLVO> getCollectTools(Long userId, String categoryCode);

    Long collect(Long userId,Long toolId);
}
