package com.newland.uutools.service.impl;

import com.newland.uutools.entity.ToolComment;
import com.newland.uutools.mapper.ToolCommentMapper;
import com.newland.uutools.service.IToolCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class ToolCommentServiceImpl extends ServiceImpl<ToolCommentMapper, ToolComment> implements IToolCommentService {

}
