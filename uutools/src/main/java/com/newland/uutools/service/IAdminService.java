package com.newland.uutools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.dto.AdminPageDTO;

import java.util.List;

/**
 * <p>
 * 后台管理用户表 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:14
 */
public interface IAdminService extends IService<Admin> {
    Admin getInfo(String username);

    IPage<Admin> getAdmins(AdminPageDTO adminPageDTO);

    void addAdmin(Admin admin);

    void updateAdmin(Admin admin);

    void deleteAdmin(List<Long> ids);

    void changeStatus(Long id, int status);
}
