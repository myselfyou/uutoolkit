package com.newland.uutools.service;

public interface IAdminLoginService {
    String login(String username, String password);
}
