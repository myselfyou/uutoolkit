package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.newland.uutools.entity.Admin;
import com.newland.uutools.entity.User;
import com.newland.uutools.entity.dto.ForgetDTO;
import com.newland.uutools.entity.dto.ForgetSetDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.dto.UserDTO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newland.uutools.utils.AesUtils;
import com.newland.uutools.utils.FileUtils;
import com.newland.uutools.utils.SendMailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public IPage<User> getUsers(PageDTO pageDTO) {
        Integer pageNumber = pageDTO.getPageNumber();
        Integer pageSize = pageDTO.getPageSize();
        Page<User> page = new Page<>(pageNumber, pageSize);
        page.addOrder(OrderItem.desc("id"));
        IPage result = baseMapper.selectPage(page, Wrappers.emptyWrapper());
        return result;
    }

    @Override
    public void addUser(User user) {
        User dbUser = baseMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, user.getUsername()));
        if (dbUser != null) {
            throw new BusinessException("该用户名已经存在");
        }
        if (!org.springframework.util.StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        if (StringUtils.isEmpty(user.getNickname())) {
            user.setNickname(user.getUsername().substring(0, user.getUsername().indexOf("@")));
        }
        user.setFlag(1);
        baseMapper.insert(user);
    }

    @Override
    public void deleteUser(List<Long> ids) {
        List<User> dbUsers = baseMapper.selectBatchIds(ids);
        List<Long> backUserIds = dbUsers.stream().filter(user -> user.getFlag() == 1).map(item -> item.getId()).collect(Collectors.toList());
        baseMapper.deleteBatchIds(backUserIds);
    }

    @Override
    public void updateUser(User user) {
        User dbUser = baseMapper.selectById(user.getId());
        if (dbUser == null) {
            throw new BusinessException("该用户不存在");
        }
        LambdaUpdateWrapper<User> wrapper = Wrappers.<User>lambdaUpdate()
                .set(User::getUsername, user.getUsername())
                .set(User::getNickname, user.getNickname())
                .set(User::getAvatar, user.getAvatar())
                .set(User::getIntro, user.getIntro())
                .set(User::getStatus, user.getStatus());
        if (user.getStatus() == 1) {
            wrapper.set(User::getLockTime, null);
        }
        if (!org.springframework.util.StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            wrapper.set(User::getPassword, user.getPassword());
        }
        wrapper.eq(User::getId, user.getId());
        baseMapper.update(null, wrapper);
    }

    @Override
    public void changeStatus(Long id, Integer status) {
        if (status != 0 && status != 1 && status != 2) {
            throw new BusinessException("狀態異常");
        }
        User dbUser = baseMapper.selectById(id);
        if (dbUser == null) {
            throw new BusinessException("该用户不存在");
        }
        baseMapper.update(null, Wrappers.<User>lambdaUpdate().set(User::getStatus, status).eq(User::getId, id));
    }

    @Override
    public User getUser(String username) {
        return baseMapper.selectUserByUsername(username);
    }

    @Override
    public void forget(ForgetDTO forgetDTO) {
        String verify = AesUtils.decrypt(forgetDTO.getVerify());
        if (!verify.toUpperCase().equals(forgetDTO.getVerifyCode().toUpperCase())) {
            throw new BusinessException("验证码错误");
        }
        User user = baseMapper.selectUserByUsername(forgetDTO.getEmail());
        if (user == null) {
            throw new BusinessException("该用户不存在");
        }
        String verifyCode = UUID.randomUUID().toString();
        user.setVerifyCode(verifyCode);
        String verifyUrl = "http://localhost:8081/setpassword?token=" + verifyCode + "&email=" + forgetDTO.getEmail();
        String content = FileUtils.getClassFileContent("forget.html");
        content = content.replace("${url}", verifyUrl);
        SendMailUtil.sendSyncMail(user.getUsername(), content);
        baseMapper.updateById(user);
    }

    @Override
    public void forgetSet(ForgetSetDTO forgetDTO) {
        User user = baseMapper.selectUserByUsername(forgetDTO.getEmail());
        if (user == null) {
            throw new BusinessException("该用户不存在");
        }
        String verify = forgetDTO.getVerify();
        if (!verify.equals(user.getVerifyCode())) {
            throw new BusinessException("操作异常");
        }
        user.setPassword(passwordEncoder.encode(AesUtils.decrypt(forgetDTO.getPassword())));
        baseMapper.updateById(user);
    }

    @Override
    public void updateUserInfo(UserDTO user) {
        User dbUser = baseMapper.selectUserByUsername(user.getUsername());
        if (dbUser == null) {
            throw new BusinessException("用户不存在");
        }
        LambdaUpdateWrapper<User> wrapper = Wrappers.<User>lambdaUpdate()
                .set(User::getNickname, user.getNickname())
                .set(User::getAvatar, user.getAvatar())
                .set(User::getIntro, user.getIntro());
        wrapper.eq(User::getId, dbUser.getId());
        baseMapper.update(null, wrapper);
    }

    @Override
    public void updatePassword(String username, String password) {
        User dbUser = baseMapper.selectUserByUsername(username);
        if (dbUser == null) {
            throw new BusinessException("用户不存在");
        }
        dbUser.setPassword(passwordEncoder.encode(AesUtils.decrypt(password)));
        baseMapper.updateById(dbUser);
    }
}
