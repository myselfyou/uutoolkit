package com.newland.uutools.service.impl;

import com.newland.uutools.entity.Tag;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.mapper.TagMapper;
import com.newland.uutools.service.ITagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 标签 服务实现类
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements ITagService {

    @Override
    public List<Tag> list(PageDTO pageDTO) {
        return null;
    }
}
