package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.newland.uutools.entity.Discuss;
import com.newland.uutools.entity.DiscussCategory;
import com.newland.uutools.entity.Feedback;
import com.newland.uutools.entity.User;
import com.newland.uutools.entity.dto.FeedbackPageDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.mapper.FeedbackMapper;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IFeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 反馈表 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements IFeedbackService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public IPage<Feedback> getList(FeedbackPageDTO pageDTO) {
        Integer pageNumber = pageDTO.getPageNumber();
        Integer pageSize = pageDTO.getPageSize();
        Page<Feedback> page = new Page<>(pageNumber, pageSize);

        LambdaQueryWrapper<Feedback> wrapper = Wrappers.lambdaQuery();
        if (pageDTO.getStatus() != null && pageDTO.getStatus() == 1) {
            wrapper.ne(Feedback::getAdminId, null);
        }
        IPage<Feedback> result = baseMapper.selectPage(page, wrapper);
        return result;
    }

    @Override
    public void add(String username, Feedback feedback) {
        if (username != null) {
            User user = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, username));
            if (user != null) {
                feedback.setUserId(user.getId());
            }
        }
        baseMapper.insert(feedback);
    }

    @Override
    public void updateItem(Feedback feedback) {
        baseMapper.updateById(feedback);
    }
}
