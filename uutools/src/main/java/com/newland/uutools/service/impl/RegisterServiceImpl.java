package com.newland.uutools.service.impl;

import com.newland.uutools.entity.User;
import com.newland.uutools.entity.dto.RegisterDTO;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IRegisterService;
import com.newland.uutools.utils.AesUtils;
import com.newland.uutools.utils.FileUtils;
import com.newland.uutools.utils.SendMailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;

@Service
public class RegisterServiceImpl implements IRegisterService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void register(RegisterDTO registerDTO) {
        User dbUser = userMapper.selectUserByUsername(registerDTO.getUsername());
        if (dbUser != null && dbUser.getStatus() != 0) {
            throw new BusinessException("注册账户已存在");
        }
        String password = AesUtils.decrypt(registerDTO.getPassword());
        if (password.length() < 6 || password.length() > 15) {
            throw new BusinessException("密码长度在6-15之间");
        }
        User user = new User();
        user.setUsername(registerDTO.getUsername());
        user.setPassword(passwordEncoder.encode(password));
        String verifyCode = UUID.randomUUID().toString();
        user.setVerifyCode(verifyCode);
        String verifyUrl = "https://uutoolkit.com/verify?token=" + verifyCode;
        String content = FileUtils.getClassFileContent("register.html");
        content = content.replace("${url}", verifyUrl);
        SendMailUtil.sendSyncMail(user.getUsername(), content);
        if (dbUser != null) {
            user.setId(dbUser.getId());
            userMapper.updateById(user);
        } else {
            userMapper.insert(user);
        }
    }

}
