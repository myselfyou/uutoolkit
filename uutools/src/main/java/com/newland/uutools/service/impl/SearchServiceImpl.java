package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.newland.uutools.entity.Discuss;
import com.newland.uutools.entity.dto.SearchDTO;
import com.newland.uutools.entity.vo.BlogArticleVO;
import com.newland.uutools.entity.vo.ToolLVO;
import com.newland.uutools.mapper.BlogArticleMapper;
import com.newland.uutools.mapper.DiscussMapper;
import com.newland.uutools.mapper.ToolMapper;
import com.newland.uutools.service.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl implements ISearchService {
    @Autowired
    private ToolMapper toolMapper;
    @Autowired
    private BlogArticleMapper blogArticleMapper;
    @Autowired
    private DiscussMapper discussMapper;

    @Override
    public IPage<ToolLVO> searchTools(SearchDTO pageDTO) {
        Page<ToolLVO> page = new Page<>(pageDTO.getPageNumber(), pageDTO.getPageSize());
        return toolMapper.selectToolPageByText(page, pageDTO.getText());
    }

    @Override
    public IPage<BlogArticleVO> searchArticles(SearchDTO pageDTO) {
        Integer pageNumber = pageDTO.getPageNumber();
        Integer pageSize = pageDTO.getPageSize();
        Page<BlogArticleVO> page = new Page<>(pageNumber, pageSize);
        IPage result = blogArticleMapper.getArticlesByText(page, pageDTO.getText());
        return result;
    }

    @Override
    public IPage<Discuss> searchDiscuss(SearchDTO pageDTO) {
        Integer pageNumber = pageDTO.getPageNumber();
        Integer pageSize = pageDTO.getPageSize();
        Page<Discuss> page = new Page<>(pageNumber, pageSize);
        IPage result = discussMapper.selectDiscussListByText(page, pageDTO.getText());
        return result;
    }
}
