package com.newland.uutools.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.entity.Feedback;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.dto.FeedbackPageDTO;
import com.newland.uutools.entity.dto.PageDTO;

/**
 * <p>
 * 反馈表 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IFeedbackService extends IService<Feedback> {

    IPage<Feedback> getList(FeedbackPageDTO pageDTO);

    void add(String username,Feedback feedback);

    void updateItem(Feedback feedback);
}
