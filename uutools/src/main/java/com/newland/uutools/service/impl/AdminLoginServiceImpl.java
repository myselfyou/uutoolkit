package com.newland.uutools.service.impl;

import com.newland.uutools.entity.Admin;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.AdminMapper;
import com.newland.uutools.service.IAdminLoginService;
import com.newland.uutools.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AdminLoginServiceImpl implements IAdminLoginService {

    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public String login(String username, String password) {
        Admin admin = adminMapper.getAdmin(username);
        if (admin == null) {
            throw new BusinessException("账户不存在");
        }
        if (passwordEncoder.matches(password, admin.getPassword())) {
            admin.setLoginTime(LocalDateTime.now().withNano(0));
            adminMapper.updateById(admin);
            return JwtTokenUtil.createToken(username);
        }
        return null;
    }
}
