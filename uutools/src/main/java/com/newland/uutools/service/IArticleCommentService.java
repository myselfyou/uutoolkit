package com.newland.uutools.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.newland.uutools.entity.ArticleComment;
import com.newland.uutools.entity.ArticleReply;
import com.newland.uutools.entity.vo.ArticleCommentDataVO;

import java.util.List;

/**
 * <p>
 * 文章评论 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-02-08 14:14:30
 */
public interface IArticleCommentService extends IService<ArticleComment> {
    ArticleCommentDataVO list(Long articleId, Long commentId);

    List<ArticleReply> replyList(Long commentId, Long replyId);

    void deleteReply(Long replyId);

    void deleteComment(Long commentId);

    void reply(ArticleReply discussReply);

    void comment(ArticleComment discussComment);
}
