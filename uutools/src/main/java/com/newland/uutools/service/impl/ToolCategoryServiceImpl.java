package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newland.uutools.entity.ToolCategory;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.ToolCategoryMapper;
import com.newland.uutools.service.IToolCategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 文章类别 服务实现类
 *
 * @author leellun
 * @since 2022-01-25 21:52:50
 */
@Service
public class ToolCategoryServiceImpl extends ServiceImpl<ToolCategoryMapper, ToolCategory> implements IToolCategoryService {

    @Override
    public List<ToolCategory> getCategoryList() {
        return baseMapper.selectList(Wrappers.<ToolCategory>lambdaQuery().orderByAsc(ToolCategory::getPosition));
    }

    @Override
    public void add(ToolCategory category) {
        ToolCategory dbCategory = baseMapper.selectOne(Wrappers.<ToolCategory>lambdaQuery().eq(ToolCategory::getName, category.getName()).or().eq(ToolCategory::getCode, category.getCode()));
        if (dbCategory != null) {
            throw new BusinessException("已经存在指定类名或者code");
        }
        List<ToolCategory> list = baseMapper.selectList(Wrappers.<ToolCategory>lambdaQuery().orderByAsc(ToolCategory::getPosition));
        setToolCategoryList(list, category);
    }

    @Override
    public void delete(List<Long> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public void updateItem(ToolCategory category) {
        ToolCategory dbItem = baseMapper.selectById(category.getId());
        if (dbItem == null) {
            throw new BusinessException("指定类名不存在");
        }
        ToolCategory dbCategory = baseMapper.selectOne(Wrappers.<ToolCategory>lambdaQuery().eq(ToolCategory::getName, category.getName()).or().eq(ToolCategory::getCode, category.getCode()));
        if (dbCategory != null && dbCategory.getId() != dbItem.getId()) {
            throw new BusinessException("已经存在指定类名或者code");
        }
        List<ToolCategory> list = baseMapper.selectList(Wrappers.<ToolCategory>lambdaQuery().ne(ToolCategory::getId, category.getId()).orderByAsc(ToolCategory::getPosition));
        setToolCategoryList(list, category);
    }

    private void setToolCategoryList(List<ToolCategory> list, ToolCategory category) {
        List<ToolCategory> newList = new ArrayList<>(list);
        List<ToolCategory> saveOrUpdateList = new ArrayList<>();
        newList.add(category);
        Collections.sort(newList, Comparator.comparing(ToolCategory::getPosition));
        for (int i = 0; i < newList.size(); i++) {
            ToolCategory item = newList.get(i);
            if (item.getPosition() != i) {
                item.setPosition(i);
                saveOrUpdateList.add(item);
            } else if (item.getId() == null || item.getId() == category.getId()) {
                saveOrUpdateList.add(item);
            }
        }
        saveOrUpdateBatch(saveOrUpdateList);
    }
}
