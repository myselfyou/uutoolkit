package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.newland.uutools.entity.DiscussCategory;
import com.newland.uutools.entity.DiscussCategory;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.DiscussCategoryMapper;
import com.newland.uutools.service.IDiscussCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 话题分类 服务实现类
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class DiscussCategoryServiceImpl extends ServiceImpl<DiscussCategoryMapper, DiscussCategory> implements IDiscussCategoryService {
    @Override
    public List<DiscussCategory> getCategoryList() {
        return baseMapper.selectList(Wrappers.<DiscussCategory>lambdaQuery().orderByAsc(DiscussCategory::getPosition));
    }

    @Override
    public void add(DiscussCategory category) {
        DiscussCategory dbCategory = baseMapper.selectOne(Wrappers.<DiscussCategory>lambdaQuery().eq(DiscussCategory::getName, category.getName()).or().eq(DiscussCategory::getCode, category.getCode()));
        if (dbCategory != null) {
            throw new BusinessException("已经存在指定类名或者code");
        }
        List<DiscussCategory> list = baseMapper.selectList(Wrappers.<DiscussCategory>lambdaQuery().orderByAsc(DiscussCategory::getPosition));
        setDiscussCategoryList(list, category);
    }

    @Override
    public void delete(List<Long> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public void updateItem(DiscussCategory category) {
        DiscussCategory dbItem = baseMapper.selectById(category.getId());
        if (dbItem == null) {
            throw new BusinessException("指定类名不存在");
        }
        DiscussCategory dbCategory = baseMapper.selectOne(Wrappers.<DiscussCategory>lambdaQuery().eq(DiscussCategory::getName, category.getName()).or().eq(DiscussCategory::getCode, category.getCode()));
        if (dbCategory != null && dbCategory.getId() != dbItem.getId()) {
            throw new BusinessException("已经存在指定类名或者code");
        }
        List<DiscussCategory> list = baseMapper.selectList(Wrappers.<DiscussCategory>lambdaQuery().ne(DiscussCategory::getId, category.getId()).orderByAsc(DiscussCategory::getPosition));
        setDiscussCategoryList(list, category);
    }

    private void setDiscussCategoryList(List<DiscussCategory> list, DiscussCategory category) {
        List<DiscussCategory> newList = new ArrayList<>(list);
        List<DiscussCategory> saveOrUpdateList = new ArrayList<>();
        newList.add(category);
        Collections.sort(newList, Comparator.comparing(DiscussCategory::getPosition));
        for (int i = 0; i < newList.size(); i++) {
            DiscussCategory item = newList.get(i);
            if (item.getPosition() != i) {
                item.setPosition(i);
                saveOrUpdateList.add(item);
            } else if (item.getId() == null || item.getId() == category.getId()) {
                saveOrUpdateList.add(item);
            }
        }
        saveOrUpdateBatch(saveOrUpdateList);
    }
}
