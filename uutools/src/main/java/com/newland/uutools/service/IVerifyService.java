package com.newland.uutools.service;

public interface IVerifyService {
    void verify(String verifyCode);
    String verifyForget(String verifyCode);
}
