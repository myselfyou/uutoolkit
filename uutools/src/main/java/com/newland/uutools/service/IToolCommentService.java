package com.newland.uutools.service;

import com.newland.uutools.entity.ToolComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IToolCommentService extends IService<ToolComment> {

}
