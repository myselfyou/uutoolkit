package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.newland.uutools.entity.User;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.UserMapper;
import com.newland.uutools.service.IVerifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class VerifyServiceImpl implements IVerifyService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public void verify(String verifyCode) {
        User user = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getVerifyCode, verifyCode));
        if(user==null){
            throw new BusinessException("激活用户不存在");
        }
        if (user.getStatus() == 0) {
            if (verifyCode.equals(user.getVerifyCode())) {
                user.setVerifyTime(LocalDateTime.now());
                user.setStatus(1);
                user.setVerifyCode("");
                userMapper.updateById(user);
            } else {
                throw new BusinessException("用户激活链接已过期");
            }
        } else {
            throw new BusinessException("用户已经激活");
        }
    }

    @Override
    public String verifyForget(String verifyCode) {
        User user = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getVerifyCode, verifyCode));
        if(user==null){
            throw new BusinessException("用户不存在");
        }
        if (!verifyCode.equals(user.getVerifyCode())) {
            throw new BusinessException("验证失败");
        }else{
            return user.getUsername();
        }
    }
}
