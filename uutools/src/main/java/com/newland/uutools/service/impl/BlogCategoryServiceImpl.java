package com.newland.uutools.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newland.uutools.entity.BlogCategory;
import com.newland.uutools.exception.BusinessException;
import com.newland.uutools.mapper.BlogCategoryMapper;
import com.newland.uutools.service.IBlogCategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 * 文章类别 服务实现类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Service
public class BlogCategoryServiceImpl extends ServiceImpl<BlogCategoryMapper, BlogCategory> implements IBlogCategoryService {
    @Override
    public List<BlogCategory> getCategoryList() {
        return baseMapper.selectList(Wrappers.<BlogCategory>lambdaQuery().orderByAsc(BlogCategory::getPosition));
    }

    @Override
    public void add(BlogCategory category) {
        BlogCategory dbCategory = baseMapper.selectOne(Wrappers.<BlogCategory>lambdaQuery().eq(BlogCategory::getName, category.getName()).or().eq(BlogCategory::getCode, category.getCode()));
        if (dbCategory != null) {
            throw new BusinessException("已经存在指定类名或者code");
        }
        List<BlogCategory> list = baseMapper.selectList(Wrappers.<BlogCategory>lambdaQuery().orderByAsc(BlogCategory::getPosition));
        setBlogCategoryList(list, category);
    }

    @Override
    public void delete(List<Long> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public void updateItem(BlogCategory category) {
        BlogCategory dbItem = baseMapper.selectById(category.getId());
        if (dbItem == null) {
            throw new BusinessException("指定类名不存在");
        }
        BlogCategory dbCategory = baseMapper.selectOne(Wrappers.<BlogCategory>lambdaQuery().eq(BlogCategory::getName, category.getName()).or().eq(BlogCategory::getCode, category.getCode()));
        if (dbCategory != null && dbCategory.getId() != dbItem.getId()) {
            throw new BusinessException("已经存在指定类名或者code");
        }
        List<BlogCategory> list = baseMapper.selectList(Wrappers.<BlogCategory>lambdaQuery().ne(BlogCategory::getId, category.getId()).orderByAsc(BlogCategory::getPosition));
        setBlogCategoryList(list, category);
    }

    private void setBlogCategoryList(List<BlogCategory> list, BlogCategory category) {
        List<BlogCategory> newList = new ArrayList<>(list);
        List<BlogCategory> saveOrUpdateList = new ArrayList<>();
        newList.add(category);
        Collections.sort(newList, Comparator.comparing(BlogCategory::getPosition));
        for (int i = 0; i < newList.size(); i++) {
            BlogCategory item = newList.get(i);
            if (item.getPosition() != i) {
                item.setPosition(i);
                saveOrUpdateList.add(item);
            } else if (item.getId() == null || item.getId() == category.getId()) {
                saveOrUpdateList.add(item);
            }
        }
        saveOrUpdateBatch(saveOrUpdateList);
    }
}
