package com.newland.uutools.service;

import com.newland.uutools.entity.Picture;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 图片表 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
public interface IPictureService extends IService<Picture> {
    String uploadPicture(MultipartFile file);
}
