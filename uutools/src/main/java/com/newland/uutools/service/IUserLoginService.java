package com.newland.uutools.service;

import org.springframework.stereotype.Service;

@Service
public interface IUserLoginService {
    String login(String username, String password);
}
