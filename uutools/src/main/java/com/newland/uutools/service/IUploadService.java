package com.newland.uutools.service;

import com.newland.uutools.entity.Upload;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 图片表 服务类
 * </p>
 *
 * @author leellun
 * @since 2022-01-30 21:18:55
 */
public interface IUploadService extends IService<Upload> {
    String uploadPicture(MultipartFile file);
}
