package com.newland.uutools.service;

import com.newland.uutools.entity.dto.RegisterDTO;

public interface IRegisterService {
    void register(RegisterDTO registerDTO);
}
