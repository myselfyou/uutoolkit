package com.newland.uutools.utils;

import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat format0 = new SimpleDateFormat("yyyyMMdd");
    private static long internetTime;
    private static long uptimeMillis;

    public static String format(Date date) {
        return simpleDateFormat.format(date);
    }

    public static String formatYMD(Date date) {
        return format0.format(date);
    }

    public static String formatYMD(Calendar calendar) {
        return String.format("%04d%02d%02d", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static Date parse(String source) {
        try {
            return simpleDateFormat.parse(source);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getNetworkTime() {
        try {
            URL url = new URL("https://www.baidu.com");
            URLConnection conn = url.openConnection();
            conn.connect();
            long dateL = conn.getDate();
            internetTime = dateL;
            uptimeMillis = System.currentTimeMillis();
            Date date = new Date(dateL);
            return date;
        } catch (Exception e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public static Date getDate() {
//		if (internetTime <= 0) {
//			return getNetworkTime();
//		} else {
//			Calendar calendar = Calendar.getInstance();
//			calendar.setTimeInMillis(internetTime + (System.currentTimeMillis() - uptimeMillis));
//			return calendar.getTime();
//		}
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }
}
