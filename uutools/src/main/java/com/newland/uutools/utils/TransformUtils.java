package com.newland.uutools.utils;

import com.alibaba.fastjson.JSONObject;

import java.util.Enumeration;
import java.util.Properties;

public class TransformUtils {
    public static JSONObject propToJson(Properties properties) {
        JSONObject jsonObject = new JSONObject();
        Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String[] args = key.trim().split("\\.");
            JSONObject next = jsonObject;
            for (int i = 0; i < args.length - 1; i++) {
                String arg = args[i];
                JSONObject item = next.getJSONObject(arg);
                if (item == null) {
                    item = new JSONObject();
                    next.put(arg, item);
                }
                next = item;
            }
            next.put(args[args.length - 1], properties.getProperty(key));
        }
        return jsonObject;
    }
}
