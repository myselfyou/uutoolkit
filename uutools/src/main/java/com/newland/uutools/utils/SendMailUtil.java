package com.newland.uutools.utils;

import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class SendMailUtil {
    public static void main(String[] arg){
        sendMail("289747235@qq.com","sdfsdfsdf");
    }
    public static void sendMail(String receiveMail,String contentHtml) {
        try {
            Properties prop = new Properties();
            prop.setProperty("mail.host", "smtp.sina.cn");
            prop.setProperty("mail.transport.protocol", "smtp");
            prop.setProperty("mail.smtp.auth", "true");
            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            prop.put("mail.smtp.ssl.enable", "true");
            prop.put("mail.smtp.ssl.socketFactory", sf);

            //使用JavaMail发送邮件的5个步骤
            //1、创建session
            Session session = Session.getInstance(prop);
            //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
            session.setDebug(true);
            //2、通过session得到transport对象
            Transport ts = session.getTransport();
            //3、使用邮箱的用户名和密码连上邮件服务器，发送邮件时，发件人需要提交邮箱的用户名和密码给smtp服务器，用户名和密码都通过验证之后才能够正常发送邮件给收件人。
            ts.connect("smtp.sina.cn",465, "leellun@sina.cn", "e441bb64155ad49b");
            //4、创建邮件
            Message message = createMimeMessage(session,"leellun@sina.cn",receiveMail,contentHtml);
            //5、发送邮件
            ts.sendMessage(message, message.getAllRecipients());
            ts.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private static ExecutorService executorService=Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    public static void sendSyncMail(String receiveMail,String contentHtml) {
        executorService.submit(() -> sendMail(receiveMail,contentHtml));
    }

    public static MimeMessage createMimeMessage(Session session, String sendMail, String receiveMail,String contentHtml) throws Exception {
        // 1. 创建一封邮件
        MimeMessage message = new MimeMessage(session);
        // 2. From: 发件人
        message.setFrom(new InternetAddress(sendMail, "UUToolKit网站", "UTF-8"));
        // 3. To: 收件人（可以增加多个收件人、抄送、密送）
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, "XX用户", "UTF-8"));
        // 4. Subject: 邮件主题
        message.setSubject("UUToolKit网站账户电子邮件确认", "UTF-8");
        // 5. Content: 邮件正文（可以使用html标签）
        message.setContent(contentHtml, "text/html;charset=UTF-8"); //以本人网站为例
        // 6. 设置发件时间
        message.setSentDate(new Date());
        // 7. 保存设置
        message.saveChanges();
        return message;
    }
}
