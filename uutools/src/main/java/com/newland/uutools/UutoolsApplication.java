package com.newland.uutools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
public class UutoolsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UutoolsApplication.class, args);
    }

}
