package com.newland.uutools.controller;

import com.newland.uutools.entity.dto.LoginDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IAdminLoginService;
import com.newland.uutools.utils.AesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("/api/admin/b")
@RestController
public class AdminLoginController {
    @Autowired
    private IAdminLoginService loginService;

    @RequestMapping("/login")
    public ResponseEntity login(@Validated @RequestBody LoginDTO loginDto) {
        String token = loginService.login(loginDto.getUsername(), AesUtils.decrypt(loginDto.getPassword()));
        if (token != null) {
            Map<String, String> map = new HashMap<>();
            map.put("token", token);
            return ResponseEntity.ok("登录成功",map);
        } else {
            return ResponseEntity.error("登录失败");
        }
    }
}
