package com.newland.uutools.controller;


import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.DiscussCategory;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IDiscussCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 话题分类 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/discuss/category")
public class DiscussCategoryController {
    @Autowired
    private IDiscussCategoryService discussCategoryService;

    @RequestMapping(path = {"/f/list","/b/list"})
    public ResponseEntity list() {
        List<DiscussCategory> list = discussCategoryService.getCategoryList();
        return ResponseEntity.ok(list);
    }

    @RequestMapping("/b/add")
    public ResponseEntity add(@RequestBody @Validated(value = Insert.class) DiscussCategory category) {
        discussCategoryService.add(category);
        return ResponseEntity.success("添加成功");
    }

    @RequestMapping("/b/delete")
    public ResponseEntity delete(@RequestParam(value = "idStr") String idStr) {
        if (StringUtils.isEmpty(idStr)) {
            return ResponseEntity.error("參數傳遞錯誤");
        }
        List<Long> ids = Arrays.stream(idStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        discussCategoryService.delete(ids);
        return ResponseEntity.success("删除成功");
    }

    @RequestMapping("/b/update")
    public ResponseEntity update(@RequestBody @Validated(value = Update.class) DiscussCategory category) {
        discussCategoryService.updateItem(category);
        return ResponseEntity.success("修改成功");
    }
}
