package com.newland.uutools.controller;

import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IVerifyService;
import com.newland.uutools.utils.AesUtils;
import com.newland.uutools.utils.JwtTokenUtil;
import com.newland.uutools.utils.ValidateCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/api/verify")
public class VerifyController  {
    @Autowired
    private IVerifyService verifyService;
    @PostMapping
    @ResponseBody
    public ResponseEntity verify(@RequestParam("verifyCode") String verifyCode){
        verifyService.verify(verifyCode);
        return ResponseEntity.success("成功激活");
    }
    @PostMapping("/forget")
    @ResponseBody
    public ResponseEntity verifyForget(@RequestParam("verifyCode") String verifyCode){
        return ResponseEntity.ok(verifyService.verifyForget(verifyCode));
    }
    @GetMapping("/code")
    public void getVerifyImg(HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");//设置相应类型,告诉浏览器输出的内容为图片
        response.setHeader("Pragma", "No-cache");//设置响应头信息，告诉浏览器不要缓存此内容
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expire", 0);

        ValidateCode vCode = new ValidateCode(160,40,4,150);
        response.setHeader(JwtTokenUtil.VERIFY_IMG, AesUtils.encrypt(vCode.getCode()));
        vCode.write(response.getOutputStream());
    }
}
