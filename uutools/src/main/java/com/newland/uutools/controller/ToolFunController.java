package com.newland.uutools.controller;

import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IToolFunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/toolfun")
public class ToolFunController {
    @Autowired
    private IToolFunService toolFunService;
    @RequestMapping("/f/prop2yaml")
    public ResponseEntity prop2yaml(@RequestParam("from") Integer from,@RequestParam("to") Integer to,@RequestParam("content") String content) {
        return ResponseEntity.ok(toolFunService.prop2yaml(from,to,content));
    }
}
