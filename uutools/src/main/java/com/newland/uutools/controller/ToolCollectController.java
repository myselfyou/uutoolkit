package com.newland.uutools.controller;


import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.entity.vo.ToolLVO;
import com.newland.uutools.service.IToolCollectService;
import com.newland.uutools.utils.SecurityContextUtils;
import org.apache.catalina.security.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 我的工具 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-30 15:21:28
 */
@RestController
@RequestMapping("/api/tool/collect")
public class ToolCollectController {
    @Autowired
    private IToolCollectService toolCollectService;

    @PostMapping("/list")
    public ResponseEntity list(@RequestParam(value = "categoryCode", required = false) String categoryCode) {
        List<ToolLVO> list = toolCollectService.getCollectTools(SecurityContextUtils.getUserId(), categoryCode);
        return ResponseEntity.ok(list);
    }

    @PostMapping("/favorite")
    public ResponseEntity collect(@RequestParam(value = "toolId") Long toolId) {
        Long result = toolCollectService.collect(SecurityContextUtils.getUserId(), toolId);
        if (result != null) {
            return ResponseEntity.ok("收藏成功", result);
        } else {
            return ResponseEntity.ok("取消收藏成功", null);
        }
    }
}
