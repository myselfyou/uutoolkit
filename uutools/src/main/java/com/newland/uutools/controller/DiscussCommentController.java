package com.newland.uutools.controller;


import com.newland.uutools.entity.DiscussComment;
import com.newland.uutools.entity.DiscussReply;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IDiscussCommentService;
import com.newland.uutools.service.impl.DiscussCommentServiceImpl;
import com.newland.uutools.utils.SecurityContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;

/**
 * <p>
 * 话题评论 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/discuss")
public class DiscussCommentController {
    @Autowired
    private IDiscussCommentService discussCommentService;

    @PostMapping("/comment/f/{discussId}")
    public ResponseEntity list(@PathVariable("discussId") Long discussId, @RequestParam(value = "commentId", required = false) Long commentId) {
        return ResponseEntity.ok(discussCommentService.list(discussId, commentId));
    }

    @PostMapping("/reply/f/{commentId}")
    public ResponseEntity replyList(@PathVariable("commentId") Long commentId, @RequestParam(value = "replyId", required = false) Long replyId) {
        return ResponseEntity.ok(discussCommentService.replyList(commentId, replyId));
    }

    @PostMapping("/comment/submit")
    public ResponseEntity comment(@RequestBody @Validated DiscussComment discussComment) {
        discussComment.setFromId(SecurityContextUtils.getUserId());
        discussCommentService.comment(discussComment);
        return ResponseEntity.success("提交评论成功");
    }

    @PostMapping("/reply/submit")
    public ResponseEntity reply(@RequestBody @Validated DiscussReply discussReply) {
        discussReply.setFromId(SecurityContextUtils.getUserId());
        discussCommentService.reply(discussReply);
        return ResponseEntity.success("回复成功");
    }

    @PostMapping("/comment/delete/{commentId}")
    public ResponseEntity deleteComment(@PathVariable("commentId") Long commentId) {
        discussCommentService.deleteComment(commentId);
        return ResponseEntity.success("删除成功");
    }

    @PostMapping("/reply/delete/{replyId}")
    public ResponseEntity deleteReply(@PathVariable("replyId") Long replyId) {
        discussCommentService.deleteReply(replyId);
        return ResponseEntity.success("删除成功");
    }

}
