package com.newland.uutools.controller;


import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.BlogCategory;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IBlogCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 文章类别
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/article/category")
public class BlogCategoryController {
    @Autowired
    private IBlogCategoryService blogCategoryService;

    @RequestMapping(path = {"/b/list","/f/list"})
    public ResponseEntity list() {
        List<BlogCategory> list = blogCategoryService.getCategoryList();
        return ResponseEntity.ok(list);
    }

    @RequestMapping("/b/add")
    public ResponseEntity add(@RequestBody @Validated(value = Insert.class) BlogCategory category) {
        blogCategoryService.add(category);
        return ResponseEntity.success("添加成功");
    }

    @RequestMapping("/b/delete")
    public ResponseEntity delete(@RequestParam(value = "idStr") String idStr) {
        if (StringUtils.isEmpty(idStr)) {
            return ResponseEntity.error("參數傳遞錯誤");
        }
        List<Long> ids = Arrays.stream(idStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        blogCategoryService.delete(ids);
        return ResponseEntity.success("删除成功");
    }

    @RequestMapping("/b/update")
    public ResponseEntity update(@RequestBody @Validated(value = Update.class) BlogCategory category) {
        blogCategoryService.updateItem(category);
        return ResponseEntity.success("修改成功");
    }
}
