package com.newland.uutools.controller;


import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.ToolCategory;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IToolCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 工具分类
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/tool/category")
public class ToolCategoryController {
    @Autowired
    private IToolCategoryService toolCategoryService;

    @PostMapping(path = {"/f/list","/b/list"})
    public ResponseEntity list() {
        List<ToolCategory> list = toolCategoryService.getCategoryList();
        return ResponseEntity.ok(list);
    }

    @RequestMapping("/b/add")
    public ResponseEntity add(@RequestBody @Validated(value = Insert.class) ToolCategory category) {
        toolCategoryService.add(category);
        return ResponseEntity.success("添加成功");
    }

    @RequestMapping("/b/delete")
    public ResponseEntity delete(@RequestParam(value = "idStr") String idStr) {
        if (StringUtils.isEmpty(idStr)) {
            return ResponseEntity.error("參數傳遞錯誤");
        }
        List<Long> ids = Arrays.stream(idStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        toolCategoryService.delete(ids);
        return ResponseEntity.success("删除成功");
    }

    @RequestMapping("/b/update")
    public ResponseEntity update(@RequestBody @Validated(value = Update.class) ToolCategory category) {
        toolCategoryService.updateItem(category);
        return ResponseEntity.success("修改成功");
    }

}
