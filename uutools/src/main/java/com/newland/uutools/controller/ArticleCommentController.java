package com.newland.uutools.controller;


import com.newland.uutools.entity.ArticleComment;
import com.newland.uutools.entity.ArticleReply;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IArticleCommentService;
import com.newland.uutools.utils.SecurityContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 文章评论 前端控制器
 *
 * @author leellun
 * @since 2022-02-08 14:14:30
 */
@RestController
@RequestMapping("/api/article")
public class ArticleCommentController {
    @Autowired
    private IArticleCommentService articleCommentService;

    @PostMapping("/comment/f/{articleId}")
    public ResponseEntity list(@PathVariable("articleId") Long articleId, @RequestParam(value = "commentId", required = false) Long commentId) {
        return ResponseEntity.ok(articleCommentService.list(articleId, commentId));
    }

    @PostMapping("/reply/f/{commentId}")
    public ResponseEntity replyList(@PathVariable("commentId") Long commentId, @RequestParam(value = "replyId", required = false) Long replyId) {
        return ResponseEntity.ok(articleCommentService.replyList(commentId, replyId));
    }

    @PostMapping("/comment/submit")
    public ResponseEntity comment(@RequestBody @Validated ArticleComment comment) {
        comment.setFromId(SecurityContextUtils.getUserId());
        articleCommentService.comment(comment);
        return ResponseEntity.success("提交评论成功");
    }

    @PostMapping("/reply/submit")
    public ResponseEntity reply(@RequestBody @Validated ArticleReply reply) {
        reply.setFromId(SecurityContextUtils.getUserId());
        articleCommentService.reply(reply);
        return ResponseEntity.success("回复成功");
    }

    @PostMapping("/comment/delete/{commentId}")
    public ResponseEntity deleteComment(@PathVariable("commentId") Long commentId) {
        articleCommentService.deleteComment(commentId);
        return ResponseEntity.success("删除成功");
    }

    @PostMapping("/reply/delete/{replyId}")
    public ResponseEntity deleteReply(@PathVariable("replyId") Long replyId) {
        articleCommentService.deleteReply(replyId);
        return ResponseEntity.success("删除成功");
    }
}
