package com.newland.uutools.controller;

import com.newland.uutools.entity.dto.RegisterDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/f")
public class RegisterController {
    @Autowired
    private IRegisterService registerService;
    @PostMapping("/register")
    public ResponseEntity register(@RequestBody @Validated RegisterDTO registerDTO){
        registerService.register(registerDTO);
        return ResponseEntity.success("注册成功,请前往验证邮件验证账户");
    }
}
