package com.newland.uutools.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.Feedback;
import com.newland.uutools.entity.Tool;
import com.newland.uutools.entity.dto.FeedbackPageDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.entity.vo.ToolLVO;
import com.newland.uutools.service.IFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 反馈表 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/feedback")
public class FeedbackController {
    @Autowired
    private IFeedbackService feedbackService;
    @ResponseBody
    @PostMapping(path = {"/f/list","/b/list"})
    public ResponseEntity list(@RequestBody FeedbackPageDTO pageDTO){
        IPage<Feedback> list = feedbackService.getList(pageDTO);
        return ResponseEntity.ok(list);
    }
    @RequestMapping("/f/submit")
    public ResponseEntity submit(@RequestBody @Validated(value = Update.class) Feedback feedback) {
        String username=SecurityContextHolder.getContext().getAuthentication().getName();
        feedbackService.add(username,feedback);
        return ResponseEntity.success("提交成功");
    }
    @RequestMapping("/b/update")
    public ResponseEntity update(@RequestBody @Validated(value = Update.class) Feedback feedback) {
        feedbackService.updateItem(feedback);
        return ResponseEntity.success("完成回复");
    }
}
