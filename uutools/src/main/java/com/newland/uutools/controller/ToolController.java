package com.newland.uutools.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.Tool;
import com.newland.uutools.entity.Tool;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.entity.vo.ToolLVO;
import com.newland.uutools.service.IToolService;
import com.newland.uutools.utils.SecurityContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 工具表 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/tool")
public class ToolController {
    @Autowired
    private IToolService toolService;

    @RequestMapping("/b/list")
    public ResponseEntity list(@RequestBody PageDTO pageDTO) {
        IPage<ToolLVO> list = toolService.getList(pageDTO);
        return ResponseEntity.ok(list);
    }

    @RequestMapping("/b/add")
    public ResponseEntity add(@RequestBody @Validated(value = Insert.class) Tool category) {
        toolService.add(category);
        return ResponseEntity.success("添加成功");
    }

    @RequestMapping("/b/delete")
    public ResponseEntity delete(@RequestParam(value = "idStr") String idStr) {
        if (StringUtils.isEmpty(idStr)) {
            return ResponseEntity.error("参数错误");
        }
        List<Long> ids = Arrays.stream(idStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        toolService.delete(ids);
        return ResponseEntity.success("删除成功");
    }

    @RequestMapping("/b/update")
    public ResponseEntity update(@RequestBody @Validated(value = Update.class) Tool category) {
        toolService.updateItem(category);
        return ResponseEntity.success("修改成功");
    }

    @PostMapping("/f/list")
    public ResponseEntity tools(@RequestParam(value = "category",required = false) String category) {
        List<ToolLVO> list = toolService.getTools(SecurityContextUtils.getUserId(),category);
        return ResponseEntity.ok(list);
    }
}
