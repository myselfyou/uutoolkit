package com.newland.uutools.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.entity.Discuss;
import com.newland.uutools.entity.dto.SearchDTO;
import com.newland.uutools.entity.vo.BlogArticleVO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.entity.vo.ToolLVO;
import com.newland.uutools.service.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/search/f")
public class SearchController {
    @Autowired
    private ISearchService searchService;

    @RequestMapping("/tools")
    public ResponseEntity tools(@RequestBody SearchDTO pageDTO) {
        IPage<ToolLVO> list = searchService.searchTools(pageDTO);
        return ResponseEntity.ok(list);
    }

    @RequestMapping("/articles")
    public ResponseEntity articles(@RequestBody SearchDTO pageDTO) {
        IPage<BlogArticleVO> list = searchService.searchArticles(pageDTO);
        return ResponseEntity.ok(list);
    }

    @RequestMapping("/discuss")
    public ResponseEntity discuss(@RequestBody SearchDTO pageDTO) {
        IPage<Discuss> list = searchService.searchDiscuss(pageDTO);
        return ResponseEntity.ok(list);
    }
}
