package com.newland.uutools.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.User;
import com.newland.uutools.entity.dto.ForgetDTO;
import com.newland.uutools.entity.dto.ForgetSetDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.dto.UserDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @RequestMapping("/b/list")
    public ResponseEntity list(@RequestBody PageDTO pageDTO) {
        IPage<User> list = userService.getUsers(pageDTO);
        return ResponseEntity.ok(list);
    }

    @RequestMapping("/b/add")
    public ResponseEntity add(@RequestBody @Validated(value = Insert.class) User user) {
        userService.addUser(user);
        return ResponseEntity.success("添加用户成功");
    }

    @RequestMapping("/b/delete")
    public ResponseEntity delete(@RequestParam(value = "idStr") String idStr) {
        if (StringUtils.isEmpty(idStr)) {
            return ResponseEntity.error("參數傳遞錯誤");
        }
        List<Long> ids = Arrays.stream(idStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        userService.deleteUser(ids);
        return ResponseEntity.success("删除用户成功");
    }

    @RequestMapping("/b/update")
    public ResponseEntity update(@RequestBody @Validated(value = Update.class) User user) {
        if (StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(null);
        }
        userService.updateUser(user);
        return ResponseEntity.success("修改用户成功");
    }

    @RequestMapping("/f/updateInfo")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity updateInfo(@RequestBody @Validated UserDTO user) {
        user.setUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        userService.updateUserInfo(user);
        return ResponseEntity.success("修改用户成功");
    }

    @RequestMapping("/b/changeStatus/{id}")
    public ResponseEntity changeStatus(@PathVariable(value = "id") Long id, @RequestParam("status") Integer status) {
        userService.changeStatus(id, status);
        return ResponseEntity.success("修改状态成功");
    }

    @PostMapping("/f/info")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity info() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.getUser(username);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/f/forget")
    public ResponseEntity forget(@RequestBody @Validated ForgetDTO forgetDTO) {
        userService.forget(forgetDTO);
        return ResponseEntity.success("请前往邮箱验证邮件");
    }

    @PostMapping("/f/forgetSet")
    public ResponseEntity setForgetPassword(@RequestBody @Validated ForgetSetDTO forgetDTO) {
        userService.forgetSet(forgetDTO);
        return ResponseEntity.success("密码设置成功");
    }

    @PostMapping("/f/updatePassword")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity updatePassword(@RequestParam("password") String password) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        userService.updatePassword(username, password);
        return ResponseEntity.success("密码设置成功");
    }
}
