package com.newland.uutools.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.BlogArticle;
import com.newland.uutools.entity.dto.ArticleDTO;
import com.newland.uutools.entity.dto.ArticleListDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.BlogArticleVO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IBlogArticleService;
import com.newland.uutools.utils.SecurityContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * 博客文章
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/article")
public class BlogArticleController {
    @Autowired
    private IBlogArticleService blogArticleService;

    @PostMapping(path = {"/f/list", "/b/list"})
    public ResponseEntity list(@RequestBody ArticleListDTO page) {
        IPage<BlogArticleVO> result = blogArticleService.getArticleList(page);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/f/get/{id}")
    public ResponseEntity getArticle(@PathVariable("id") Long id) {
        if (id == null) {
            return ResponseEntity.error("文章id未传");
        }
        return ResponseEntity.ok(blogArticleService.getArticle(id));
    }
    @PostMapping("/getUserArticles")
    public ResponseEntity getUserArticles(@RequestBody ArticleListDTO page) {
        page.setUserId(SecurityContextUtils.getUserId());
        IPage<BlogArticleVO> result = blogArticleService.getUserArticleList(page);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/add")
    public ResponseEntity add(@RequestBody @Validated(Insert.class) ArticleDTO article) {
        Long userId=SecurityContextUtils.getUserId();
        article.setUserId(userId);
        blogArticleService.addArticle(article);
        return ResponseEntity.success("提交成功");
    }

    @PostMapping("/update")
    public ResponseEntity update(@RequestBody @Validated(Update.class) ArticleDTO article) {
        blogArticleService.updateArticle(SecurityContextUtils.getUserId(),article);
        return ResponseEntity.success("修改成功");
    }
    @RequestMapping("/b/delete/{id}")
    public ResponseEntity backDelete(@PathVariable("id") Long id) {
        blogArticleService.backDeleteArticle(id);
        return ResponseEntity.success("删除成功");
    }
    @RequestMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        blogArticleService.deleteArticle(SecurityContextUtils.getUserId(),id);
        return ResponseEntity.success("删除成功");
    }
}
