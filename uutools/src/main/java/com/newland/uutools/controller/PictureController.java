package com.newland.uutools.controller;


import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IPictureService;
import com.newland.uutools.service.IUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 图片表 前端控制器
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/picture")
public class PictureController {
    @Autowired
    private IUploadService pictureService;
    @PostMapping("/upload")
    public ResponseEntity uploadPicture(@RequestParam("file") MultipartFile file){
        String url=pictureService.uploadPicture(file);
        if(StringUtils.isEmpty(url)){
            return ResponseEntity.error("上傳失敗");
        }else{
            return ResponseEntity.ok(url);
        }
    }
}
