package com.newland.uutools.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.Admin;
import com.newland.uutools.entity.dto.AdminPageDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IAdminLoginService;
import com.newland.uutools.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 后台管理用户表 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:14
 */
@RestController
@RequestMapping("/api/admin/b/")
public class AdminController {

    @Autowired
    private IAdminService adminService;

    @RequestMapping("/info")
    public ResponseEntity info() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(adminService.getInfo(username));
    }

    @RequestMapping("/list")
    public ResponseEntity list(@RequestBody AdminPageDTO adminPageDTO) {
        IPage<Admin> list = adminService.getAdmins(adminPageDTO);
        return ResponseEntity.ok(list);
    }

    @RequestMapping("/add")
    public ResponseEntity add(@RequestBody @Validated(value = Insert.class) Admin admin) {
        adminService.addAdmin(admin);
        return ResponseEntity.success("添加用户成功");
    }

    @RequestMapping("/delete")
    public ResponseEntity delete(@RequestParam(value = "idStr") String idStr) {
        if(StringUtils.isEmpty(idStr)){
            return ResponseEntity.error("參數傳遞錯誤");
        }
        List<Long> ids = Arrays.stream(idStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        adminService.deleteAdmin(ids);
        return ResponseEntity.success("删除用户成功");
    }

    @RequestMapping("/update")
    public ResponseEntity update(@RequestBody @Validated(value = Update.class) Admin admin) {
        if(StringUtils.isEmpty(admin.getPassword())){
            admin.setPassword(null);
        }
        adminService.updateAdmin(admin);
        return ResponseEntity.success("修改用户成功");
    }

    @RequestMapping("/changeStatus/{id}")
    public ResponseEntity changeStatus(@PathVariable(value = "id") Long id,@RequestParam("status") Integer status) {
        adminService.changeStatus(id,status);
        return ResponseEntity.success("修改状态成功");
    }

}
