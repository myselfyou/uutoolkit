package com.newland.uutools.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import com.newland.uutools.entity.Discuss;
import com.newland.uutools.entity.dto.DiscussDTO;
import com.newland.uutools.entity.dto.DiscussPageDTO;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.IDiscussService;
import com.newland.uutools.utils.SecurityContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 话题 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/discuss")
public class DiscussController {
    @Autowired
    private IDiscussService discussService;
    @RequestMapping(path = {"/b/list","/f/list"})
    public ResponseEntity list(@RequestBody DiscussPageDTO page) {
        IPage<Discuss> result = discussService.getList(page);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = {"/b/get/{id}","/f/get/{id}"})
    public ResponseEntity getDiscuss(@PathVariable("id") Long id) {
        if (id == null) {
            return ResponseEntity.error("话题id未传");
        }
        return ResponseEntity.ok(discussService.getDiscuss(id));
    }
    @GetMapping(path = {"/f/detail/{id}"})
    public ResponseEntity getDetailDiscuss(@PathVariable("id") Long id) {
        if (id == null) {
            return ResponseEntity.error("话题id未传");
        }
        return ResponseEntity.ok(discussService.getDetailDiscuss(id));
    }
    @RequestMapping(path = {"/getUserDiscuss"})
    public ResponseEntity getUserDiscuss(@RequestBody DiscussPageDTO page) {
        page.setUserId(SecurityContextUtils.getUserId());
        IPage<Discuss> result = discussService.getList(page);
        return ResponseEntity.ok(result);
    }

    @RequestMapping("/add")
    public ResponseEntity add(@RequestBody @Validated(Insert.class) DiscussDTO discuss) {
        discuss.setUserId(SecurityContextUtils.getUserId());
        discussService.add(discuss);
        return ResponseEntity.success("添加成功");
    }

    @RequestMapping("/update")
    public ResponseEntity update(@RequestBody @Validated(Update.class) DiscussDTO discuss) {
        discussService.updateItem(SecurityContextUtils.getUserId(),discuss);
        return ResponseEntity.success("修改成功");
    }

    @RequestMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        discussService.delete(SecurityContextUtils.getUserId(),id);
        return ResponseEntity.success("删除成功");
    }
}
