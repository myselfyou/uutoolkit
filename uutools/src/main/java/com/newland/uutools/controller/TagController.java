package com.newland.uutools.controller;


import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.ResponseEntity;
import com.newland.uutools.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章标签
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/tag")
public class TagController {
    @Autowired
    private ITagService tagService;
    @PostMapping("/b/list")
    public ResponseEntity list(PageDTO pageDTO){
        return ResponseEntity.ok(tagService.list(pageDTO));
    }
}
