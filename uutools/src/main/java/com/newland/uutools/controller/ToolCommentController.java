package com.newland.uutools.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@RestController
@RequestMapping("/api/tool/comment")
public class ToolCommentController {

}
