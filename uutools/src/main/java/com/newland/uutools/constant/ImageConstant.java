package com.newland.uutools.constant;

public class ImageConstant {
    public static final int FLAG_AVATAR = 1;
    public static final int FLAG_TOOL = 2;
    public static final int FLAG_DISCUSS = 3;
}
