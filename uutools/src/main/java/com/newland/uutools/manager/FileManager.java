package com.newland.uutools.manager;

import com.newland.uutools.utils.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class FileManager {
    @Value("${picture.prefix}")
    private String prefix;
    @Value("${picture.path}")
    private String imgPath;
    @Value("${picture.html}")
    private String htmlPath;

    public String getPrefix() {
        return prefix;
    }

    public String getPrefixMapping() {
        return prefix + "**";
    }

    public String getHtmlPath() {
        return htmlPath;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setContent(String path, String content) {
        FileUtils.writeStr(path, content, false);
    }

    public String getContent(String path) {
        return FileUtils.readStr(path);
    }

    public String getContent(Long id, LocalDateTime createDate) {
        String path = getContentPath(id, createDate);
        return FileUtils.readStr(path);
    }

    public String getContentPath(Long id, LocalDateTime createDate) {
        File folder = new File(htmlPath + File.separator + createDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder.getPath() + File.separator + String.valueOf(id);
    }
}
