package com.newland.uutools.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.newland.uutools.entity.Tool;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.dto.PageDTO;
import com.newland.uutools.entity.vo.ToolLVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 工具表 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Repository
public interface ToolMapper extends BaseMapper<Tool> {
    IPage<ToolLVO> selectToolLPage(Page<ToolLVO> page);

    IPage<ToolLVO> selectToolPageByText(Page<ToolLVO> page, @Param("text") String text);

    List<ToolLVO> selectTools(@Param("categoryId") Long categoryId);

    List<ToolLVO> selectToolsWithLogin(@Param("categoryId") Long categoryId, @Param("userId") Long userId);
}
