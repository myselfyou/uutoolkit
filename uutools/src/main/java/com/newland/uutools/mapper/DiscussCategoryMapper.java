package com.newland.uutools.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.DiscussCategory;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 话题分类 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Repository
public interface DiscussCategoryMapper extends BaseMapper<DiscussCategory> {

}
