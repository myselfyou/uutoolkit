package com.newland.uutools.mapper;

import com.newland.uutools.entity.DiscussComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.vo.DiscussCommentVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 话题评论 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-02-06 22:02:16
 */
@Mapper
public interface DiscussCommentMapper extends BaseMapper<DiscussComment> {
    List<DiscussCommentVO> selectDiscussComments(@Param("discussId") Long discussId, @Param("commentId") Long commentId, @Param("pageSize") Integer pageSize);

    DiscussComment selectCommentForUpdate(@Param("commentId") Long commentId);
}
