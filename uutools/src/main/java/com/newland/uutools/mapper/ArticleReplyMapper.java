package com.newland.uutools.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.ArticleReply;
import com.newland.uutools.entity.DiscussReply;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 话题回复表 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-02-08 14:14:30
 */
@Repository
public interface ArticleReplyMapper extends BaseMapper<ArticleReply> {
    List<ArticleReply> selectReplys(@Param("commentId") Long commentId, @Param("replyId") Long replyId, @Param("pageSize") Integer pageSize);
}
