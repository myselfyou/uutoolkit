package com.newland.uutools.mapper;

import com.newland.uutools.entity.ToolCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 *  工具类别
 *
 * @author leellun
 * @since 2022-01-25 21:52:50
 */
@Repository
public interface ToolCategoryMapper extends BaseMapper<ToolCategory> {

}
