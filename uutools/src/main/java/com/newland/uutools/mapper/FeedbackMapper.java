package com.newland.uutools.mapper;

import com.newland.uutools.entity.Feedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 反馈表 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Mapper
public interface FeedbackMapper extends BaseMapper<Feedback> {

}
