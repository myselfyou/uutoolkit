package com.newland.uutools.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.ArticleComment;
import com.newland.uutools.entity.vo.ArticleCommentVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 文章评论 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-02-08 14:14:30
 */
@Mapper
public interface ArticleCommentMapper extends BaseMapper<ArticleComment> {
    List<ArticleCommentVO> selectComments(@Param("articleId") Long discussId, @Param("commentId") Long commentId, @Param("pageSize") Integer pageSize);

    ArticleComment selectCommentForUpdate(@Param("commentId") Long commentId);
}
