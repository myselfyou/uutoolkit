package com.newland.uutools.mapper;

import com.newland.uutools.entity.ArticleTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 文章标签中间表 Mapper 接口
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Repository
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {
    void insertBatch(@Param("beans") List<ArticleTag> beans);
}
