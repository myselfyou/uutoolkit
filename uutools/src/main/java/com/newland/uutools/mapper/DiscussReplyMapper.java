package com.newland.uutools.mapper;

import com.newland.uutools.entity.DiscussReply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.vo.DiscussCommentVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 话题回复表 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-02-06 22:02:16
 */
@Repository
public interface DiscussReplyMapper extends BaseMapper<DiscussReply> {
    List<DiscussReply> selectDiscussReplys(@Param("commentId") Long commentId, @Param("replyId") Long replyId, @Param("pageSize") Integer pageSize);
}
