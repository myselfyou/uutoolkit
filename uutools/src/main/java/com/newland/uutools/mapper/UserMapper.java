package com.newland.uutools.mapper;

import com.newland.uutools.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
    User selectUserByUsername(@Param("username") String username);
}
