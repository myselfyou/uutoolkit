package com.newland.uutools.mapper;

import com.newland.uutools.entity.ToolComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Mapper
public interface ToolCommentMapper extends BaseMapper<ToolComment> {

}
