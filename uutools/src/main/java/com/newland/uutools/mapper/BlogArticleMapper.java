package com.newland.uutools.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.newland.uutools.entity.BlogArticle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.dto.ArticleListDTO;
import com.newland.uutools.entity.vo.BlogArticleVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 博客文章 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Repository
public interface BlogArticleMapper extends BaseMapper<BlogArticle> {
    IPage<BlogArticleVO> getArticles(Page<BlogArticleVO> page, @Param("dto") ArticleListDTO dto);

    IPage<BlogArticleVO> getArticlesByText(Page<BlogArticleVO> page, @Param("text") String text);

    IPage<BlogArticleVO> getUserArticles(Page<BlogArticleVO> page, @Param("dto") ArticleListDTO dto);

    BlogArticleVO getArticle(@Param("id") Long id);
}
