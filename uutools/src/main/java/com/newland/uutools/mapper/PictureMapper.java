package com.newland.uutools.mapper;

import com.newland.uutools.entity.ArticleTag;
import com.newland.uutools.entity.Picture;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 图片表 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Repository
public interface PictureMapper extends BaseMapper<Picture> {
    void insertBatch(@Param("beans") List<Picture> beans);
    List<Picture> selectDiscussPicture(@Param("discussId") Long discussId);
}
