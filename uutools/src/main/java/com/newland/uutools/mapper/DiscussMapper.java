package com.newland.uutools.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.newland.uutools.entity.Discuss;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.dto.DiscussPageDTO;
import com.newland.uutools.entity.vo.DiscussVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 话题 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Repository
public interface DiscussMapper extends BaseMapper<Discuss> {
    IPage<Discuss> selectDiscussList(Page<Discuss> page, @Param("dto") DiscussPageDTO dto);

    IPage<Discuss> selectDiscussListByText(Page<Discuss> page, @Param("text") String text);

    DiscussVO selectDiscussVO(@Param("id") Long id);
}
