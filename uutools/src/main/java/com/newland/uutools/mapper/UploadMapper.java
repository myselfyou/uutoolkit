package com.newland.uutools.mapper;

import com.newland.uutools.entity.Upload;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *  上传表
 * @author leellun
 * @since 2022-01-30 21:18:55
 */
@Mapper
public interface UploadMapper extends BaseMapper<Upload> {

}
