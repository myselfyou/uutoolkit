package com.newland.uutools.mapper;

import com.newland.uutools.entity.ToolCollect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.vo.ToolLVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 我的工具 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-30 15:21:28
 */
@Mapper
public interface ToolCollectMapper extends BaseMapper<ToolCollect> {
    List<ToolLVO> selectTools(@Param("userId") Long userId, @Param("categoryId") Long categoryId);
}
