package com.newland.uutools.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newland.uutools.entity.Admin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 后台管理用户表 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:14
 */
@Repository
public interface AdminMapper extends BaseMapper<Admin> {
    Admin getAdmin(@Param("username") String username);
}
