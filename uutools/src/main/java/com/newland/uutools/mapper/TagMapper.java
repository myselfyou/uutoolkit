package com.newland.uutools.mapper;

import com.newland.uutools.entity.Tag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 标签 Mapper 接口
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Repository
public interface TagMapper extends BaseMapper<Tag> {
    List<Tag> selectTags(@Param("articleId") Long articleId);
}
