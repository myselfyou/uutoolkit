package com.newland.uutools.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class FeedbackPageDTO extends PageDTO {
    private Integer status;
}
