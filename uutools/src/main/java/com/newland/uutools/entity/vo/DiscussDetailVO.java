package com.newland.uutools.entity.vo;

import com.newland.uutools.entity.User;
import lombok.Data;

@Data
public class DiscussDetailVO {
    private DiscussVO discussVO;
    private User user;
}
