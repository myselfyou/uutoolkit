package com.newland.uutools.entity.vo;

import lombok.Data;

import java.util.List;

@Data
public class DiscussCommentDataVO {
    private List<DiscussCommentVO> list;
    private int count;
}
