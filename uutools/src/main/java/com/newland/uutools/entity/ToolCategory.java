package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.IntOptionValid;
import com.newland.uutools.annotation.Update;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 工具分类
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@TableName("t_tool_category")
public class ToolCategory extends Model<ToolCategory> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 分类
     */
    @NotEmpty(message = "分类不能为空", groups = {Insert.class, Update.class})
    @TableField("name")
    private String name;

    /**
     * 编码
     */
    @NotEmpty(message = "分类编码不能为空", groups = {Insert.class, Update.class})
    @TableField("code")
    private String code;

    /**
     * 顺序
     */
    @NotNull(message = "顺序不能为空", groups = {Insert.class, Update.class})
    @TableField("position")
    private Integer position;

    @NotNull(message = "状态不能为空", groups = {Insert.class, Update.class})
    @IntOptionValid(message = "状态值不正确", options = {0, 1})
    @TableField("status")
    private Integer status;

    /**
     * 描述
     */
    @TableField("description")
    private String description;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
