package com.newland.uutools.entity.vo;

import com.newland.uutools.entity.DiscussComment;
import com.newland.uutools.entity.DiscussReply;
import lombok.Data;

import java.util.List;

@Data
public class DiscussCommentVO extends DiscussComment{
    List<DiscussReply> replyList;
}
