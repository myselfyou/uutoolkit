package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 话题回复表
 * </p>
 *
 * @author leellun
 * @since 2022-02-06 22:02:16
 */
@Getter
@Setter
@TableName("t_discuss_reply")
public class DiscussReply extends Model<DiscussReply> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 评论id
     */
    @NotNull(message = "请指定回复评论")
    @TableField("comment_id")
    private Long commentId;

    /**
     * 平价内容
     */
    @NotEmpty(message = "请提交回复内容")
    @Length(max = 200,message = "评论内容字符长度最多200字")
    @TableField("content")
    private String content;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 用户id
     */
    @TableField("from_id")
    private Long fromId;

    /**
     * 用户名称
     */
    @TableField("from_name")
    private String fromName;

    /**
     * 评论者头像
     */
    @TableField("from_avatar")
    private String fromAvatar;

    /**
     * 被评论用户id
     */
    @NotNull(message = "请指定回复对象")
    @TableField("to_id")
    private Long toId;

    /**
     * 被评论用户名称
     */
    @TableField("to_name")
    private String toName;

    /**
     * 被评论评论者头像
     */
    @TableField("to_avatar")
    private String toAvatar;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
