package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 * 我的工具
 * </p>
 *
 * @author leellun
 * @since 2022-01-30 15:21:28
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("t_tool_collect")
public class ToolCollect extends Model<ToolCollect> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 工具id
     */
    @TableField("tool_id")
    private Long toolId;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 收藏时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

    public ToolCollect(Long userId, Long toolId) {
        this.toolId = toolId;
        this.userId = userId;
    }

}
