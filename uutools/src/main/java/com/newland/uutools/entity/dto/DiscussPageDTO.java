package com.newland.uutools.entity.dto;

import lombok.Data;

@Data
public class DiscussPageDTO extends PageDTO{
    private String category;
    private Long categoryId;
    private Long userId;
}
