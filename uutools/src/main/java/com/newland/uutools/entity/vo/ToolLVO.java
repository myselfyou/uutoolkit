package com.newland.uutools.entity.vo;

import com.newland.uutools.entity.Tool;
import lombok.Data;

@Data
public class ToolLVO extends Tool {
    private String category;
    private String code;
    private Long collectId;
}
