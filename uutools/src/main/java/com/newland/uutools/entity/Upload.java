package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 上传表
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("t_upload")
public class Upload extends Model<Upload> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 图片地址
     */
    @TableField("path")
    private String path;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    public Upload(String path){
        this.path=path;
    }

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
