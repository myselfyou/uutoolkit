package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 评论
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@TableName("t_tool_comment")
public class ToolComment extends Model<ToolComment> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 工具id
     */
    @NotNull(message = "工具不能为空")
    @TableField("tool_id")
    private Long toolId;

    /**
     * 平价内容
     */
    @NotEmpty(message = "请输入评价内容")
    @TableField("content")
    private String content;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 顶层平价id
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 回复用户id
     */
    @TableField("reply_id")
    private Long replyId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
