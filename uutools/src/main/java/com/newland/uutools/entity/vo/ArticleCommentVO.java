package com.newland.uutools.entity.vo;

import com.newland.uutools.entity.ArticleComment;
import com.newland.uutools.entity.ArticleReply;
import lombok.Data;

import java.util.List;

@Data
public class ArticleCommentVO extends ArticleComment {
    List<ArticleReply> replyList;
}
