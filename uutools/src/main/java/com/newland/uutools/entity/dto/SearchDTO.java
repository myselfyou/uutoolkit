package com.newland.uutools.entity.dto;

import lombok.Data;

@Data
public class SearchDTO extends PageDTO {
    private String text;
}
