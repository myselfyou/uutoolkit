package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 话题
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@TableName("t_discuss")
public class Discuss extends Model<Discuss> {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "话题不存在", groups = {Update.class})
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    /**
     * 话题标题
     */
    @NotEmpty(message = "话题标题不能为空", groups = {Insert.class, Update.class})
    @Length(max = 100, message = "话题标题最大长度100", groups = {Insert.class, Update.class})
    @TableField("title")
    private String title;

    /**
     * 话题内容
     */
    @NotEmpty(message = "话题内容不能为空", groups = {Insert.class, Update.class})
    @Length(max = 255, message = "话题标题最大长度255", groups = {Insert.class, Update.class})
    @TableField("content")
    private String content;

    /**
     * 类别
     */
    @NotNull(message = "请选择分类", groups = {Insert.class, Update.class})
    @TableField("category_id")
    private Long categoryId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
