package com.newland.uutools.entity.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
@Data
public class UserDTO {
    /**
     * 邮箱地址用户
     */
    private String username;
    /**
     * 昵称
     */
    @Length(max = 255,message = "昵称长度不能超过36个字符")
    private String nickname;

    /**
     * 个人简介
     */
    @Length(max = 255,message = "个人简介长度不能超过255")
    private String intro;

    /**
     * 头像
     */
    private String avatar;
}
