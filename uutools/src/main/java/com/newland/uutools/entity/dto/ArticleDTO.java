package com.newland.uutools.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.newland.uutools.entity.BlogArticle;
import com.newland.uutools.entity.Tag;
import lombok.Data;

import java.util.List;

@Data
public class ArticleDTO extends BlogArticle {
    @TableField(exist = false)
    private List<String> tagList;
}
