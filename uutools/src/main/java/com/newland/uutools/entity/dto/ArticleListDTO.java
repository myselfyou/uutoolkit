package com.newland.uutools.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ArticleListDTO extends PageDTO{
    private String category;
    private Long categoryId;
    private Long userId;
}
