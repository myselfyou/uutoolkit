package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.newland.uutools.annotation.Insert;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 反馈表
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@TableName("t_feedback")
public class Feedback extends Model<Feedback> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 反馈内容
     */
    @NotEmpty(message = "请提交反馈内容",groups = {Insert.class})
    @Length(message = "抱歉!反馈内容不能超过255字",max = 255,groups = {Insert.class})
    @TableField("content")
    private String content;

    /**
     * 回复内容
     */
    @TableField("reply")
    private String reply;

    /**
     * 反馈用户
     */
    @NotNull(message = "反馈用户不存在",groups = {Insert.class})
    @TableField("user_id")
    private Long userId;

    /**
     * 操作人员
     */
    @TableField("admin_id")
    private Long adminId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 回复时间
     */
    @TableField("reply_time")
    private LocalDateTime replyTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
