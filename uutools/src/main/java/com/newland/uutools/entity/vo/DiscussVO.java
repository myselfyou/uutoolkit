package com.newland.uutools.entity.vo;

import com.newland.uutools.entity.Discuss;
import com.newland.uutools.entity.Picture;
import lombok.Data;

import java.util.List;

@Data
public class DiscussVO extends Discuss {
    private List<Picture> pictures;
}
