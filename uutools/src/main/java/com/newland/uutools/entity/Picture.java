package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 * 图片表
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("t_picture")
public class Picture extends Model<Picture> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("tid")
    private Long tid;
    @TableField("position")
    private Integer position;
    @TableField("flag")
    private Integer flag;
    /**
     * 图片地址
     */
    @TableField("path")
    private String path;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    public Picture(String path) {
        this.path = path;
    }

    public Picture(Long tid, String path, Integer flag, Integer position) {
        this.tid = tid;
        this.flag = flag;
        this.position = position;
        this.path = path;
        this.createTime = LocalDateTime.now();
    }

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
