package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.newland.uutools.annotation.Insert;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * 用户表
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@TableName("t_user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 邮箱地址用户
     */
    @Length(message = "邮箱地址长度为6-36",max = 36,min = 6)
    @Pattern(regexp = "^([a-zA-Z]|[0-9])(\\w|\\-)+@[a-zA-Z0-9]+\\.([a-zA-Z]{2,4})$",message = "邮箱格式不正确")
    @TableField("username")
    private String username;

    /**
     * 密码
     */
    @NotEmpty(message = "请输入密码",groups = {Insert.class})
    @TableField("password")
    private String password;

    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 个人简介
     */
    @TableField("intro")
    private String intro;

    /**
     * 头像
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 登录时间
     */
    @TableField("login_time")
    private LocalDateTime loginTime;

    /**
     * 0 未激活 1激活 2 锁定
     */
    @TableField("status")
    private Integer status;
    /**
     * 锁定时间
     */
    @TableField("lock_time")
    private LocalDateTime lockTime;

    /**
     * 0 普通用户 1后台用户
     */
    @TableField("flag")
    private Integer flag;

    /**
     * 验证时间
     */
    @TableField("verify_time")
    private LocalDateTime verifyTime;
    /**
     * 验证码
     */
    @TableField("verify_code")
    private String verifyCode;



    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
