package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.IntOptionValid;
import com.newland.uutools.annotation.Update;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 话题分类
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@TableName("t_discuss_category")
public class DiscussCategory extends Model<DiscussCategory> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 分类
     */
    @NotEmpty(message = "分类不能为空",groups = {Insert.class, Update.class})
    @TableField("name")
    private String name;

    /**
     * 编码
     */
    @NotEmpty(message = "分类编码不能为空",groups = {Insert.class, Update.class})
    @TableField("code")
    private String code;

    @NotNull(message = "状态不能为空", groups = {Insert.class, Update.class})
    @IntOptionValid(message = "状态值不正确", options = {0, 1})
    @TableField("status")
    private Integer status;

    /**
     * 顺序
     */
    @NotNull(message = "顺序不能为空",groups = {Insert.class, Update.class})
    @TableField("position")
    private Integer position;

    /**
     * 描述
     */
    @TableField("description")
    private String description;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
