package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.IntOptionValid;
import com.newland.uutools.annotation.Update;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 博客文章
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@TableName("t_blog_article")
public class BlogArticle extends Model<BlogArticle> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 标题
     */
    @NotEmpty(message = "请设置标题",groups = {Insert.class, Update.class})
    @Length(message = "标题长度最长为100",max = 100)
    @TableField("title")
    private String title;

    /**
     * 简介
     */
    @Length(message = "简介长度最长为255",max = 255,groups = {Insert.class, Update.class})
    @TableField("intro")
    private String intro;

    /**
     * 内容
     */
    @NotEmpty(message = "请输入内容",groups = {Insert.class, Update.class})
    @TableField("content")
    private String content;

    /**
     * 封面
     */
    @TableField("cover")
    private String cover;

    /**
     * 1 原创，2 转载，3 翻译
     */
    @IntOptionValid(options = {1,2,3},message = "创作声明不正确",groups = {Insert.class, Update.class})
    @TableField("flag")
    private Integer flag;

    /**
     * 浏览量
     */
    @TableField("pv")
    private Integer pv;

    /**
     * 喜欢
     */
    @TableField("favour")
    private Integer favour;

    /**
     * 不喜欢
     */
    @TableField("hate")
    private Integer hate;

    /**
     * 是否发布 1 发布 0下架
     */
    @IntOptionValid(options = {0,1},message = "发布状态不正确",groups = {Insert.class, Update.class})
    @TableField("published")
    private Integer published;

    /**
     * 是否推荐
     */
    @IntOptionValid(options = {0,1},message = "推薦狀態不正确",groups = {Insert.class, Update.class})
    @TableField("recommend")
    private Integer recommend;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 类别
     */
    @TableField("category_id")
    private Long categoryId;

    /**
     * 来源
     */
    @TableField("source")
    private String source;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 0 未删除 1删除
     */
    @TableLogic
    @IntOptionValid(options = {0,1},message = "删除状态不正确",groups = {Insert.class, Update.class})
    @TableField("deleted")
    private Integer deleted;

    /**
     * 删除时间
     */
    @TableField("delete_time")
    private LocalDateTime deleteTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
