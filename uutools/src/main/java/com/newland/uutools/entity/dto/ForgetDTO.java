package com.newland.uutools.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class ForgetDTO {
    @NotEmpty(message = "验证码不能为空")
    private String verifyCode;
    @NotEmpty(message = "操作异常")
    private String verify;
    @NotEmpty(message = "邮箱地址不能为空")
    @Pattern(regexp = "^([a-zA-Z]|[0-9])(\\w|\\-)+@[a-zA-Z0-9]+\\.([a-zA-Z]{2,4})$",message = "邮箱地址的格式不正确")
    private String email;
}
