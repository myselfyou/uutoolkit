package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 * 文章标签中间表
 * </p>
 *
 * @author leellun
 * @since 2022-01-22 12:59:15
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("t_article_tag")
public class ArticleTag extends Model<ArticleTag> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 标签id
     */
    @TableField("tag_id")
    private Long tagId;

    /**
     * 文章id
     */
    @TableField("article_id")
    private Long articleId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 排序
     */
    @TableField("position")
    private Integer position;

    public ArticleTag(Long articleId, Long tagId, Integer position, LocalDateTime createTime) {
        this.articleId = articleId;
        this.tagId = tagId;
        this.position = position;
        this.createTime = createTime;
    }

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
