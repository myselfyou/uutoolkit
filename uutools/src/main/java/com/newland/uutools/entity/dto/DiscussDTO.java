package com.newland.uutools.entity.dto;

import com.newland.uutools.entity.Discuss;
import lombok.Data;

import java.util.List;
@Data
public class DiscussDTO extends Discuss {
    private List<String> pictures;
}
