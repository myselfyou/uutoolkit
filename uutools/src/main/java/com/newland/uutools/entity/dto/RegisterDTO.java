package com.newland.uutools.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class RegisterDTO {
    @NotEmpty(message = "邮箱地址不能为空")
    @Pattern(regexp = "^([a-zA-Z]|[0-9])(\\w|\\-)+@[a-zA-Z0-9]+\\.([a-zA-Z]{2,4})$",message = "邮箱地址的格式不正确")
    private String username;
    @NotEmpty(message = "密码不能为空")
    private String password;
}
