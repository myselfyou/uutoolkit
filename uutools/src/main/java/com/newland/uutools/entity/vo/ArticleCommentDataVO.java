package com.newland.uutools.entity.vo;

import lombok.Data;

import java.util.List;

@Data
public class ArticleCommentDataVO {
    private List<ArticleCommentVO> list;
    private int count;
}
