package com.newland.uutools.entity.vo;

import com.newland.uutools.entity.BlogArticle;
import com.newland.uutools.entity.Tag;
import lombok.Data;

import java.util.List;

/**
 * @author liulun
 * @date 2022/1/25 12:47
 */
@Data
public class BlogArticleVO extends BlogArticle {
    private String nickname;
    private String avatar;
    private List<Tag> tags;
}
