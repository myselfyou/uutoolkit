package com.newland.uutools.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.newland.uutools.annotation.Insert;
import com.newland.uutools.annotation.Update;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 后台管理用户表
 *
 * @author leellun
 * @since 2022-01-22 12:59:14
 */
@Getter
@Setter
@TableName("t_admin")
public class Admin extends Model<Admin> {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "请指定操作用户",groups = {Update.class})
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 账号
     */
    @NotEmpty(message = "请输入账号",groups = {Insert.class,Update.class})
    @Length(message = "账号长度为6-20",max = 20,min = 6)
    @TableField("username")
    private String username;

    /**
     * 密码
     */
    @NotEmpty(message = "请输入密码",groups = {Insert.class})
    @TableField("password")
    private String password;

    /**
     * 生日
     */
    @TableField("birth")
    private LocalDate birth;

    /**
     * 1 男 2女
     */
    @NotNull(message = "请选择性别",groups = {Insert.class,Update.class})
    @TableField("sex")
    private Integer sex;

    /**
     * 邮箱
     */
    @Length(message = "邮箱地址长度为6-36",max = 36,min = 6)
    @Pattern(regexp = "^([a-zA-Z]|[0-9])(\\w|\\-)+@[a-zA-Z0-9]+\\.([a-zA-Z]{2,4})$",message = "邮箱格式不正确")
    @TableField("email")
    private String email;

    /**
     * 登录时间
     */
    @TableField("login_time")
    private LocalDateTime loginTime;

    /**
     * 1 启用 0 不启用
     */
    @NotNull(message = "请选择状态",groups = {Insert.class,Update.class})
    @TableField("status")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
