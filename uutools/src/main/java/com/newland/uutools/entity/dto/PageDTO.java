package com.newland.uutools.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PageDTO {
    @NotNull(message = "当前页不能为空")
    private Integer pageNumber;
    @NotNull(message = "页大小不能为空")
    private Integer pageSize;
}
