package com.newland.uutools.entity.vo;

import com.newland.uutools.constant.ResultCode;
import lombok.Data;

@Data
public class ResponseEntity<T> {
    private Integer code;
    private String msg;
    private T data;

    public ResponseEntity(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseEntity(Integer code, T data) {
        this.code = code;
        this.data = data;
    }

    public ResponseEntity(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> ResponseEntity ok(T data) {
        return new ResponseEntity<>(ResultCode.SUCCESS.getCode(), data);
    }

    public static <T> ResponseEntity ok(String msg, T data) {
        return new ResponseEntity<>(ResultCode.SUCCESS.getCode(), msg, data);
    }

    public static <T> ResponseEntity success(String msg) {
        return new ResponseEntity<>(ResultCode.SUCCESS.getCode(), msg);
    }

    public static <T> ResponseEntity error(int code, String msg) {
        return new ResponseEntity<>(code, msg);
    }

    public static <T> ResponseEntity error(String msg) {
        return new ResponseEntity<>(ResultCode.ERROR.getCode(), msg);
    }

    public static <T> ResponseEntity access(String msg) {
        return new ResponseEntity<>(ResultCode.UNAUTHORIZED.getCode(), msg);
    }
}
