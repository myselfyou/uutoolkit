package com.newland.uutools.exception;

import com.newland.uutools.constant.ResultCode;
import lombok.Data;

@Data
public class BusinessException extends RuntimeException {
    private Integer code;
    private String msg;
    private Object data;

    public BusinessException(Integer code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
    public BusinessException(String msg) {
        super(msg);
        this.code = ResultCode.ERROR.getCode();
        this.msg = msg;
    }
}
