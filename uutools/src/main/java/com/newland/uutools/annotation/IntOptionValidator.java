package com.newland.uutools.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IntOptionValidator implements ConstraintValidator<IntOptionValid, Integer> {
    private int[] options = new int[0];

    @Override
    public void initialize(IntOptionValid constraintAnnotation) {
        options = constraintAnnotation.options();
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext constraintValidatorContext) {
        if(value==null)return true;
        boolean result = false;
        for (int option : options) {
            if (value == option) {
                result = true;
                break;
            }
        }
        return result;
    }
}
