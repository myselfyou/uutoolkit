package com.newland.uutools.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {IntOptionValidator.class}
)
public @interface IntOptionValid {
    String message() default "{javax.validation.constraints.NotNull.message}";

    int[] options() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
