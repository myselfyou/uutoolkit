/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3308
 Source Schema         : uutools

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 24/11/2023 22:46:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `birth` date NULL DEFAULT NULL COMMENT '生日',
  `sex` tinyint(1) NULL DEFAULT NULL COMMENT '1 男 2女',
  `email` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `status` tinyint(1) NOT NULL COMMENT '1 启用 0 不启用',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_name`(`username`) USING BTREE COMMENT '用户名索引'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台管理用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_admin
-- ----------------------------
INSERT INTO `t_admin` VALUES (1, 'admin', '$2a$10$7qtv.6ksq6irdJyPRYITJeq.BxK0De3l8oxQvE19C.ZUzL9zTnNNG', '2021-10-21', 1, '289742735@qq.com', '2022-02-19 21:48:57', 1, '2021-12-06 05:46:18', '2022-02-19 21:48:57');

-- ----------------------------
-- Table structure for t_article_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_article_comment`;
CREATE TABLE `t_article_comment`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `article_id` bigint(0) NOT NULL COMMENT '文章id',
  `content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '平价内容',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `from_id` bigint(0) NOT NULL COMMENT '用户id',
  `from_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名称',
  `from_avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论者头像',
  `to_count` int(0) NOT NULL DEFAULT 0 COMMENT '回复数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章评论' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_article_comment
-- ----------------------------
INSERT INTO `t_article_comment` VALUES (1, 15, '234234234234243', '2022-02-08 14:54:02', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 2);
INSERT INTO `t_article_comment` VALUES (2, 15, 'sdfsdfsdfsdf', '2022-02-08 15:06:06', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 0);
INSERT INTO `t_article_comment` VALUES (3, 15, '', '2022-02-08 15:28:18', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 0);

-- ----------------------------
-- Table structure for t_article_reply
-- ----------------------------
DROP TABLE IF EXISTS `t_article_reply`;
CREATE TABLE `t_article_reply`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(0) NOT NULL COMMENT '话题id',
  `content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '平价内容',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `from_id` bigint(0) NOT NULL COMMENT '用户id',
  `from_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名称',
  `from_avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论者头像',
  `to_id` bigint(0) NOT NULL COMMENT '被评论用户id',
  `to_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '被评论用户名称',
  `to_avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '被评论评论者头像',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_comment_id`(`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '话题回复表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_article_reply
-- ----------------------------
INSERT INTO `t_article_reply` VALUES (14, 1, '234234234234', '2022-02-08 15:03:58', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg');

-- ----------------------------
-- Table structure for t_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_article_tag`;
CREATE TABLE `t_article_tag`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(0) NOT NULL COMMENT '标签id',
  `article_id` bigint(0) NOT NULL COMMENT '文章id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `position` int(0) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_article_tag`(`article_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文章标签中间表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_article_tag
-- ----------------------------
INSERT INTO `t_article_tag` VALUES (3, 13, 89, '2022-02-05 17:50:44', 0);
INSERT INTO `t_article_tag` VALUES (4, 14, 89, '2022-02-05 17:53:42', 0);
INSERT INTO `t_article_tag` VALUES (5, 14, 90, '2022-02-05 17:53:42', 1);
INSERT INTO `t_article_tag` VALUES (6, 15, 82, '2022-02-05 17:59:35', 0);
INSERT INTO `t_article_tag` VALUES (8, 14, 91, '2022-02-05 19:52:33', 0);
INSERT INTO `t_article_tag` VALUES (9, 92, 14, '2022-02-05 20:13:23', 0);
INSERT INTO `t_article_tag` VALUES (10, 93, 14, '2022-02-05 20:20:48', 1);
INSERT INTO `t_article_tag` VALUES (11, 94, 14, '2022-02-05 20:22:16', 2);
INSERT INTO `t_article_tag` VALUES (12, 95, 14, '2022-02-05 20:23:01', 3);
INSERT INTO `t_article_tag` VALUES (13, 96, 15, '2022-02-08 13:34:16', 0);
INSERT INTO `t_article_tag` VALUES (14, 97, 15, '2022-02-08 13:34:16', 1);
INSERT INTO `t_article_tag` VALUES (15, 98, 18, '2022-04-29 21:25:08', 0);

-- ----------------------------
-- Table structure for t_blog_article
-- ----------------------------
DROP TABLE IF EXISTS `t_blog_article`;
CREATE TABLE `t_blog_article`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '简介',
  `content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `cover` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '封面',
  `flag` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 原创，2 转载，3 翻译',
  `pv` int(0) NOT NULL DEFAULT 0 COMMENT '浏览量',
  `favour` int(0) NOT NULL DEFAULT 0 COMMENT '喜欢',
  `hate` int(0) NOT NULL DEFAULT 0 COMMENT '不喜欢',
  `published` tinyint(1) NOT NULL COMMENT '是否发布 1 发布 0下架',
  `recommend` tinyint(1) NULL DEFAULT NULL COMMENT '是否推荐',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `category_id` bigint(0) NOT NULL COMMENT '类别',
  `source` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '0 未删除 1删除',
  `delete_time` datetime(0) NULL DEFAULT NULL COMMENT '删除时间',
  `user_id` bigint(0) NOT NULL COMMENT '用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '博客文章' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_blog_article
-- ----------------------------
INSERT INTO `t_blog_article` VALUES (7, '234234', '<p>234234234234234</p>', '<p>234234234</p>', '/uploads/20220108/1641654758078.jpg', 1, 2, 0, 0, 1, 0, '2022-01-03 16:16:04', '2022-01-08 23:12:50', 1, NULL, 0, NULL, 3);
INSERT INTO `t_blog_article` VALUES (8, '美国“统一战线”破产，乌克兰针对俄颁新法，中日经济关系新突破 ', '<p style=\"text-align:justify;\">美国还没放弃立陶宛。新年伊始，美国国务卿布林肯就忙着到处打电话，3日布林肯与9个国家的外长、副外长通话，谈到了立陶宛以及俄乌局势。布林肯要求罗马尼亚、保加利亚、匈牙利、拉脱维亚、立陶宛、波兰、斯洛伐克、捷克和爱沙尼亚这9个国家，“与立陶宛团结一致”，应对中国的“施压与胁迫”。立陶宛蜉蝣撼树的举动，被美国从去年炒到了今年，去年年底也就是大概20天以前，布林肯才给英法德三国外长及欧盟外交与安全政策高级代表博雷利打了电话。</p>', '<p style=\"text-align:justify;\">美国还没放弃立陶宛。新年伊始，美国国务卿布林肯就忙着到处打电话，3日布林肯与9个国家的外长、副外长通话，谈到了立陶宛以及俄乌局势。</p><p style=\"text-align:justify;\">布林肯要求罗马尼亚、保加利亚、匈牙利、拉脱维亚、立陶宛、波兰、斯洛伐克、捷克和爱沙尼亚这9个国家，“与立陶宛团结一致”，应对中国的“施压与胁迫”。立陶宛蜉蝣撼树的举动，被美国从去年炒到了今年，去年年底也就是大概20天以前，布林肯才给英法德三国外长及欧盟外交与安全政策高级代表博雷利打了电话。</p><p style=\"text-align:center;\"><img src=\"https://p1.itc.cn/images01/20220105/b0c1d70b74404e5d94a6107d9ebcc027.jpeg\"></p><p style=\"text-align:justify;\">同一个欧洲，美国同一个要求：“与立陶宛团结一致”！电话打了这么多，效果可好？要求本就不团结的欧洲“团结”，布林肯真会为难人，于是大部分欧洲国家只能保持沉默。欧盟委员会也只能表示，将在评估中方行动且各成员国达成一致后才会提供援助，这需要至少半年的时间。</p><p style=\"text-align:justify;\">也就是说立陶宛政府需要说服的剩下的26个欧盟成员国，还包括明确的一块“铁板”匈牙利，匈牙利此前就曾警告立陶宛妥善处理对华关系，后果严重的话会将立陶宛踢出自己的供应链。欧盟商讨如何采取行动时，却尴尬地发现立陶宛产业链上的其他国家，包括法国、德国与瑞典的商品，全遭受“池鱼之殃”，德国商会也发出警告，不改善对华关系，关闭系在立陶宛的工厂。</p><p style=\"text-align:justify;\">立陶宛政府一直对国内声称立陶宛与中国的贸易额很少，“中国市场对立陶宛不重要”。问题是，中国市场可能对于立陶宛而言是不重要，但立陶宛承受不起的是连锁反应。立陶宛工业家联合会估算，2022年如若中国实施制裁，立陶宛工业每年的损失可能高达30至50亿欧元。</p><p style=\"text-align:center;\"><img src=\"https://p2.itc.cn/images01/20220105/c9e83fa663c44d07afa6ac6cbcdd3e7a.jpeg\"></p><p style=\"text-align:justify;\">一串打击接踵而至，立陶宛政府12月支持率跌至历史新低，其中站得最高的立陶宛外长兰茨贝尔吉斯摔得最惨。干坏事的人就是这样，第一第二次觉得没啥，不以为意，次数多了警告的人越来越多，干坏事的人多少还是有些心悸，于是，立陶宛总统出来认错了。瑙塞达表示，“立陶宛犯了一个错误，错误不在台当局以‘台湾’之名开设代表处，而是它的名称没有同我协调。”</p><p style=\"text-align:justify;\">亡羊补牢，顾左右而言他，立陶宛还是好好想想到底错在哪。虽然没有道歉，但瑙塞达此举说明立陶宛真的被打痛了。相比于“犯了一个错误”，瑙塞达可能更想说一句“万万没想到”！</p><p style=\"text-align:justify;\">布林肯给9个欧洲国家打电话时，还讨论了如何应对“俄罗斯威胁”，不过又是老一套：美国拉拢欧洲形成应对中俄的“统一战线”。由于美国不断为乌克兰“撑腰”，俄乌关系持续紧张。</p><p style=\"text-align:justify;\">1日，乌克兰关于禁止俄罗斯船只进入乌克兰内河的法律正式生效。根据该法律，悬挂俄罗斯国旗的船只无法在乌克兰包括第聂伯河、多瑙河和德涅斯特河等内河港口之间的国际航线运送乘客和货物。此外，法律还禁止俄罗斯公民的船舶在乌克兰国家船舶登记处注册。</p><figure class=\"image\"><img src=\"https://p7.itc.cn/images01/20220105/d1ac308b9c0e4aac9caea96ef0395831.jpeg\"><figcaption>被俄罗斯扣留的乌克兰海军3艘船只</figcaption></figure><p style=\"text-align:justify;\">新法律一颁布，就代表原本向所有国家开放的乌克兰内河水域，将不再有俄罗斯的身影。在俄罗斯、美国及北约尚未坐到安全保障谈判桌上前，俄乌局势势必会继续恶化下去，乌克兰总统泽连斯基当演员可能不错，治国理政可能差点意思。</p><p style=\"text-align:justify;\">不管布林肯打几个电话，美国拉拢欧洲应对中俄的“统一战线”早已没有了建立的基础。以前跟着美国可以喝酒吃肉，如今汤都不剩一口，还会让自己一身伤痕，傻子才干呢！</p><p style=\"text-align:justify;\">就看美国在亚洲最铁的盟友日本。1日，亚太15个国家共同签署的《区域全面经济伙伴协议》（RCEP）生效，众多媒体尤其是台湾媒体关注到，这是中日经济关系的历史性的突破，中日两国首次建立双边自贸关系，达成双边关税减让安排。RCEP的成功串联中国与日本、日本与韩国这两组经济体。</p><figure class=\"image\"><img src=\"https://p0.itc.cn/images01/20220105/82612827338c4b8a84168f3afaf75088.png\"><figcaption>日美联合训练</figcaption></figure><p style=\"text-align:justify;\">日本目前大有配合美国破坏世界和地区和平稳定的态势，中俄都给予了一定程度的警告和敲打，虽然无法改变美日同盟的基本架构，但从长远来看，经济的紧密联系是可以在一定程度上化解原本存在的地缘风险的。</p><p>在RCEP框架内，日本既不会过于刺激美国，也真正得到了经济上的实惠；对于亚洲而言，日本进入RCEP，也有利于改变日本国内的政治生态，时间久了，民意基础更胜，在配合美国时，日本政府会面临更多的掣肘</p>', 'https://p1.itc.cn/images01/20220105/b0c1d70b74404e5d94a6107d9ebcc027.jpeg', 2, 5, 0, 0, 1, 1, '2022-01-05 12:50:49', '2022-01-08 15:55:18', 4, 'sdfg', 0, NULL, 3);
INSERT INTO `t_blog_article` VALUES (9, '印度女子跟丈夫吵架后离家出走，上了陌生男子的车，遭到轮流侵犯 ', '<p>据印度媒体报道，印度又发生了一起侵犯女性的恶性事件——一名女子遭到了两名男子的轮流侵犯。事情发生在凌晨，受害者因为跟丈夫吵架离家出走，走在半路上，一辆车开到了她旁边，询问受害者是否要搭便车。这个套路在印度已经非常老套了，因为搭便车遭到侵犯的女性非常多。</p>', '<p>据印度媒体报道，印度又发生了一起侵犯女性的恶性事件——一名女子遭到了两名男子的轮流侵犯。事情发生在凌晨，受害者因为跟丈夫吵架离家出走，走在半路上，一辆车开到了她旁边，询问受害者是否要搭便车。这个套路在印度已经非常老套了，因为搭便车遭到侵犯的女性非常多。</p><p>但这名女子连最基本的防范意识都没有，真就上了车，她既不知道自己要去哪里，也不知道这辆车要去哪里，甚至都说不清楚当时自己在哪里，然后就稀里糊涂地上了车。之后会发生什么事情可想而知，在印度，光天化日之下就会发生侵犯事件，更别说深更半夜了。</p><p style=\"text-align:center;\"><img src=\"https://p4.itc.cn/images01/20220105/6f27ac328a6d42dfa5cba9d077f39c19.jpeg\"></p><p>女子上了车之后，就遭到了两个人的轮流侵犯，在这个过程中，汽车一直是开动的，最后，两人将女子丢在路边，扬长而去。根据当地警方的调查，女子被丢弃的地方，距离她上车的地方有大约10公里的路程。</p><p>警察通过监控很快锁定了嫌疑人，几个小时之后就抓捕归案，审讯了一番之后，发现一个非常纳闷的问题，受害者跟犯罪嫌疑人没有任何交集，压根就不认识。据警方透露，这种情况在当地非常罕见。数据显示，当地平均每天要发生4起侵犯案件，在超过98%的案件中，嫌疑人跟受害者都是认识的，仅有个别案件是双方互不相识，并且没有任何血缘关系。</p><p style=\"text-align:center;\"><img src=\"https://p6.itc.cn/images01/20220105/376210c1533349ad9d2852feb52f872c.jpeg\"></p><p>印度侵犯案件频繁，有一个显著的特点，就是熟人作案。占比最高的是亲戚之间，这种亲戚既包括直系亲属，也包括那种拐了好几道弯的亲戚。其次就是邻居，受害者遭到邻居侵犯的可能性仅次于遭到亲戚侵犯，因为这两类犯罪嫌疑人都能够掌握受害者的行踪，并且长期接触，对受害者较为了解。</p><p style=\"text-align:center;\"><img src=\"https://p2.itc.cn/images01/20220105/97af79e976134e78b6a516b84bb02c56.jpeg\"></p><p>之所以陌生人之间的侵犯案件占比非常低，是多方面因素造成的。对于犯罪者来说，他们也不愿意选择不熟悉的人下手，熟人作案有一个好处，事情不会闹大，哪怕报警了，也能想办法压下来，犯罪成本非常低，甚至可以说几乎没有风险。此外还有一个不可忽视的因素，对熟人下手方便，容易找到机会。</p><p style=\"text-align:center;\"><img src=\"https://p5.itc.cn/images01/20220105/0c333f0c28624aa8a534aaad33a967b1.jpeg\"></p><p>对于受害者来说，由于女性安全问题在已经成为了印度的一个痼疾，女性的防范意识非常强，基本上不会跟陌生男子接触。这正是警察对本案感到纳闷的主要原因，一个女人，大半夜在街头晃荡，还上了不认识的人的车。这不就相当于把羊往狼嘴里送吗？怎么可能不出事！</p><p style=\"text-align:center;\"><img src=\"https://p2.itc.cn/images01/20220105/84e4eec21fcc4d41afe18da0d64bf384.jpeg\"></p><p>目前，印度在如何保护女性安全方面仍然没有什么进步，只是有些政客会在嘴上说说，并没有什么实际行动。甚至有些政客连嘴上放放空炮都不愿意，还有些政客还会说出一些匪夷所思的言论，不仅不解决问题，还把责任推给了受害者。在这样的大环境下，印度女性只能想办法自保，并祈祷不会让自己遇到不幸的事情。</p>', 'https://p4.itc.cn/images01/20220105/6f27ac328a6d42dfa5cba9d077f39c19.jpeg', 2, 42, 0, 0, 1, 1, '2022-01-05 12:52:27', '2022-01-09 14:51:59', 2, NULL, 0, NULL, 3);
INSERT INTO `t_blog_article` VALUES (13, 'sdfsdf', 'sdfsdfsdfsdf', 'sdfsdfsdfsdf', '/uutools/uploads/20220205/1644053820742.jpg', 2, 0, 0, 0, 1, NULL, '2022-02-05 17:50:43', NULL, 59, '', 0, NULL, 14);
INSERT INTO `t_blog_article` VALUES (14, 'sdfsdfsdf', 'sdfsdfsdfsdrf', 'sdfsdfsdfsdfsdfsdf', '/uutools/uploads/20220205/1644054796736.jpg', 1, 0, 0, 0, 1, NULL, '2022-02-05 17:53:41', '2022-02-05 20:23:01', 59, '', 0, NULL, 14);
INSERT INTO `t_blog_article` VALUES (15, '3453453', '54345345', '### 水电费水电费\n# sdfsdfsdfsdf', '/uutools/uploads/20220205/1644055145223.jpg', 1, 0, 0, 0, 1, NULL, '2022-02-05 17:59:35', '2022-02-08 13:39:57', 59, '', 0, NULL, 14);
INSERT INTO `t_blog_article` VALUES (16, 'AI技术开源', 'AI技术开源\nPaddleHub\n特性：\n\nPaddleHub旨在为开发者提供丰富的、高质量的、直接可用的预训练模型\n\n【模型种类丰富】: 涵盖CV、NLP、Audio、Video、工业应用主流五大品类的 300+ 预训练模型，全部开源下载，离线可运行', 'AI技术开源\nPaddleHub\n特性：\n\nPaddleHub旨在为开发者提供丰富的、高质量的、直接可用的预训练模型\n\n【模型种类丰富】: 涵盖CV、NLP、Audio、Video、工业应用主流五大品类的 300+ 预训练模型，全部开源下载，离线可运行', '', 1, 0, 0, 0, 1, NULL, '2022-02-08 20:17:26', '2022-02-08 22:49:43', 59, '', 0, NULL, 14);
INSERT INTO `t_blog_article` VALUES (17, 'sdfsdf', 'sdf', 'sdfsdfsdf', '/uutools/uploads/20220429/1651238607601.png', 1, 0, 0, 0, 1, NULL, '2022-04-29 21:23:35', NULL, 59, '', 0, NULL, 14);
INSERT INTO `t_blog_article` VALUES (18, '234234', '234234', '234234234', '/uutools/uploads/20220429/1651238697245.png', 1, 0, 0, 0, 1, NULL, '2022-04-29 21:25:08', NULL, 59, '', 0, NULL, 14);
INSERT INTO `t_blog_article` VALUES (19, '234234', '234234234234234', '234234234', '/uutools/uploads/20220429/1651238764994.png', 1, 0, 0, 0, 1, NULL, '2022-04-29 21:26:40', NULL, 59, '', 0, NULL, 14);
INSERT INTO `t_blog_article` VALUES (20, '345345345', '345345345', '345345345', '/uutools/uploads/20220429/1651238764994.png', 1, 0, 0, 0, 1, NULL, '2022-04-29 21:27:37', NULL, 59, '', 0, NULL, 14);

-- ----------------------------
-- Table structure for t_blog_category
-- ----------------------------
DROP TABLE IF EXISTS `t_blog_category`;
CREATE TABLE `t_blog_category`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类',
  `code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编码',
  `position` int(0) NOT NULL COMMENT '顺序',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章类别' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_blog_category
-- ----------------------------
INSERT INTO `t_blog_category` VALUES (59, 'sdfsdf', 'sdfsdf', 0, 'sdfsdf', '2022-01-25 23:22:29', '2022-01-25 23:30:32', 0);

-- ----------------------------
-- Table structure for t_discuss
-- ----------------------------
DROP TABLE IF EXISTS `t_discuss`;
CREATE TABLE `t_discuss`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '话题标题',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '话题内容',
  `category_id` bigint(0) NOT NULL COMMENT '类别',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `user_id` bigint(0) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '话题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_discuss
-- ----------------------------
INSERT INTO `t_discuss` VALUES (8, '水电费水电费水电费', 'sdfsdfsdfsdf', 60, '2022-02-05 21:08:56', '2022-02-05 21:29:30', 14);

-- ----------------------------
-- Table structure for t_discuss_category
-- ----------------------------
DROP TABLE IF EXISTS `t_discuss_category`;
CREATE TABLE `t_discuss_category`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类',
  `code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `position` int(0) NULL DEFAULT NULL COMMENT '顺序',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '话题分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_discuss_category
-- ----------------------------
INSERT INTO `t_discuss_category` VALUES (60, 'sedfsdf', 'sdfsdf', 0, 'sersdfsdfsdf', '2022-01-25 23:30:53', NULL, 1);
INSERT INTO `t_discuss_category` VALUES (61, 'rfgdfg', 'cfgdfg', 1, 'dfg', '2022-01-25 23:31:37', NULL, 0);
INSERT INTO `t_discuss_category` VALUES (62, 'sdfsdf', 'sfwdf', 2, 'sdfsdfsdf', '2022-01-29 14:09:29', NULL, 1);

-- ----------------------------
-- Table structure for t_discuss_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_discuss_comment`;
CREATE TABLE `t_discuss_comment`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `discuss_id` bigint(0) NOT NULL COMMENT '话题id',
  `content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '平价内容',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `from_id` bigint(0) NOT NULL COMMENT '用户id',
  `from_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名称',
  `from_avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论者头像',
  `to_count` int(0) NULL DEFAULT 0 COMMENT '回复数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '话题评论' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_discuss_comment
-- ----------------------------
INSERT INTO `t_discuss_comment` VALUES (1, 8, '234234', '2022-02-07 19:42:21', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 0);
INSERT INTO `t_discuss_comment` VALUES (4, 8, '22222222222222222222222', '2022-02-07 23:56:46', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 0);
INSERT INTO `t_discuss_comment` VALUES (5, 8, 'aaaaaaaaaaaaaaaaaaaaa', '2022-02-08 00:00:07', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 0);
INSERT INTO `t_discuss_comment` VALUES (6, 8, 'aaaaaaaaaaaaaaaaaaaaa', '2022-02-08 00:03:21', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 0);
INSERT INTO `t_discuss_comment` VALUES (7, 8, 'sdfsdfsdf', '2022-02-08 00:04:45', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 4);

-- ----------------------------
-- Table structure for t_discuss_reply
-- ----------------------------
DROP TABLE IF EXISTS `t_discuss_reply`;
CREATE TABLE `t_discuss_reply`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(0) NOT NULL COMMENT '话题id',
  `content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '平价内容',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `from_id` bigint(0) NOT NULL COMMENT '用户id',
  `from_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名称',
  `from_avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论者头像',
  `to_id` bigint(0) NOT NULL COMMENT '被评论用户id',
  `to_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '被评论用户名称',
  `to_avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '被评论评论者头像',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_comment_id`(`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '话题回复表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_discuss_reply
-- ----------------------------
INSERT INTO `t_discuss_reply` VALUES (1, 1, 'sdfsdfsdf', '2022-02-07 22:17:31', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg');
INSERT INTO `t_discuss_reply` VALUES (3, 7, 'sdfsdfsdfsdf', '2022-02-08 00:04:51', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg');
INSERT INTO `t_discuss_reply` VALUES (11, 7, 'sdfsdfsdfsdf', '2022-02-08 11:30:19', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg');
INSERT INTO `t_discuss_reply` VALUES (13, 6, '我是谁呀', '2022-02-08 11:32:32', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg', 14, 'sdfsdf', '/uutools/uploads/20220130/1643525178215.jpg');

-- ----------------------------
-- Table structure for t_feedback
-- ----------------------------
DROP TABLE IF EXISTS `t_feedback`;
CREATE TABLE `t_feedback`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '反馈内容',
  `reply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `user_id` bigint(0) NULL DEFAULT NULL COMMENT '反馈用户',
  `admin_id` bigint(0) NULL DEFAULT NULL COMMENT '操作人员',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `reply_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '反馈表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_feedback
-- ----------------------------
INSERT INTO `t_feedback` VALUES (1, 'sdfsdf', 'sdfsdfsdf', 1, 1, '2022-01-26 21:00:32', '2022-01-29 15:39:34');
INSERT INTO `t_feedback` VALUES (2, '234234234234234', NULL, NULL, NULL, '2022-01-30 10:43:33', NULL);

-- ----------------------------
-- Table structure for t_picture
-- ----------------------------
DROP TABLE IF EXISTS `t_picture`;
CREATE TABLE `t_picture`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `flag` tinyint(1) NULL DEFAULT NULL COMMENT 'flag',
  `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片地址',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `position` int(0) NULL DEFAULT 0 COMMENT '位置',
  `tid` bigint(0) NULL DEFAULT NULL COMMENT 'tid',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_tid_flag`(`tid`, `flag`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '图片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_picture
-- ----------------------------
INSERT INTO `t_picture` VALUES (54, 3, '/uutools/uploads/20220130/1643557257393.jpg', '2022-01-30 23:42:35', 0, 7);
INSERT INTO `t_picture` VALUES (69, 3, '/uutools/uploads/20220205/1644066531489.jpg', '2022-02-05 21:29:31', 0, 8);
INSERT INTO `t_picture` VALUES (70, 3, '/uutools/uploads/20220205/1644066531489.jpg', '2022-02-05 21:29:31', 1, 8);

-- ----------------------------
-- Table structure for t_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_tag`;
CREATE TABLE `t_tag`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 99 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_tag
-- ----------------------------
INSERT INTO `t_tag` VALUES (82, '345345', '2022-01-03 14:21:46');
INSERT INTO `t_tag` VALUES (83, '23423434', '2022-01-03 14:33:30');
INSERT INTO `t_tag` VALUES (84, '82', '2022-01-03 15:52:38');
INSERT INTO `t_tag` VALUES (85, '印度', '2022-01-05 12:52:27');
INSERT INTO `t_tag` VALUES (89, 'sdfsdf', '2022-02-05 17:50:43');
INSERT INTO `t_tag` VALUES (90, 'sd', '2022-02-05 17:53:41');
INSERT INTO `t_tag` VALUES (91, 'sdf', '2022-02-05 19:52:07');
INSERT INTO `t_tag` VALUES (92, '234234', '2022-02-05 20:13:22');
INSERT INTO `t_tag` VALUES (93, 'sdfsd', '2022-02-05 20:20:48');
INSERT INTO `t_tag` VALUES (94, '111111', '2022-02-05 20:21:45');
INSERT INTO `t_tag` VALUES (95, 'aaaa', '2022-02-05 20:23:01');
INSERT INTO `t_tag` VALUES (96, '水电费', '2022-02-08 13:34:16');
INSERT INTO `t_tag` VALUES (97, '是是', '2022-02-08 13:34:16');
INSERT INTO `t_tag` VALUES (98, '23', '2022-04-29 21:25:08');

-- ----------------------------
-- Table structure for t_tool
-- ----------------------------
DROP TABLE IF EXISTS `t_tool`;
CREATE TABLE `t_tool`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `cover` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面地址',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `category_id` bigint(0) NULL DEFAULT NULL COMMENT '分类id',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `router` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由地址',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '外链地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '工具表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_tool
-- ----------------------------
INSERT INTO `t_tool` VALUES (9, '在线文本对比', '在线文本对比', '/uutools/uploads/20220208/1644329934533.jpg', '2022-02-08 22:19:01', NULL, 63, 1, 'textcontrast', NULL);
INSERT INTO `t_tool` VALUES (10, '文本转ASCII', '文本转ASCII', '/uutools/uploads/20220208/1644329934533.jpg', '2022-02-08 22:22:05', NULL, 63, 1, 'ascii', NULL);
INSERT INTO `t_tool` VALUES (11, '中文转拼音', '中文转拼音', '/uutools/uploads/20220208/1644329934533.jpg', '2022-02-08 22:24:14', NULL, 63, 1, 'pinyin', NULL);
INSERT INTO `t_tool` VALUES (12, '大小写转化（金额）', '大小写转化（金额）', '/uutools/uploads/20220208/1644329934533.jpg', '2022-02-08 22:26:03', '2022-02-08 22:26:16', 63, 1, 'rmb', NULL);
INSERT INTO `t_tool` VALUES (13, '在线繁体字转换', '在线繁体字转换', '/uutools/uploads/20220208/1644329934533.jpg', '2022-02-08 22:27:22', '2022-02-08 22:27:34', 63, 1, 'complex', NULL);
INSERT INTO `t_tool` VALUES (14, '重量计量单位转换', '重量计量单位转换', '/uutools/uploads/20220209/1644374998059.jpg', '2022-02-09 10:50:16', NULL, 69, 1, 'weight', NULL);
INSERT INTO `t_tool` VALUES (15, '手机号归属查询', '手机号归属查询', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 14:00:16', '2022-02-09 18:46:03', 69, 1, 'phone', NULL);
INSERT INTO `t_tool` VALUES (16, 'QQ在线状态', 'QQ在线状态', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 14:01:12', '2022-02-09 19:15:05', 69, 1, 'qqstatus', NULL);
INSERT INTO `t_tool` VALUES (17, '长度计量单位换算', '长度计量单位换算', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 15:13:21', NULL, 69, 1, 'measure', NULL);
INSERT INTO `t_tool` VALUES (18, 'IP地址查询', 'IP地址查询', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 15:27:27', NULL, 69, 1, 'ipaddress', NULL);
INSERT INTO `t_tool` VALUES (19, 'Dns检测', 'Dns检测', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 16:19:05', NULL, 68, 1, 'dns', NULL);
INSERT INTO `t_tool` VALUES (20, '超级Ping', '超级Ping', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 16:33:33', '2022-02-09 16:33:41', 68, 1, 'ping', NULL);
INSERT INTO `t_tool` VALUES (21, '在线端口扫描', '在线端口扫描', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 16:48:59', NULL, 68, 1, 'port', NULL);
INSERT INTO `t_tool` VALUES (22, '域名权重查询', '域名权重查询', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 17:05:57', NULL, 68, 1, 'rank', NULL);
INSERT INTO `t_tool` VALUES (23, '网站状态码查询', '在线网站状态码查询', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 17:13:04', '2022-02-09 17:13:14', 68, 1, 'statuscode', NULL);
INSERT INTO `t_tool` VALUES (24, '颜色值转换', '颜色值转换', '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 19:14:45', '2022-02-09 19:14:54', 66, 1, 'color', NULL);
INSERT INTO `t_tool` VALUES (25, '字符串加解密', '字符串加解密', '/uutools/uploads/20220210/1644495102945.jpg', '2022-02-10 20:11:47', NULL, 64, 1, 'code', NULL);
INSERT INTO `t_tool` VALUES (26, '图片base64', '图片base64', '/uutools/uploads/20220210/1644495102945.jpg', '2022-02-10 20:20:25', '2022-02-10 20:20:36', 65, 1, 'base64', NULL);
INSERT INTO `t_tool` VALUES (27, 'Unix时间戳', 'Unix时间戳', '/uutools/uploads/20220212/1644645005374.jpg', '2022-02-12 13:50:07', NULL, 64, 1, 'unix', NULL);
INSERT INTO `t_tool` VALUES (28, 'properties转yaml', 'properties转yaml', '/uutools/uploads/20220212/1644645005374.jpg', '2022-02-12 16:21:51', '2022-02-12 16:22:01', 64, 1, 'prop2yaml', NULL);
INSERT INTO `t_tool` VALUES (30, 'html/js压缩格式化', 'html/js压缩格式化', '/uutools/uploads/20220213/1644760668374.jpg', '2022-02-13 21:57:38', '2022-02-17 21:05:54', 64, 1, 'html', NULL);
INSERT INTO `t_tool` VALUES (31, 'CSS格式化、压缩', 'CSS格式化、压缩', '/uutools/uploads/20220213/1644760668374.jpg', '2022-02-16 00:21:21', NULL, 64, 1, 'css', NULL);
INSERT INTO `t_tool` VALUES (32, 'XML格式化、压缩', 'XML格式化、压缩', '/uutools/uploads/20220216/1645014603348.jpg', '2022-02-16 20:30:05', '2022-02-16 20:47:01', 64, 1, 'xml', NULL);
INSERT INTO `t_tool` VALUES (33, '在线计算机', '在线计算机', '/uutools/uploads/20220216/1645014603348.jpg', '2022-02-16 20:44:46', NULL, 67, 1, 'cacl', NULL);

-- ----------------------------
-- Table structure for t_tool_category
-- ----------------------------
DROP TABLE IF EXISTS `t_tool_category`;
CREATE TABLE `t_tool_category`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类',
  `code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编码',
  `position` int(0) NOT NULL COMMENT '顺序',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章类别' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_tool_category
-- ----------------------------
INSERT INTO `t_tool_category` VALUES (63, '文字类', 'characters', 0, '文字类', '2022-01-26 23:58:00', '2022-02-08 22:39:40', 1);
INSERT INTO `t_tool_category` VALUES (64, '开发类', 'developer', 1, '开发类', '2022-02-08 22:08:28', '2022-02-08 22:39:48', 1);
INSERT INTO `t_tool_category` VALUES (65, '图像类', 'image', 2, '图像类', '2022-02-08 22:08:50', NULL, 1);
INSERT INTO `t_tool_category` VALUES (66, '设计类', 'design', 3, '设计类', '2022-02-08 22:09:22', NULL, 1);
INSERT INTO `t_tool_category` VALUES (67, '计算类', 'calculate', 4, '计算类', '2022-02-08 22:09:52', NULL, 1);
INSERT INTO `t_tool_category` VALUES (68, '站长类', 'website', 5, '站长类', '2022-02-08 22:10:21', NULL, 1);
INSERT INTO `t_tool_category` VALUES (69, '查询类', 'query', 6, '站长类', '2022-02-08 22:10:48', NULL, 1);

-- ----------------------------
-- Table structure for t_tool_collect
-- ----------------------------
DROP TABLE IF EXISTS `t_tool_collect`;
CREATE TABLE `t_tool_collect`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tool_id` bigint(0) NOT NULL COMMENT '工具id',
  `user_id` bigint(0) NOT NULL COMMENT '用户id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_user_tool`(`tool_id`, `user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '我的工具' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_tool_collect
-- ----------------------------
INSERT INTO `t_tool_collect` VALUES (10, 8, 14, '2022-01-30 17:31:39');
INSERT INTO `t_tool_collect` VALUES (11, 7, 14, '2022-01-30 17:31:40');
INSERT INTO `t_tool_collect` VALUES (12, 6, 14, '2022-01-30 17:31:41');
INSERT INTO `t_tool_collect` VALUES (14, 3, 16, '2022-02-08 21:06:58');
INSERT INTO `t_tool_collect` VALUES (16, 5, 16, '2022-02-08 21:07:03');
INSERT INTO `t_tool_collect` VALUES (17, 1, 16, '2022-02-08 21:07:19');
INSERT INTO `t_tool_collect` VALUES (18, 4, 16, '2022-02-08 21:07:20');
INSERT INTO `t_tool_collect` VALUES (19, 9, 14, '2022-02-11 00:01:59');
INSERT INTO `t_tool_collect` VALUES (20, 10, 14, '2022-02-11 00:02:01');
INSERT INTO `t_tool_collect` VALUES (21, 26, 14, '2022-02-11 00:02:05');
INSERT INTO `t_tool_collect` VALUES (22, 30, 14, '2022-02-13 22:20:12');
INSERT INTO `t_tool_collect` VALUES (23, 17, 14, '2022-02-18 21:12:50');
INSERT INTO `t_tool_collect` VALUES (24, 13, 14, '2022-02-18 21:12:51');
INSERT INTO `t_tool_collect` VALUES (25, 32, 14, '2022-02-18 23:20:05');

-- ----------------------------
-- Table structure for t_tool_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_tool_comment`;
CREATE TABLE `t_tool_comment`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(0) NULL DEFAULT NULL COMMENT '用户id',
  `tool_id` bigint(0) NULL DEFAULT NULL COMMENT '工具id',
  `content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '平价内容',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `parent_id` bigint(0) NULL DEFAULT NULL COMMENT '顶层平价id',
  `reply_id` bigint(0) NULL DEFAULT NULL COMMENT '回复用户id',
  `flag` int(0) NULL DEFAULT NULL COMMENT '回复类型：1 文章 2 话题 3 工具',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '评论' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_tool_comment
-- ----------------------------

-- ----------------------------
-- Table structure for t_upload
-- ----------------------------
DROP TABLE IF EXISTS `t_upload`;
CREATE TABLE `t_upload`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片地址',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 92 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '图片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_upload
-- ----------------------------
INSERT INTO `t_upload` VALUES (1, '/uutools/uploads/20220125/1643113179522.jpg', '2022-01-25 20:19:39');
INSERT INTO `t_upload` VALUES (2, '/uutools/uploads/20220125/1643113234658.jpg', '2022-01-25 20:20:34');
INSERT INTO `t_upload` VALUES (3, '/uutools/uploads/20220125/1643113258458.jpg', '2022-01-25 20:20:58');
INSERT INTO `t_upload` VALUES (4, '/uutools/uploads/20220125/1643113300383.jpg', '2022-01-25 20:21:40');
INSERT INTO `t_upload` VALUES (5, '/uutools/uploads/20220125/1643113317407.jpg', '2022-01-25 20:21:57');
INSERT INTO `t_upload` VALUES (6, '/uutools/uploads/20220125/1643113365275.jpg', '2022-01-25 20:22:45');
INSERT INTO `t_upload` VALUES (7, '/uutools/uploads/20220125/1643113428277.jpg', '2022-01-25 20:23:48');
INSERT INTO `t_upload` VALUES (8, '/uutools/uploads/20220125/1643113674474.jpg', '2022-01-25 20:27:54');
INSERT INTO `t_upload` VALUES (9, '/uutools/uploads/20220125/1643113830207.jpg', '2022-01-25 20:30:30');
INSERT INTO `t_upload` VALUES (10, '/uutools/uploads/20220125/1643113848315.jpg', '2022-01-25 20:30:48');
INSERT INTO `t_upload` VALUES (11, '/uutools/uploads/20220125/1643113865821.jpg', '2022-01-25 20:31:05');
INSERT INTO `t_upload` VALUES (12, '/uutools/uploads/20220125/1643113894890.jpg', '2022-01-25 20:31:34');
INSERT INTO `t_upload` VALUES (13, '/uutools/uploads/20220125/1643113929493.jpg', '2022-01-25 20:32:09');
INSERT INTO `t_upload` VALUES (14, '/uutools/uploads/20220125/1643113955887.jpg', '2022-01-25 20:32:35');
INSERT INTO `t_upload` VALUES (15, '/uutools/uploads/20220125/1643113986375.jpg', '2022-01-25 20:33:06');
INSERT INTO `t_upload` VALUES (16, '/uutools/uploads/20220125/1643114046979.jpg', '2022-01-25 20:34:06');
INSERT INTO `t_upload` VALUES (17, '/uutools/uploads/20220125/1643114066538.jpg', '2022-01-25 20:34:26');
INSERT INTO `t_upload` VALUES (18, '/uutools/uploads/20220125/1643114132005.jpg', '2022-01-25 20:35:32');
INSERT INTO `t_upload` VALUES (19, '/uutools/uploads/20220125/1643114222044.jpg', '2022-01-25 20:37:02');
INSERT INTO `t_upload` VALUES (20, '/uutools/uploads/20220125/1643114264329.jpg', '2022-01-25 20:37:44');
INSERT INTO `t_upload` VALUES (21, '/uutools/uploads/20220125/1643114285445.jpg', '2022-01-25 20:38:05');
INSERT INTO `t_upload` VALUES (22, '/uutools/uploads/20220125/1643114301189.jpg', '2022-01-25 20:38:21');
INSERT INTO `t_upload` VALUES (23, '/uutools/uploads/20220125/1643114331026.jpg', '2022-01-25 20:38:51');
INSERT INTO `t_upload` VALUES (24, '/uutools/uploads/20220125/1643114366098.jpg', '2022-01-25 20:39:26');
INSERT INTO `t_upload` VALUES (25, '/uutools/uploads/20220125/1643114414242.jpg', '2022-01-25 20:40:14');
INSERT INTO `t_upload` VALUES (26, '/uutools/uploads/20220125/1643114738076.jpg', '2022-01-25 20:45:38');
INSERT INTO `t_upload` VALUES (27, '/uutools/uploads/20220125/1643115549840.jpg', '2022-01-25 20:59:09');
INSERT INTO `t_upload` VALUES (28, '/uutools/uploads/20220125/1643124966431.jpg', '2022-01-25 23:36:06');
INSERT INTO `t_upload` VALUES (29, '/uutools/uploads/20220126/1643128161043.png', '2022-01-26 00:29:21');
INSERT INTO `t_upload` VALUES (30, '/uutools/uploads/20220126/1643128229961.jpg', '2022-01-26 00:30:29');
INSERT INTO `t_upload` VALUES (31, '/uutools/uploads/20220126/1643128309737.jpg', '2022-01-26 00:31:49');
INSERT INTO `t_upload` VALUES (32, '/uutools/uploads/20220126/1643202927748.jpg', '2022-01-26 21:16:03');
INSERT INTO `t_upload` VALUES (33, '/uutools/uploads/20220126/1643202994191.jpg', '2022-01-26 21:16:36');
INSERT INTO `t_upload` VALUES (34, '/uutools/uploads/20220126/1643212695860.jpg', '2022-01-26 23:58:16');
INSERT INTO `t_upload` VALUES (35, '/uutools/uploads/20220130/1643524972696.jpg', '2022-01-30 14:42:53');
INSERT INTO `t_upload` VALUES (36, '/uutools/uploads/20220130/1643525047729.jpg', '2022-01-30 14:44:07');
INSERT INTO `t_upload` VALUES (37, '/uutools/uploads/20220130/1643525178215.jpg', '2022-01-30 14:46:18');
INSERT INTO `t_upload` VALUES (38, '/uutools/uploads/20220130/1643556948913.png', '2022-01-30 23:35:49');
INSERT INTO `t_upload` VALUES (39, '/uutools/uploads/20220130/1643556948913.png', '2022-01-30 23:35:49');
INSERT INTO `t_upload` VALUES (40, '/uutools/uploads/20220130/1643556948913.png', '2022-01-30 23:35:49');
INSERT INTO `t_upload` VALUES (41, '/uutools/uploads/20220130/1643556948913.png', '2022-01-30 23:35:49');
INSERT INTO `t_upload` VALUES (42, '/uutools/uploads/20220130/1643556948936.jpg', '2022-01-30 23:35:49');
INSERT INTO `t_upload` VALUES (43, '/uutools/uploads/20220130/1643556948913.png', '2022-01-30 23:35:49');
INSERT INTO `t_upload` VALUES (44, '/uutools/uploads/20220130/1643556949565.jpg', '2022-01-30 23:35:49');
INSERT INTO `t_upload` VALUES (45, '/uutools/uploads/20220130/1643557054972.png', '2022-01-30 23:37:35');
INSERT INTO `t_upload` VALUES (46, '/uutools/uploads/20220130/1643557257393.jpg', '2022-01-30 23:40:57');
INSERT INTO `t_upload` VALUES (47, '/uutools/uploads/20220205/1644048701581.jpg', '2022-02-05 16:11:41');
INSERT INTO `t_upload` VALUES (48, '/uutools/uploads/20220205/1644048938696.jpg', '2022-02-05 16:15:38');
INSERT INTO `t_upload` VALUES (49, '/uutools/uploads/20220205/1644049082626.jpg', '2022-02-05 16:18:02');
INSERT INTO `t_upload` VALUES (50, '/uutools/uploads/20220205/1644049134774.png', '2022-02-05 16:18:54');
INSERT INTO `t_upload` VALUES (51, '/uutools/uploads/20220205/1644049222470.jpg', '2022-02-05 16:20:22');
INSERT INTO `t_upload` VALUES (52, '/uutools/uploads/20220205/1644049343096.jpg', '2022-02-05 16:22:23');
INSERT INTO `t_upload` VALUES (53, '/uutools/uploads/20220205/1644053820742.jpg', '2022-02-05 17:37:00');
INSERT INTO `t_upload` VALUES (54, '/uutools/uploads/20220205/1644054796736.jpg', '2022-02-05 17:53:16');
INSERT INTO `t_upload` VALUES (55, '/uutools/uploads/20220205/1644055145223.jpg', '2022-02-05 17:59:05');
INSERT INTO `t_upload` VALUES (56, '/uutools/uploads/20220205/1644066475696.png', '2022-02-05 21:07:56');
INSERT INTO `t_upload` VALUES (57, '/uutools/uploads/20220205/1644066480215.jpg', '2022-02-05 21:08:01');
INSERT INTO `t_upload` VALUES (58, '/uutools/uploads/20220205/1644066531489.jpg', '2022-02-05 21:08:51');
INSERT INTO `t_upload` VALUES (59, '/uutools/uploads/20220205/1644066531802.jpg', '2022-02-05 21:08:51');
INSERT INTO `t_upload` VALUES (60, '/uutools/uploads/20220208/1644329934533.jpg', '2022-02-08 22:18:54');
INSERT INTO `t_upload` VALUES (61, '/uutools/uploads/20220208/1644330116916.jpg', '2022-02-08 22:21:57');
INSERT INTO `t_upload` VALUES (62, '/uutools/uploads/20220208/1644330253831.jpg', '2022-02-08 22:24:13');
INSERT INTO `t_upload` VALUES (63, '/uutools/uploads/20220208/1644330375953.jpg', '2022-02-08 22:26:16');
INSERT INTO `t_upload` VALUES (64, '/uutools/uploads/20220208/1644330454572.jpg', '2022-02-08 22:27:34');
INSERT INTO `t_upload` VALUES (65, '/uutools/uploads/20220208/1644330467664.jpg', '2022-02-08 22:27:47');
INSERT INTO `t_upload` VALUES (66, '/uutools/uploads/20220208/1644330494270.jpg', '2022-02-08 22:28:14');
INSERT INTO `t_upload` VALUES (67, '/uutools/uploads/20220208/1644333231238.jpg', '2022-02-08 23:13:51');
INSERT INTO `t_upload` VALUES (68, '/uutools/uploads/20220209/1644374998059.jpg', '2022-02-09 10:49:58');
INSERT INTO `t_upload` VALUES (69, '/uutools/uploads/20220209/1644390800601.jpg', '2022-02-09 15:13:20');
INSERT INTO `t_upload` VALUES (70, '/uutools/uploads/20220209/1644391646158.jpg', '2022-02-09 15:27:26');
INSERT INTO `t_upload` VALUES (71, '/uutools/uploads/20220209/1644394744006.jpg', '2022-02-09 16:19:04');
INSERT INTO `t_upload` VALUES (72, '/uutools/uploads/20220209/1644395620109.jpg', '2022-02-09 16:33:40');
INSERT INTO `t_upload` VALUES (73, '/uutools/uploads/20220209/1644396538617.jpg', '2022-02-09 16:48:58');
INSERT INTO `t_upload` VALUES (74, '/uutools/uploads/20220209/1644397556091.jpg', '2022-02-09 17:05:56');
INSERT INTO `t_upload` VALUES (75, '/uutools/uploads/20220209/1644397992951.jpg', '2022-02-09 17:13:13');
INSERT INTO `t_upload` VALUES (76, '/uutools/uploads/20220209/1644403562277.jpg', '2022-02-09 18:46:02');
INSERT INTO `t_upload` VALUES (77, '/uutools/uploads/20220209/1644405292944.jpg', '2022-02-09 19:14:52');
INSERT INTO `t_upload` VALUES (78, '/uutools/uploads/20220209/1644405303861.jpg', '2022-02-09 19:15:03');
INSERT INTO `t_upload` VALUES (79, '/uutools/uploads/20220210/1644495102945.jpg', '2022-02-10 20:11:43');
INSERT INTO `t_upload` VALUES (80, '/uutools/uploads/20220210/1644495634037.jpg', '2022-02-10 20:20:34');
INSERT INTO `t_upload` VALUES (81, '/uutools/uploads/20220212/1644645005374.jpg', '2022-02-12 13:50:05');
INSERT INTO `t_upload` VALUES (82, '/uutools/uploads/20220212/1644654119755.jpg', '2022-02-12 16:22:00');
INSERT INTO `t_upload` VALUES (83, '/uutools/uploads/20220213/1644760668374.jpg', '2022-02-13 21:57:48');
INSERT INTO `t_upload` VALUES (84, '/uutools/uploads/20220216/1645014603348.jpg', '2022-02-16 20:30:04');
INSERT INTO `t_upload` VALUES (85, '/uutools/uploads/20220216/1645015485699.jpg', '2022-02-16 20:44:45');
INSERT INTO `t_upload` VALUES (86, '/uutools/uploads/20220218/1645196985070.jpg', '2022-02-18 23:09:45');
INSERT INTO `t_upload` VALUES (87, '/uutools/uploads/20220219/1645251590295.png', '2022-02-19 14:19:50');
INSERT INTO `t_upload` VALUES (88, '/uutools/uploads/20220219/1645251596468.jpg', '2022-02-19 14:19:56');
INSERT INTO `t_upload` VALUES (89, '/uutools/uploads/20220224/1645709670227.png', '2022-02-24 21:34:30');
INSERT INTO `t_upload` VALUES (90, '/uutools/uploads/20220429/1651238607601.png', '2022-04-29 21:23:27');
INSERT INTO `t_upload` VALUES (91, '/uutools/uploads/20220429/1651238697245.png', '2022-04-29 21:24:57');
INSERT INTO `t_upload` VALUES (92, '/uutools/uploads/20220429/1651238764994.png', '2022-04-29 21:26:05');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱地址用户',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `nickname` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `intro` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '个人简介',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 未激活 1激活 2 锁定 ',
  `verify_time` datetime(0) NULL DEFAULT NULL COMMENT '验证时间',
  `verify_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '验证码',
  `flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1 后台用户',
  `lock_time` datetime(0) NULL DEFAULT NULL COMMENT '锁定时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_username`(`username`) USING BTREE,
  UNIQUE INDEX `unique_verify_code`(`verify_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (3, 'sdfsdfsdfsdf@qq.com', '$2a$10$6JtMoCh/il52M8l94Ss3AeKf4TSldb72pt7En2xxLZjNCMgRv9.Ma', 'sdfsdfsdf', 'sdfsdfsdfsdf', '/uutools/uploads/20220125/1643115549840.jpg', '2022-01-25 20:59:23', NULL, NULL, 1, NULL, NULL, 1, NULL);
INSERT INTO `t_user` VALUES (8, '234234@aa.com', '$2a$10$Gjj.pH7mzZmX2No4wVE/kOxHQTlVro8Fu9N/AuE.aL3CJT0XYTJBa', 'sdfsdf', NULL, '/uutools/uploads/20220125/1643115549840.jpg', '2022-01-29 19:07:46', NULL, NULL, 1, NULL, NULL, 0, NULL);
INSERT INTO `t_user` VALUES (14, '289747235@qq.com', '$2a$10$WffUz8AvOYz/.9pZLLPIgu0ere3sS/lo0.IPdxW3UmmlXCub15YtK', '天涯明月', '天涯明月', '/uutools/uploads/20220218/1645196985070.jpg', '2022-01-29 22:51:46', '2022-04-29 21:23:11', '2022-04-29 21:23:11', 1, '2022-01-29 22:52:26', 'b54a92fb-1c95-47d5-b360-747b43c345c0', 0, NULL);
INSERT INTO `t_user` VALUES (15, 'liu289747235@gmail.com', '$2a$10$wqDe0jspU8pwVnezuSrJJeucpCPgWODUo7AN/mV/S/KnyPahqhqLy', NULL, NULL, NULL, '2022-02-08 15:10:06', '2022-02-08 15:28:07', '2022-02-08 15:28:07', 1, NULL, '6ca63320-f30f-479a-93cd-778fcde22c47', 0, NULL);
INSERT INTO `t_user` VALUES (16, 'mrliudm@qq.com', '$2a$10$QHz0w7E3JinKTDwd1M5e.u1b4QMLD5TcYIYugnUQvFqTEHPZEdlNG', NULL, NULL, NULL, '2022-02-08 21:05:21', '2022-02-27 15:34:59', '2022-02-27 15:34:59', 1, '2022-02-08 21:06:13', '0282fe1c-05be-4da6-8894-6cfd0929b90f', 0, NULL);
INSERT INTO `t_user` VALUES (17, '290931454@qq.com', '$2a$10$ZnrUFvIxRMxQ13kIrMwR7ezqTIBwFz1fP4MReV6aFRirjWiFzs3a.', NULL, NULL, NULL, '2022-02-19 12:18:26', NULL, NULL, 0, NULL, '95fa0322-96a1-44e2-8e30-7ae60a65d14a', 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;
