package com.newland.uutools;

import net.coobird.thumbnailator.Thumbnails;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ImageTest {
    public static void thumbnail() throws IOException {
        FileInputStream fis=new FileInputStream("C:\\Users\\leell\\Pictures\\Feedback\\{3F83D8C7-2AE8-43ED-976D-B71DCBDB3BE0}\\Capture001.png");
        //先压缩并保存图片
        Thumbnails.of(fis).scale(1.00f)   //压缩尺寸 范围（0.00--1.00）
                .outputQuality(0.010f)                       //压缩质量 范围（0.00--1.00）
                .outputFormat("png")                          //输出图片后缀
                .toFile("C:\\Users\\leell\\Pictures\\Feedback\\{3F83D8C7-2AE8-43ED-976D-B71DCBDB3BE0}\\Capture001-out.png");//输出路径
    }
    public static void main(String[] args){
        try {
            thumbnail();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
