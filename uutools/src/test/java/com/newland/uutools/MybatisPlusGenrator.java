package com.newland.uutools;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;

public class MybatisPlusGenrator {
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/uutools?autoReconnect=true&userUnicode=true&characterEncoding=UTF8&useSSL=false&serverTimezone=UTC&rewriteBatchedStatements=true&allowMultiQueries=true";
        FastAutoGenerator.create(url, "leellun", "liulun666")
                .globalConfig(builder -> {
                    builder.author("leellun") // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .commentDate("yyyy-MM-dd HH:mm:ss")
                            .outputDir("D://uutools"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.newland.uutools") // 设置父包名
                            .mapper("mapper")
                            .controller("controller")
                            .xml("xml")
                            .service("service")
                            .serviceImpl("service.impl"); // 设置mapperXml生成路径
                })
                .strategyConfig(builder ->
                        builder.addInclude("t_article_comment","t_article_reply") // 设置需要生成的表名
                                .addTablePrefix("t_") // 设置过滤表前缀
                                .controllerBuilder().enableRestStyle().enableHyphenStyle().formatFileName("%sController")
                                .serviceBuilder().formatServiceFileName("I%sService").formatServiceImplFileName("%sServiceImpl")
                                .entityBuilder().enableActiveRecord().idType(IdType.AUTO).enableLombok().enableRemoveIsPrefix().enableTableFieldAnnotation().addTableFills(new Column("create_time", FieldFill.INSERT), new Column("update_time", FieldFill.UPDATE))
                                .mapperBuilder().enableBaseResultMap().enableMapperAnnotation().convertMapperFileName(entityName -> String.format("%sMapper", entityName)).convertXmlFileName(entityName -> String.format("%sMapper", entityName))
                )
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
